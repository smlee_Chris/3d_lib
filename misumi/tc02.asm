#UGC:2 ASSEMBLY 1322 620 8 1 1 15 2500 2005380 00000e1f \
#- VERS 0 0                                                          \
#- HOST                                                              \
#- LINK                                                              \
#- DBID                                                              \
#- REVS 0,                                                           \
#- RELL 0,                                                           \
#- UOBJ_ID 1063816389 2098362959 -639743704                          \
#- MACH _Windows                                                     \
#-END_OF_UGC_HEADER
#P_OBJECT 6
@P_object 1 0
0 1 ->
@depend 2 0
1 2 [5]
2 2 
@model_name 3 10
3 3 B_TC02
@model_type 4 1
3 4 2
@revnum 5 1
3 5 1110
@bom_count 6 1
3 6 1
@dep_type 7 1
3 7 2
@stamp 8 0
3 8 
@major_vers 9 0
4 9 
@vers 10 1
5 10 0
@alias_name 11 10
5 11 
@minor_vers 12 0
4 12 
@vers 13 1
5 13 0
@reason 14 10
3 14 
@comp_ids 15 1
3 15 [1]
4 15 96
@comp_dep_types 16 1
3 16 [1]
4 16 2
2 2 
3 3 P1_TC02
3 4 2
3 5 1157
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 99
3 16 [1]
4 16 2
2 2 
3 3 C_TC02
3 4 2
3 5 672
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 102
3 16 [1]
4 16 2
2 2 
3 3 P2_TC02
3 4 2
3 5 692
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 105
3 16 [1]
4 16 2
2 2 
3 3 STC02
3 4 2
3 5 580
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 108
3 16 [1]
4 16 2
@dep_db 17 0
1 17 ->
@dep_db 18 0
2 18 [14]
3 18 ->
@to_model_id 19 1
4 19 0
@dep_type 20 1
4 20 2
@obj_id 21 1
4 21 96
@obj_type 22 1
4 22 3
3 18 ->
4 19 1
4 20 2
4 21 99
4 22 3
3 18 ->
4 19 2
4 20 2
4 21 102
4 22 3
3 18 ->
4 19 3
4 20 2
4 21 105
4 22 3
3 18 ->
4 19 4
4 20 2
4 21 108
4 22 3
3 18 ->
4 19 -3
4 20 4
4 21 0
4 22 242
3 18 NULL
3 18 NULL
3 18 NULL
3 18 NULL
3 18 NULL
3 18 NULL
3 18 NULL
3 18 NULL
@model_db 23 0
2 23 [5]
3 23 ->
@model_t_type 24 1
4 24 2
@model_name 25 10
4 25 B_TC02
@generic_name 26 10
4 26 
@stamp 27 0
4 27 
3 23 ->
4 24 2
4 25 P1_TC02
4 26 
4 27 
3 23 ->
4 24 2
4 25 C_TC02
4 26 
4 27 
3 23 ->
4 24 2
4 25 P2_TC02
4 26 
4 27 
3 23 ->
4 24 2
4 25 STC02
4 26 
4 27 
@usr_rev 28 0
1 28 [4]
2 28 
@name 29 10
3 29 PTC
@mod_time 30 0
3 30 
@tm_sec 31 1
4 31 32
@tm_min 32 1
4 32 34
@tm_hour 33 1
4 33 8
@tm_mday 34 1
4 34 6
@tm_mon 35 1
4 35 0
@tm_year 36 1
4 36 100
@action 37 1
3 37 1000
@comment 38 10
3 38 Hostname: 'PTC'
@rel_level 39 10
3 39 
@rev_string 40 10
3 40 
2 28 
3 29 M.ZAJC
3 30 
4 31 17
4 32 59
4 33 7
4 34 4
4 35 10
4 36 103
3 37 1000
3 38 Hostname: 'CPROG004'
3 39 
3 40 
2 28 
3 29 M.ZAJC
3 30 
4 31 4
4 32 28
4 33 9
4 34 8
4 35 11
4 36 103
3 37 1000
3 38 Hostname: 'CPROG016'
3 39 
3 40 
2 28 
3 29 SMLEE
3 30 
4 31 53
4 32 19
4 33 8
4 34 18
4 35 0
4 36 111
3 37 1000
3 38 Hostname: 'SMLEE-PC2'
3 39 
3 40 
@revnum_std 41 1
1 41 375
@flag 42 1
1 42 0
@name 43 10
1 43 NULL
@inst_hidden 44 1
1 44 0
@old_name 45 10
1 45 NULL
@user_symbols 46 0
1 46 [2]
2 46 ->
@class 47 1
3 47 1
@access 48 1
3 48 0
@changed 49 1
3 49 0
@copy_of_generic 50 1
3 50 0
@name 51 10
3 51 DESCRIPTION
@value 52 0
3 52 
@type 53 1
4 53 51
@value(s_val) 54 10
4 54 
@id 55 1
3 55 -1
@type 56 1
3 56 -1
2 46 ->
3 47 1
3 48 0
3 49 0
3 50 0
3 51 MODELED_BY
3 52 
4 53 51
4 54 
3 55 -1
3 56 -1
@appl_data 57 0
1 57 ->
@bounding_box 58 2
2 58 [2][3]
$C0452FD41433048E,BFEBF5050CC1239C,C039DFA82866091E
$4054FAE8FDCBDB34,405D1A17B4E8C5A,40414
@ver_key 59 0
1 59 ->
@time 60 1
2 60 1295338793
@random_num 61 1
2 61 903092496
@hostid 62 1
2 62 -620657349
@last_read 63 1
1 63 -1
#END_OF_P_OBJECT
#Pro/ENGINEER  TM  Wildfire 2.0  (c) 1988-2002 by Wisdom Systems  All Rights Reserved. M170
#UGC_TOC 1 32 81 17#############################################################
SolidPersistTable acd 2bf 1 1322 10 2 1fec 43d3#################################
SolidPrimdata d8c e8 1 1322 10 2 58b8 b202######################################
BasicData e74 30f 1 1322 9 -1 3709 c554#########################################
BasicText 1183 33e 1 1322 9 -1 4ba1 726e########################################
GeomDepen 14c1 16d 1 1322 9 -1 91f0 0867########################################
DispCntrl 162e c7e 1 1322 9 -1 a303 cfc6########################################
DisplData 22ac 29 1 1322 9 -1 0cac b9ca#########################################
LargeText 22d5 1270 1 1322 9 -1 e617 7dc4#######################################
FamilyInf 3545 2d 1 1322 9 -1 0e87 f0a9#########################################
VisibGeom 3572 290 1 1322 11 -1 49d0 081a#######################################
NovisGeom 3802 41 1 1322 11 -1 1a8e e601########################################
ActEntity 3843 6a1 1 1322 9 -1 b6e5 1fa6########################################
AllFeatur 3ee4 2df9 1 1322 11 -1 2b52 98f3######################################
FullMData 6cdd e2f 1 1322 9 -1 9a32 9573########################################
NeuPrtSld 7b0c 29 1 1322 9 -1 0cfc be04#########################################
NeuAsmSld 7b35 70c 1 1322 9 -1 597c e6d4########################################
ModelView#0 8241 3c4 1 1322 9 -1 95bb ce89######################################
ModelView#33 8605 2ee 1 1322 9 -1 2dd6 9f7a#####################################
ModelView#34 88f3 2f4 1 1322 9 -1 30ed 110a#####################################
ModelView#35 8be7 2f0 1 1322 9 -1 2e82 4612#####################################
ModelView#36 8ed7 2f3 1 1322 9 -1 2ffd bc0d#####################################
ModelView#37 91ca 2f3 1 1322 9 -1 30a5 0e72#####################################
ModelView#38 94bd 2f1 1 1322 9 -1 2f07 bae6#####################################
ModelView#1073741828 97ae 28e 1 1322 9 -1 fc7f 9d85#############################
DispDataColor 9a3c 23 1 1322 9 -1 08d5 57cf#####################################
DispDataTable 9a5f 255 1 1322 12 2 1f2d 142f####################################
AssMrgPrt 9cb4 21 1 1322 9 -1 09c5 6a48#########################################
UD~PSOL~GENERAL 9cd5 274 1 1322 9 -1 d9f0 9458##################################
IndentTag 9f49 36f 1 1322 11 -1 af9a 4686#######################################
FeatRefData a2b8 ff 1 1322 11 -1 787d d750######################################
################################################################################
################################################################################
#SolidPersistTable
�� �9���2r椙C��0bؔ0\��|IC  �a��!��2y� �4o~y#F�p��)cF&M:yx��M�6=��4��c��3��L��)R�S�y�N�y��gC%U6iܔY�'�԰�X�8��7r��|C.�4mQ�W,��t��SG�[�*���3M2��(�*H�a�q3f0X�)��al��7p2�I������a�.s�M����r��9�{s�Z x�ܛ���@��y��3'Ԇ@%����:\��1}�1��.���v��{��È*       ,�CP�1�v�P��Nn\&�rt�!Gb��}��qVZ �Gz|�Z"�� �0  � ���GZg|ڌu0�]��Qst��}��Vr�@<7���� �pO���dKtم�NK6i_�$�э@݋c� ��@QG����)�s�p� =���UW�u,�����&~!�] �����4_} �G%�h���Ia��E�S�.��a��х8فY[ŉ�o����}��6��Y���1�:��wA���c��F�	�l�AZr�y��oB(!�)!k$�Jn���P�JV,g�F�[�,��V��kL�1�h�Z�0B
1�Ԥ��PE�f�OJ�i �gQ���)O��� #SolidPrimdata
�� �9��9iڐ	C'̗3r�ԁ�6e �(��A�
:�(qΗ0r� ����},x0�#�0� 6o�|��M�/`  8���2fL��G%�/7�d��Lƍ_�,#gN�9t�<�	.�1j(恃�t���jZ3c˸��݊��!@i��+Hd�1o�0L�f,�x<cv�l�k۷q�XǠ�? 9�3a:i�`G/#BasicData
� Sld_BasicData ��tab_key  � tab_key_struct ��type H� tab_key_ptr �m� cmprs_tabkey ���type �� revnum_ptr ��geom_revnum �;�cosm_revnum >� tol_default_ptr ���type 
�value � �
(6��C-#�����h�#�������ang_value ���lin_digits ����ang_digits � ��standard  � iso_tol_tbls_ptr ��datum_count � �axis_count  �insert_layer_id ��layer_count �rel_part_eps (S�*0U2a�model_eps �fixed_model_eps  �assembled_count  �part_type �first_ent_id ɔ�first_view_id  �outline �HD�2զ��p�JH9 -T���X�-[��Ff#/9 �dim_num r� geom_lib_ptr ��attrib_array � �� @  �  �max_part_size �
model_name TC02 � disp_outl_info ��disp_outline �FE/�3����#�F9ߨ(f	-T�����4-]��Š/A@� sel_outl_info ���outline �FE/�3����#�F9ߨ(f	-T�����4-]��Š/A@�#BasicText
� Sld_BasicText �� assem_mass_prop_ptr ��type  �density �init_dens �rel_density �init_rel_dens  �aver_density #�@��> �vrevnum �;�volume -���VB��cg �-5�F�^+�-CO����AN��oN��inertia �-�J�^�-�-d��,E�zA��&��~�-d��,E�v-��%��AAE��!(kA��&��nAE��!.k-1���~�eps #�����h��surfarea -�g[z�$�csys_id �� mass_prop_ptr ���   ����� assign_mp_ptr ��
mat_name_ptr �� unit_arr �����type �factor (�(P�B�
�
name MM �unit_type  �����9x��KILOGRAM �(24Vx���SECS � p_unit_info ��� custom_unit_arr ����������� xp_custom_sys_unit ������_id ��_obj_type �
name PSOLunit �type �
unit_names �mm kg  sec C rad �_unit_ids ��!�
principal_sys_units �PSOLunit �_principal_sys_units_id ��history_scale �.9fh�Y��-'
�el�q�rF1�q�r�_version �_revnum � dsgnate_data ���#GeomDepen
� Sld_GeomDepend ��frst_geom_ptr  � fem_data ��first_quilt_ptr  � topol_state ��last_regen_feat_id l� incr_time_stamp ��time �M5M)�random_num �/K�hostid ��$�z�� asm_comp_map ���comp_id ��attrib  �
uniq_name �� asm_map_sys �� time_stamp ��vis_revnum ��invis_revnum �� geom_own_tbl ���num_mech_connection  �num_feat_regens w�first_lo_ptr � �#DispCntrl
� Sld_DispCntrl �� color_prop_ptr �� layer_array �����id 	�type u�user_id  � weird_wline_ptr ��
weird_wline 01_ALL_DTM_PLN � layer_contents ��� item_data ��
���revnum �P� layer_compiled ���
���
����H� layer_rbd �� lay_ref_arr �����layer_id 
�
layer_name ��model_ref_id ��status �misc_flags  ����� ��� ��� � cond_block ���saved_operation ��deftype_num ��attributes � ���
u �01__ASM_ALL_DTM_PLN ����
��� item_data �����type � id_data �����id ����������
������������������������oper_arr ���� rules_arr �����sel_option feature �rule_type � rule_value(ddt_basic_typerule_data) ��subtype  �use_flag  �comp_oper  �sel_option feature �cat_type �value ���cat_flags �  �selopt_index ��lookby_index ���feature � rule_value(ddt_basic_id_data) ��sel_id P�comp_oper ��(� scope ���scope_type �F�scope_id  �memb_num  �own_ref_id ��incl_submodels  �sel_option �feature ��� �u �01___PRT_ALL_DTM_PLN ���������� �u �01__ASM_DEF_DTM_PLN ��
��
�����������������������
��
����������������������� �u �01___PRT_DEF_DTM_PLN �������� �u �02_ALL_AXES ��
��
����(����
���
����$���������� ����� ���� �u �02__ASM_ALL_AXES ������
�������������V� data ���status �������������feature � �   feature \�����feature �*�P��-�F  � feature � �u �02___PRT_ALL_AXES ��������� �u �03_ALL_CURVES ��
��
����(����
���
����$���������� ����� ���� �u �03__ASM_ALL_CURVES ����������������feature � �   feature ���  ���feature �*�P��-�F  � feature � �u �03___PRT_ALL_CURVES ������� �u �04_ALL_DTM_PNT ��
���
����(����
���
����$���������� ����� ���� �u �04__ASM_ALL_DTM_PNT ����������������point � rule_value(ddt_basic_all_data) ��use_flag ��(���point �*�P��-�F  � point � �u �04___PRT_ALL_DTM_PNT ������� �u �05_ALL_DTM_CSYS ��
���
����P����
���
����H���������� ����� ��� ��� ���� �u �05__ASM_ALL_DTM_CSYS ����
�������������������
����������������\��4����a��d��g��j���������������feature � �   feature ���  ���feature �*�P��-�F  � feature � �u �05__ASM_DEF_DTM_CSYS ���
�������������������
������������������� �u �05___PRT_ALL_DTM_CSYS �������� �u �05___PRT_DEF_DTM_CSYS ����� �u �07__ASM_ALL_SKELETONS ������������component � �   component �  ���component �*�P��-�F  � component � �u �06_ALL_SURFS ��
��
����(����
���
����$���������� ��� �� ������ �u �06__ASM_ALL_SURFS ������������� �����feature � �   feature ���  ���feature �  feature +�  �feature �*�P�feature � �   feature ���  �feature �*�P��-�F  � feature � � u �06___PRT_ALL_SURFS ������� �Zu �BEZUGSACHSEN ��
���
�������������V����
��
�������������V���� �^u �7_ALL_FEATURES ���
�������������\���a�d�g�j������
�������������\���a�d�g�j������ �_u �2_ALL_CSYS ���
�������������\���a�d�g�j������
�������������\���a�d�g�j������ �#DisplData
� Sld_DispData �� disp_data ��#LargeText
� Sld_LargeText �� names_table �����id S�type �
name COORD �	attr  ���[DEF-PLANE-AXIS  �ASM_RIGHT  �ASM_TOP  �ASM_FRONT  �YAA_1  �ASM_DEF_CSYS  �]ACS0  �bACS1  �eACS2  �hACS3  �kACS4  ��@  �5DEFAULT  ��@  ��DEFAULT  ��@  ��DEFAULT  ��@   ��DEFAULT  � relsymdata ��� relsym_set_arr �
����id ��type �	ui_order_fixed  � symtbl �"����
name  �sym_class '�attr � � value ��type 3�
value(s_val)  � bak_val ���3 �access_fn_idx ��dim_id ��appl_int  �
description �� sym_rstr �� sym_bak_rstr ����DESCRIPTION '� �3 ��3 ��� ���� '� �3 ��3 ��� ����MODELED_BY '� �3 ��3 ��� ���� '� �3Axial-Rillenkugellager DIN 711 - 514 18 ��3Axial-Rillenkugellager DIN 711 - 514 18 ��� ���� '� �3DIN 711 ��3DIN 711 ��� ���� '� �3Axiallager mit Kugeln ��3Axiallager mit Kugeln ��� ���� '� �3Axial-Rillenkugellager DIN 711 - $NENN.  ��3Axial-Rillenkugellager DIN 711 - $NENN.  ��� ���� '� �3Axial-Rillenkugellager DIN 711 - 514 18 ��3Axial-Rillenkugellager DIN 711 - 514 18 ��� ���� '� �3 ��3 ��� ���� '� �3514 18 ��3514 18 ��� ���� '� �390.000 ��390.000 ��� ���� '� �3190 ��3190 ��� ���� '� �393.000 ��393.000 ��� ���� '� �3187.000 ��3187.000 ��� ���� '� �377.000 ��377.000 ��� ���� '� �32.100 ��32.100 ��� ���� '� �3X ��3X ��� ����NB '� �3TC02 ��3TC02 ��� ����NN '� �3TC02 ��3TC02 ��� ����NT '� �3Toggle Clamps - Push-Pull ��3Toggle Clamps - Push-Pull ��� ����NBSYN '� �3$PN. ��3$PN. ��� ����LINA '� �3TC02 ��3TC02 ��� ����PSOL_MULTISOLIDPART '� �2�value(d_val) ���2��� ����PN '� �3�TC02 ��3TC02 ��� ����MATMB '� �3JIS SS400 ��3JIS SS400 ��� ����ST '� �3Bright Chromate ��3Bright Chromate ��� ����TARE '� �3330 ��3330 ��� ����SP '� �31960 {200} ��31960 {200} ��� ����ANGLEH '� �3150 ��3150 ��� ����STROKE '� �330 ��330 ��� ����TYPE '� �3TC02 ��3TC02 ��� ����PPATH '� �3tco2_templ.prj ��3tco2_templ.prj ��� ����KT '� �3DUMMY ��3DUMMY ��� ���� rel_data ���attr  � relset_code_tree �� relset_bak_code_tree ���� ����� �� �� ��� ������PRO_MP_SOURCE <��~"x �3GEOMETRY ��3GEOMETRY �� ������MP_DENSITY -��~"x �:� value(NO_val) ����:��� ���	attr �id ��set_id ��unit_id F�origin ����� ��F���PRO_MP_ALT_MASS J��~"x �:���:��%� ������������PRO_MP_ALT_VOLUME J��~"x �:���:��%� ����@�����@���PRO_MP_ALT_AREA J��~"x �:���:��%� ����;�����;���PRO_MP_ALT_COGX K��~"x �:���:��&� ������������PRO_MP_ALT_COGY K��~"x �:���:��&� ������������PRO_MP_ALT_COGZ K��~"x �:���:��&� ������������PRO_MP_ALT_IXX K��~"x �:���:��&� ����M�����M���PRO_MP_ALT_IYY K��~"x �:���:��&� ����M�����M���PRO_MP_ALT_IZZ K��~"x �:���:��&� ����M�����M���PRO_MP_ALT_IXZ K��~"x �:���:��&� ����M�����M���PRO_MP_ALT_IYZ K��~"x �:���:��&� ����M�����M���PRO_MP_ALT_IXY K��~"x �:���:��&� ����M�����M���PRO_MP_MASS =��~�( �2�eb��ـ���2�� ������������PRO_MP_VOLUME >��~�( �2-���VB���2�� ����@�����@���PRO_MP_DENSITY ?��~�( �2#�@��> ��2��� ����F�����F���PRO_MP_AREA @��~�( �2-�g[z�$��2�� ����;�����;���PRO_MP_COGX A��~�( �2-5�F�^+���2�� ������������PRO_MP_COGY B��~�( �2-CO������2�� ������������PRO_MP_COGZ C��~�( �2AN��oN���2�� ������������PRO_MP_IXX D��~�( �2-��<�1)9��2�� ����M�����M���PRO_MP_IYY E��~�( �2-��������2� � ����M�����M���PRO_MP_IZZ F��~�( �2-��nMf6��2�!� ����M�����M���PRO_MP_IXZ G��~�( �2(��&��~���2�"� ����M�����M���PRO_MP_IYZ H��~�( �2(E��!(k��2�#� ����M�����M���PRO_MP_IXY I��~�( �2Fd��,E�z��2�$� ����M�����M����S �������	V �� restrict_table ���idx_array ����	attr � � p_alt_sets_tbl �� dsgnate_data ���)���id ��type ��	attr �
name �� symbols ��.���sym_type '�
name DESCRIPTION ��.�'MODELED_BY � cfg_states ��� disp_mgr_data �� disp_app_arr ��3���appl_type �5� rep_arr ��5���gen_db_id �@  �gen_db_type �5�gen_db_base_id ��gen_db_gen_attrs  �gen_db_app_attrs  � gen_db_refs �� cra_cis �� item_data ��=��� rep_comp_data ��status � simpl_rep ��set_by  � lw_data ��attrs  � cra_rbd ��� cond_block �� gen_db_runtime ��� gen_db_data ��� rep_data ���spec_rep_id ��attributes � � app_ids ��act_id ��cur_id ��def_id �@  ��3��� disp_style_arr ��O���6�@  ���  ����=���>��� � �����J ����@  ��� expl_arr ��P���6�@   ���  ��id ��type ���state_type  �cur_stage_ind �� stage_info ��U��� unexpl_subass_info ��� expld_comp_ref ���comp_id �� override_sys �� expld_state �� gen_info ���font �color 
��F�� ���@   �@   ��� presentation_arr ��^���6�@  ���  ��F��� disp_ref_arr ���_���id �@  �type �5�aux_id ���_��@  �����@   �� ��@  \���J ���@  �@  �#FamilyInf
� Sld_FamilyInfo �� drv_tbl_ptr ��#VisibGeom
� Sl��6�X�(>�ܘ#� ��SǄ?0�lo�ܤ�0�˹DG"Yv+������%F��@�X����şoR+3����E��.�el��}�?P+��/23�Tqp�"�	��� �#^�M1����㬃�p`���Z�0K�/��	������O/Q������������3���UE��OE�u����W�yG"���n����-7ǡL1��cѻU=;KNLB�Fv!��Y�(����*7�L;������)�}<y�&WC�@L���/�%�Vdm��&?�Q�{��U����*����ރP��y���@��Y��e50�^��73<U���3��]ۇ_���gYb1��UTv��"�J���y���qp��_u��v��BV4+���i �S����OV�H��-M�C}�G'��qd����������/СtŃ�	1lCc�ww{������ gM��Y��m���V�t��m�<Ԗu�q�}�c6��'��u���P���L�n�+3?F�]�ΘS��Z���2��ǘ�����A�ZW<��^�r�#�{��F� z��Έ��1��ȹ��m=�,������a����0����^D�>���_���1$2D����䜇��#NovisGeom
� SlK%����o�����1�'��"/�s��R �S�,$L� �H�ة��E�td#ActEntity
� Sld_Entities �� act_feat_ents �
����feat_id �datum_id � entity_ptr(cosmetic) ��type 2�id �� gen_info ��owner_type ��owner_id � text_ptr ��text_type �
text_value ASM_RIGHT �txt_attrib �owner_id �height �width_factor � format_info ��thickness �� cosm_disp_ents ��� first_disp_ent(line) ��id �� gen_info ��flip ��end1 ����#�F9ߨ(f	�end2 ����#�-9ߨ(f	� entity(line) ��������#�-9ߨ(f	-\0�P'�j-9ߨ(f	���-\0�P'�j-9ߨ(f	-\0�P'�jF9ߨ(f	���-\0�P'�jF9ߨ(f	���#�F9ߨ(f	� plane_ptr ���attr  ����2���ASM_TOP �����F9ߨ(f	FE/�3�F9ߨ(f	-T�����4�����F9ߨ(f	-T�����4-9ߨ(f	-T�����4���-9ߨ(f	-T�����4-9ߨ(f	FE/�3����-9ߨ(f	FE/�3�F9ߨ(f	FE/�3���� ��2���ASM_FRONT �����FE/�3����#�FE/�3�-\0�P'�j�����FE/�3�-\0�P'�j-T�����4-\0�P'�j���-T�����4-\0�P'�j-T�����4���#����-T�����4���#�FE/�3����#���� ��� entity_ptr(coord_sys) ��type �id � gen_info ��color �sys_type ���ref_csys_type ��radius �local_sys �����feat_id � entity(text) ���text_type "�
text_value ASM_DEF_CSYS �txt_attrib �coord �/�/z��owner_id �height �width_factor � format_info ��thickness ����V�� entity_ptr(line3d) ��type �id X� gen_info ��geom_id �W�end1 �-T���X��;��J�L��end2 �-T���X��;��J�L�A6��C-�orig_len � entity(line3d) ���3Y���"�6W-T���X��;��J�L�HD��;��J�L��(�AA_1 ��.��\���]��#�����\�"ACS0 ��-]��a��b�����-Ek��Q�.,�/ a�"ACS1 �b��d��e���t7bS�c�n����9�����9t7bS�c��FB�k*�ER-X�;ՙ$d�"ACS2 �e��g��h�����-;���Q�-2�\(��/ g�"ACS3 �h��j��k�����HD�/4 j�"ACS4 �k��#AllFeatur
� Sl�2
˒�_�p���ݱ��<�ϵ5��2B�2��TNv����Ix}<�㔲<���[<�sµ���wP0wy����.���w`b���@�V4M���#U���H���ÕQX��X�q����iݐpcn]��S�o���Ѧ;4�E�۾Mz��u-�� ��(��YU��<Z{[R��I������9�ʫO�.�R,
,�e`��غH��`��X�\�Q��i�Ѳ�:6�_[�'����[���!.�?L�T<�%�3�۲��, a�$��D��%���:�����۞a{`���G�(~�n����6|�N��������� �)*^Q���r��æ����}e��D04�����^d�F��XV�-o��ԙu�]��BS�i��͠D��G�P�K�������4^@��r���s"&���g�+vzؒ��'7J�i�S(�Tx�YM�X�A���p�@*q����9�o!�G�x\l��� y7��Mt6e�t@���2s�eN�F�;d�v����s9��˰�v4Du��Qi�zp���:N�����mF�����i�;�	�w�x���Rp���W.V����䂏��/�#�\9�a�1�`�:�6��u�5��ʟ�m���ꚁ~�"q#��"b�t��� v��i�*N�h�"��,+�q�d��p�{'����ȋ.ej'3�c���s��o8.�ѓ����x��*������Q��Ϳ�F�bBI��5�`
�v�*��s���d�C��p��ؚ��1��^�,:�q���<��o�.�@�lf��xQ���^UU��DD9?��\�!�k�b�x��k���?����4͚�������Zc��8��^L1��H����d@����3@�c�?�>%T���2��5%C�N�P�����v3�����kb )$)��1*�P��
~j����P�	w�{�qK'�M&�Ȃ�� )C�
���p�W�Ez�0H���̒ZV��m(��IL��R/�J��.��!+�vK���������0�ה}ת����c�i��.-g�..�K���h���8��^����3Z�\_�,P����&Xh#h,ǝ0K8�E��p��@#��`��.!%H��Y��h���^��MC�N��J��s�b)rb�(�e�)ϩ�m�=� ���c4��N�g��ࣛ������P��th��m2;ZY�.tr��}g/['[Dya��\M�U�.O�)6�Ȓ�<~.["Eb\���h#���B����[�d���Kz%����&FB��~A�L�D޳Eϑ�>����G�n�G��j��Q�5��"�n��0Q������"��0#E�QjeL��tv�_�/����?�9�
�q�/b�����g�#'�C�V?����秺�X�xZY1���j( �y��	�*~�g�a�8�m�ȅ��� x�L0,����Scxn�
	UFl92�A�Iu��9���a�z5`G��
}k4���w��q�/�sc���H����ņ����9�H��u�Y�⥥������h��ێ���l�����ӼM��WE���p��F����<GO8բ�$Q����Q�4�5��s}��R��=I�,0�՜�k��ʎ���w��g=B��K5B[u�
�V�>As(UV������6�OQ����)��>�:�L5��&oW	U�v�M��2;��UY8ME��if��]����{U6*��|ߚ�D|J�⏴aG���v�7�̭P���������.S��{J8z-���A���4��)�c&"�'���"�y6�'��.`�yQ�\6��R�X����6�˻�z,g�pr����u��W0I�����f�fq�<����N:ѿ��j�Vn�-�ha���ߟ[VZ"³�(�a
�2��,��7v���z��7|L<xO �ɫ�J qp��r(�`xW�{$;i1���m�I�@��f��Gof�k)�������>EH�C��n���\��iae�JY�d��Y���x ��z5��kJ@�X3���\�Q�v*muI9�`�\��\�M��A�Sb�\�����G��R��Z�s�8��.����{�a�?<Z��U7pH�O*��&�s���&�������{�9ܲ�I��!�>��		qӫ�wy�ǡkHuu�NF��,����̀|�sD�wv�Ε��L��0����q��ge�mY��(��2�}FH����X���޷��sll	��+sB}j��lS�y{{�w{�_1G��1% �G�ͻӲ�YgI�٣����q׺nv����\��Go`2���ȹ&���:��$Rх&��c*�6$�3�vv���#y�Q�l������)��$��)�X��aB�/	�=Y�azЀ�#K�ۡC�Pк(�1b\��Lv�jr��Hӆ�쵑���]x�j���z�[/��wBE�p�x���&��j�2�j��a�\�l5)���A|U��?�,k�-��w ���!�`I���0/��w�bQ�����\P��Wۼ�.��L��(��aʳ�E���Y�W��r��}�:-	�U���Ű������r �ץ�Yv�zd��o���w�p�b���0��j���g*���bl��@	� �,+?)���U��_���5�=�
l�����%^�����ڲ,������Rt>���hI�����å��Tϡ��uff��`����ͩ�x���a
��F�:����>�+���ٛK�	�q��E�#�����4��No�66��t��s��K`���QP���y��� ��
uZy=C�z2��2��Y:�?Kp��v��*-V� �!UPc;_'�\�~�0	�����&��^	-w���B���
M�'@*�1�_���|�VN�����L
��d��g>+/��#&��������x�@�SϢ�6q��'U��Y'��1�i:}l.��Ag��F{>�>��s;$1P�t���g�!�*4��r��TVq�a��� �Y4N #�Qӻ#'Ԍ&��,~&T�Sx@a�G��!�N2�E�zm��������I��~�%#HN����� �eȖQ2�voވ)�0�L &����!�,���{�4�}�Y�~ؘ�`?\���Yo�@�T[��� =��ib�*�=<���͗�<�]�3��C��J�_^�����p��d�g$s�%P?�J:���al�9-%�l�\A[H��#_��ƈ����pLϮ@[�b�0�dT'e��a��E� �w�~����Չ�f@�Z�]���54�*��vO�B8jؓ�.�y����O�"�d����?�9�gl��{W^:l�w�M���kG����A �4��!�fd�E�5ֹ���2@������G��#�#�~��5U��4�;SZf5�k�R�zm�JL���U�%l��h�T����#L�������5��N0���5z/L�)���'Q�*��P�C�wZo�b���_ڀ��c�N&~�p{�5Ǌ)�m���:�_QI�M������
�Swt1-th^�6T3�qUt�<�k�r`鄤�?��N�+�6v8-��RTD�:k�e�:8�N-Y��Ķ+7ő۩�W2# �m�&2ߜ��M��if����!���#��9Y���=�7x��3���I������{�{�70��L��?a0���?�\��>1�V7I ����"T��)}�RwA�����D���VAϓm��������°(ג $����:��h:-��S�z�>�g/mZD�}�ٛ{˸�{xp� a��}�n����~���/�������.�d7fm2jű�uɹʹ	�cQ9�K�X��QA��$��lj&����w'>���%&������؃��B��+�I{c����#��>�P����&�ů��S��(O��.�)Em�]���!;Bt�c�N����ſ� R���R0~ ,��eg�׫^`�~UƖ K��9��z/p��*��i*7j!=e�.��̉������'MW�s�IO�}|��U|z��&�����Qu�����:�It��F��Àؼ`��o�֬_�օ�Y����e���^�I��'j��m�7h�S(��h6��u��D'C�}�?	��v���}��zQ��-�XVj\�JS��Rm��ߍ�!'��	.�Ԫ~�N�ZAEP�ؔq�!�΢��r��D��{[��;T���@y�$���}�^�,:�q���<��o����8V^<�Ҿ�b��R{�]ج�P�a��ׁ�!�1��:Co���X�
��?�3՟�:ܷ��-cQM�\H=K�2��3W8�8-�!�&����a�{?q�s	���CO�C��F��{�1l�v�G�X��
C,����윆�3w��N���p3�� �����J�5�>�i��*nͰ��
�f� 4MB��Q�6>��7��Ý{��Q�'����(�MɃg���11n��9F��#2T-�<cxU�J��|K�P�D��۽^��T�W�y&����
7�v!D�R�A}������\��Y�:���V���f���:=�e^��D�J�A�h<����Z�V;�+�鎎4�3��{ �)�;��[��9����gaY^{s<������1��|>�7B޿ �Y�<�,�˙�oH=(��>�SB�M����,�1Vu�£Z�W���G�����+
s/i���2C�a?���Tk
A�M�U{�.���R�H��� �>,�=�.����R���Ddl�n~&}L)6����}�������pL)�e�T��*İ����:}��
�M�5��Ы����3.a�~�%>lv��Đ�A7����`�[��M~�hh8i$���\��`73�?�~fo9*"w��L��+u&&�t���L��}_��x�����ա{Z4�@�.�>��Te�q���F��*$�_g]��+�r��Qu�;��/�~����� ����?�@bb`Ė  %Ko�=V�(�~��x5��=xh�Rk�n��3�`���-Bi���P�����j%����UC��g�r"�Ǔ��8~��u`�x���Ƕ.�̲��p3��'���\���ڬ6��]���GeMOF�,޴��͇~ۄ��W�����O�_��Fj�-[H_��.��>��ɫ`������{�|90|�%1{�U{d!\_&!�^�|��ì��[�Vt$8�&��"��@���[P�L��ѹ���RY��D��-j������6�#lؗ���9��F�����/B�5����!8�	fY��M��FQ��-D���@_}T
�&�"��!�i(Y�'�=�����	�����C�Խ+kN�ONK�L�#���ET���@h��^�bA:XE�\�z��s�%@g��*�c)fH�G~���U`����f�W��܀�%[i�<��'4Ɩ8qu���[=Ћ�1���L��Q2�ӛi+�;�a�"5�&�{`��)O�ϥIi��eboV��o��P�7�
?�څ�F"]�В�|^N6h	YNЭ6w)U���
W�4 N8���>t��6�05o�Ք�^��J���|�i��c�R�h۝�A�����o�0s颣W����t��Hg����e<U�(�Z�|6l=�0�+Ƞ�S0��M�ȭ�~\��Y�:���V���f���:=�e^��D�J�A�h<����Z�V;�+�鎎4�3��{ �)�;��[��9����gaY^{s<������1��|>�7B޿ �Y�<�,�˙�oH=(��>�SB�M����,�1Vu�£Z�W���G�����+
s/i���2C�a>�h���ai2f�D�DCh�Ѻ\��;*U�Ρ�Uz�N�}��z3�λ�F�l��dY��8��"cp���-1�4L���2d��5q0<:�n��t˚.L���a�cM�D�:�G6O��1r�r���'VБ���M�}�_��]�5H$-&cq����f��H�/�ܶ����'?��&Sx�r��1��!J��,{�\���[ԀP�����Bzц�>�/��?,34��S�_!xI���R||-�3�L��X&�K-Ά1�¦��^t}}P�͒�u��Y�0�����E��]�����*^�,<�t�65�k��Ϥ=�el�@!�����]E���"�bad�x/V�Z���Ii:�6��^`D>�d��v�=
�GM�n����1(~Q������I��|�ʁle�3*1/s�@4���(�/����[t�)֔½������ۯA�*RA��uٱi�Y/E���A�I`��˩���md&�IP]/�c㪘q�T|�-���u�!��h���<��5Z)W�>�xV���{�nr��v=!���~�I�J����h�e!PyP�!�2oZCs�T��q�2?�V�R�x�1L��`rtd��:o�����
?���� ^�J�0:�6:�wW�ŔfCЂ�<β(L:�]�p{�hk�8��w�h��;�J�y%|�q/���SF�"&|�G��${*v��-�U����4n���1.?(�<�����_&1>��n/��ύ�#�Հ-+���u�5��ʟ�m��o�X���O`��C�䚺'(�k��މ����'E��-�HE����-�$��"�=x�U�?�Syf�9�[��4)�!"�N������R���ӐC��\�Ih�NE��%�c�[t�t�D�I(!���U�M����L����3�8�/�JA`���v�	�*�inx/?L�:꟏9; � ��3=S0��(N#p4��ZԵ}�ۮJ̷��^�c
r��18pA��
���;' pr�"p>��ˠ�8�D�Rk�F���I>ɼ������^gf��ͥe`�@cB^��<޾e͖���{���M��O1�)>�.N�xt�Cb)f�3�� 0YT�����ލ�pD�~��zu*�2����ѼX֔�Q/�O� ��gs	rhp4���]?�Z�
{��<S��>k�ؘ�\%i)�ɡ�����F::{#Kڤ����	I�h�����Lފ���{G�9��6tR= ��}���=��$����Aڇ
�����}�-@w����i�֪��2DR����J�e<U�(�Z�|6l=�0�+Ƞ�S0��M�ȭ�~\��Y�:���V���f���:=�e^��D�J�A�h<����Z�V;�+�鎎4�3��{ �)�;��[��9����gaY^{s<������1��|>�7B޿ �Y�<�,�˙�oH=(��>�SB�M����,�1Vu�£Z�W���G�����+
s/i���2C�a>�h���ai2f�D�DCh�Ѻ\��;*U�Ρ�Uz�N�}��z3�λ�F�l��dY��8��"cp���-1�4L���2d��5q0<:�n��t˚.L���a�cM�D�:�G6O�?�.>Z.NṮR�\Q.y�-&.�5��_GV'SV���ň��dM{b��i��اϡH5w��gkT�l�L�b���ks-x&��к��)�;ݑ�<�7s`̩��m)5Z�|�b��r�$ȨQ��g(�����+�wJ嘽\%i)�ɡ�����F::{#Kڤ����	I�Ւ7��<g$g�]�(mxf��	�
4���]��fJ����h�e!PyP�!ۙ	dDwL�#���6��#Ppw��K�m�n �ݜ	���ik���^nj�� ��뷦���9��H�+A�����1hy7ы������I�{R���l5)���A|U��?�,k�-��w ���!�`I���0/��w�bQ�����8����WZ��Ulhc���~�b��WH���U�F��L�K3K7qOA���Q�~�N��ۦP@ �@�C%�	�7� ��h0d4�����1)���@}�;c�ٙN�`��q�-���@r]�t��PB��"yn{�Q$�i���G?���Uc���y�N͓8��MV�����Cy�ƴ���a��a9�bҳ��'������H !�+؈_��R��<������%�8��º�1�$3���*�[��;d�9l��� �@n+C'y����ͲH5�eر	��*"��\�E٩��t����:�z�5�g6԰���dDA��Gjx�$�Ȉ.�VН�c��Z<���Տ5��W������%�s���U��\�<�����z-��"nR��m��E%:C���1�ǯ���Z�f�������-*dK��E�z�j�G~��KY۰�W�:@M>�@�Z��-��gA�hN!)�E�)}��n��j��m%�D�?�T�A^;1�m�Zt9^F]!R!�L���a��O�6��4����4�=Ps�a��O��KU�̺����I�D�U'[��|�%�x�#Qm��9��ղ.���:���d&�����90�A7� +�,�����U�g��Y�9u����Ā�s�z�_�O�((�`xW�{$;i1���m�I�@��f��Gof�k)�������>EH�C��n���\��iae�JY�d��Y���x ��z5��kJ@�X3���\�Q�v*muI9�`�\��\�M����_��;Ƌ�()���3Y�p�1�߳�\�vq 0_g���6F0]KKx�5g���\�G�!�2#���.hAi#M�,������~9`�X>^x{Ю����~݄��wm���b��Xr�ǈ��Y(�Z(�_��0jZ6���f�J���ª�����S�<�h��%��$z�֞���S�*`�4G闣*1���)�T�y��!s���f�
3�# ���N6ѳζ�}%��0�(�����m�J�E
~#��-�(a���[w30�������i���w�^U��8H�KH��H`9<�/���5	��oz�}��Du�BҺ:�j��£5�$a���� `�/�����{�e�j������#���m+�Ώ�A���%df%��/Z}��<կ>(��U�����ca�1�q#4j�$��䂯}-~�u�]؎�ߖ��l2����=������2��;<�F��-W�DFw{��h��0��ZBb`Ė  %Ko�=V�(�~��x5��=xh�Rk��l���Jc`�oE_g@��P�����j%����UC��g�r"�Ǔ��8~��u`�x���Ƕ.�̲��p3��'���\���ڬ6��]���GeMOF�,޴��͇~ۄ��W�����O�_��Fj�-[H_��.��>��ɫ`������{�|90|�%1{�U{d!\_&!�^�|��ì��[�Vt$8�&��"��@���[P�L��ѹ���RY��D��-j������6�#lؗ���9��F�����/B�5����!8�	fY��M��FQ��-D���@_}T
�&�"��!�i(?��ЙQ�b�FTi��\s�CTJ�V������U�M�l������+R��UfƱ�nHd��4b0>����)��۰)X���_!�]C��6����<Tk��Q"@,I3;��!��R*�^0����:���a���
4��C�@�����
�$������ۍ֨����}(���s�J�a�N���\��fD�~&�Y�u&]�*�$"H8��Jҟ+.�U���KB����-2�`<��S�ǎz��*����G)�����(��pm����`(��\ХAx}0b�U�]��elE����(�A��P�^F��4��I*
!��q�Ȭ�r�L3��C�� �ɕ$4����4�4��v?���Os����쳒�`��I��"˼J.`�8�U���ի7��u\J����l5)���A|U��?�,k�-��w ���!�`I���0/��w�bQ�����\P��Wۼ�.��L��(��aʳ�E���Y�W��r��}�:-	�UPd�S��-^�&�Nǩuj���a؈Q������lV��j���S�Y���]��f���AK>s G�&
�9�"D#�2�N`B��}rj
3�����nK�Gb����G.<2�X�I*�Ѡ�2�/����AW�?i`w�u�g��ר��eo ���i�)��,��n��')���˻�"�x�QZ��9կ�k��Z]V�V�1?�-,�fI�ě�E���E��8!M��eIt�cEk�i�W��*Z$�[4���p��5��^����s�)��yf����f3ʩZ��Kn!�ߢ�p��� �B7��1iF�X����T�U���},�q�a�ф�Ώ��Z�_Y�`�m"9j�<�DN����M܃�����Bo�m��Ÿ-��.��ȭ*Co�=e��3�
�[�֏^G�{3#g���D�K» ��5V	pC5x��5���[d��0��i��b`Ė  %Ko�=V�(�~��x5��=xh�Rk���T1N�����ݞ���P�����j%����UC��g�r"�Ǔ��8~��u`�x���Ƕ.�̲��p3��'���\���ڬ6��]���GeMOF�,޴��͇~ۄ��W�����O�_��Fj�-[H_��.��>��ɫ`������{�|90|�%1{�U{d!\_&!�^�|��ì��[�Vt$8�&��"��@���[P�L��ѹ���RY��D��-j������6�#lؗ���9��F�����/B�5����!8�	fY��M��FQ��-D���@_}T
�&�"��!�i(���P��;�^�Ng����f�
z�Ic�߿�A�+����;�T�߫*�Ү�a<�Pu,�hr��u2�[XZs�[&�̐R�J(��^fG�5��__9 ��_Ӥ��g�|�Lǁ9۰e���!�$=�87-�ӂ�-�q��YY����R$Vd�*�Ǩ���71b��]�MH��Y!��WF���7����d�i����]�"����u��� �Cܾ����T!O�	<��\T�WQ�	�V̷/���Xj��9����w]�h>�&'�h.�$҂���ǂ͐z.�����>��G�ͺd��{��e<U�(�Z�|6l=�0�+Ƞ�S0��M�ȭ�~\��Y�:���V���f���:=�e^��D�J�A�h<����Z�V;�+�鎎4�3��{ �)�;��[��9����gaY^{s<������1��|>�7B޿ �Y�<�,�˙�oH=(��>�SB�M����,�1Vu�£Z�W���G�����+
s/i���2C�a>�h���ai2f�D�DCh�Ѻ\��;*U�Ρ�Uz�N�}��z3�λ�F�l��dY��8��"cp���-1�4L���2d��5q0<:�n��t˚.L���a�cM�D�:�G6O��p��'��d���YnKB,�.�d��U��$��e e�	�Zϥ����CC% �j55EI
Fa$}�����+�T[qٷsE�@gZq,��͌a�����	և͒��Т�"/<��SC�{�0��s �)p�{���ۧRR8�{v�R9�iڑ�r�P_s���OgRPd�(o��+m���e�U�d��s�0_�����"6�N$]X�J\S��R�n�D����0�~K�����;K|)��ۚ��D0�i� .��qD���s)̀lU�r��<��vs��������ìD�*�q�l,��Q"!�2M�qh��-�^䰽�" η͡Z�g8�W<φa_!
�{-�'�T`�A���lkg�3�&�IG=.0��}���eO��QX��>��3^|�%5�����4����������▖d#FullMData
� Sld_FullData �� first_member_ptr �� alumni_members �� ref_part_table �����
name TC02 �type �revnum ��dep_id �attributes  � simpl_rep ��feat_id �� next ��� first_xsec_ptr ��� lang_ptr ��interact_on �� first_csys_ptr ��� dim_array �r����������������������������������������������������������������������������������������type �dim_type �feat_id \�attributes �� dim_dat_ptr ��value �bck_value � tol_ptr ��type 	�tol_type �dual_digits_diff ���value ���digits ��nominal_value ��bound �bck_bound �� bck_tol_ptr ��value ���datum_def_id ���places_denom �bck_places_denom �dim_data_attrib  � dim_cosm_ptr ��� p_sld_info �� attach_ptr ��parent_dim_id �� line_array ��,��� text_array ��-���text_type =�
text_value  �txt_attrib �owner_id T�height �width_factor � format_info ��thickness ���-�:X T�� symbol ���type �text(i_val) T���\���	�������� ������,���-��-���.= U���-�:Y U���6�U�\���	�������� ������,���-��-���.= V���-�:Z V���6�V�	\���	��#���������#�������� �����6�W�	\���	��#���������#�������� �����X�	\���	��#���������#�������� �����Y�a���	�������� �����,��,���-��-���.= Z���-�:X Z���6�Z�a���	�������� ������,���-��-���.= [���-�:Y [���6�[�a���	�������� ������,���-��-���.= \���-�:Z \���6�\�	a��-Ek��Q�-Ek��Q��	��#�������-Ek��Q���#�������� �����6�]�	a��.,�.,��	��#�������.,���#�������� �����^�	a��/ / �	��#�������/ ��#�������� �����_�d���	�������� �����,��,���-��-���.= `���-�:X `���6�`�d���	�������� ������,���-��-���.= a���-�:Y a���6�a�d��HC HC �	����HC ����������,���-��-���.= b���-�:Z b���6�b�	d��FB�k*�ERFB�k*�ER�	��#�������FB�k*�ER��#�������������6�c�	d��-X�;ՙ$-X�;ՙ$�	��#�������-X�;ՙ$��#�������� �����d�	d���	��#���������#�������� �����e�g���	�������� �����,��,���-��-���.= f���-�:X f���6�f�g���	�������� ������,���-��-���.= g���-�:Y g���6�g�g���	�������� ������,���-��-���.= h���-�:Z h���6�h�	g��-;���Q�-;���Q��	��#�������-;���Q���#�������� �����6�i�	g��-2�\(��-2�\(���	��#�������-2�\(����#�������� �����j�	g��/ / �	��#�������/ ��#�������� �����k�j���	�������� �����,��,���-��-���.= l���-�:X l���6�l�j���	�������� ������,���-��-���.= m���-�:Y m���6�m�j���	�������� ������,���-��-���.= n���-�:Z n���6�n�	j��HD�HD��	��#�������HD���#�������������6�o�	j��/4 /4 �	��#�������/4 ��#�������� �����p�	j���	��#���������#�������� �����q�ipar_num � � smt_info_ptr �� cable_info_ptr ��id ��type ^�ref_harness_id ��cable_assem_id ��cable_assem_simp_id �� logi_info ��work_subharn_id ����parent_ext_ref_id �� p_frbn_info ��
log_info_src_name ��log_info_src_type  � cable_report_info ��
cable_jacket_report_name DEFAULT �
cable_shield_report_name SHIELD �
cable_node_report_name - � comp_info_ptr ��� prt_misc_ptr �� shrink_data_ptr �� expld_mv_info ��� state_info_ptr �� alumni_feats �� p_mdl_grid �� mdl_status_ptr �� appl_info_arr ��S���id ��type 7� post_reg_outline ��outline �Foʐf���-oʐf�����S����S��� ��� ribbon_info_ptr ��� p_xref_data �� first_analysis_feat ��� da_info �� setups ��first_free �� id_map_info_ptr ���faceted_rep  � sw_info ��offset �-`��]�G�-`�\`v�-:�o�03�-I]����t-;��]x@-M)��*�)-8/��a^�-D>#���#NeuPrtSld
� Sld_NeutPart �� neut_part ��#NeuAsmSld
� Sld_NeutAssem �� neut_assem �� assem_top ��revnum  �accuracy �outline ���accuracy_is_relative  � mass_props �� time_stamp �� colors �� attributes �����
name DESCRIPTION � value ��type 3�
value(s_val)  �ref_id ���ref_type ���MODELED_BY �3 ��NB �3TC02 ��NN �3TC02 ��NT �3Toggle Clamps - Push-Pull ��NBSYN �3$PN. ��LINA �3TC02 ��PSOL_MULTISOLIDPART �2�value(d_val) ����PN �3�TC02 ��MATMB �3JIS SS400 ��ST �3Bright Chromate ��TARE �3330 ��SP �31960 {200} ��ANGLEH �3150 ��STROKE �330 ��TYPE �3TC02 ��PPATH �3tco2_templ.prj ��KT �3DUMMY �� layers ������id 	�
name 01_ALL_DTM_PLN �user_id  �operation ���
01__ASM_ALL_DTM_PLN  �ids ���01__ASM_DEF_DTM_PLN  �01___PRT_ALL_DTM_PLN  ��01___PRT_DEF_DTM_PLN  �02_ALL_AXES  �02__ASM_ALL_AXES  ��V�02___PRT_ALL_AXES  ��03_ALL_CURVES  �03__ASM_ALL_CURVES  �03___PRT_ALL_CURVES  �04_ALL_DTM_PNT  �04__ASM_ALL_DTM_PNT  �04___PRT_ALL_DTM_PNT  �05_ALL_DTM_CSYS  �05__ASM_ALL_DTM_CSYS  ��\adgj�05__ASM_DEF_DTM_CSYS  ��05___PRT_ALL_DTM_CSYS  ��05___PRT_DEF_DTM_CSYS  �06_ALL_SURFS  �06__ASM_ALL_SURFS  � 06___PRT_ALL_SURFS  �07__ASM_ALL_SKELETONS  �_2_ALL_CSYS  ��\adgj�^7_ALL_FEATURES  \adgj�ZBEZUGSACHSEN  �V� item_names ������
name COORD �obj_id S�obj_type ���DEF-PLANE-AXIS [�ASM_RIGHT �ASM_TOP �ASM_FRONT �AA_1 Y�ASM_DEF_CSYS �ACS0 ]�ACS1 b�ACS2 e�ACS3 h�ACS4 k�DEFAULT �@  �5�DEFAULT �@  ���DEFAULT �@  ���DEFAULT �@   ��� members ������id `�type �
name B_TC02 �
family_name B_TC02 �e1 ���e2 ��e3 ���origin �����cP1_TC02 P1_TC02 ��-Ek��Q�.,�/ �fC_TC02 C_TC02 t7bS�cѶ����9n����9t7bS�c��FB�k*�ER-X�;ՙ$�iP2_TC02 P2_TC02 ��-;���Q�-2�\(��/ �lSTC02 STC02 ��HD�/4 �#ModelView#0
� View ��id  � viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �xyT�ۢ�c�:�+gk���RFW n�:���rv�P�A��?6���]�AmR@�8Q���local_sys �v����Ib�]��A�p�V�D�m����|pΦ�'T�A�D(�����%q"lN���d/@/z^F�9���v� world_matrix_ptr ��world_matrix �v����Ib�]��A�p�V�D�m����|pΦ�'T�A�D(�����%q"lN���d��model2world �v����Ib�]��A�p�V�D�m����|pΦ�'T�A�D(�����%q"lN���d�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���outer_xsec_index ��next_view_id !�simp_rep_id  �
name no name �fit_outline �/Y /UJ�F~RY/� /��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#33
� View ��id !� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k�y9�|��k�y9�|��k/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���� ���� ���outer_xsec_index ��next_view_id "�simp_rep_id  �
name FRONT �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#34
� View ��id "� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ��y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���� ���� ���outer_xsec_index ��next_view_id #�simp_rep_id  �
name RIGHT �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#35
� View ��id #� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���� ���� ���outer_xsec_index ��next_view_id $�simp_rep_id  �
name TOP �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#36
� View ��id $� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys ��9�|��k��9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���� ���� ���outer_xsec_index ��next_view_id %�simp_rep_id  �
name BOTTOM �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#37
� View ��id %� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ���9�|��k�y9�|��k�y9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���� ���� ���outer_xsec_index ��next_view_id &�simp_rep_id  �
name LEFT �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#38
� View ��id &� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys ��9�|��k�y9�|��k��9�|��k/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���� ���� ���outer_xsec_index ��next_view_id �@  �simp_rep_id  �
name BACK �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#1073741828
� View ��id �@  � viewattr ��attr_arr � @ � mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys ����� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���outer_xsec_index ��next_view_id ��simp_rep_id  �
name DEFAULT �fit_outline ��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ���view_angle � explode_state ���model_cosm_rev_num  �zone_id ��#DispDataColor
� ref_color_table ��#DispDataTable
��\2��b��e�;K���%�@�Kfk��f��?�Z��*M���\j{�ܝ��6�2�a�5� �@�Y�v��b��2�(�t,�f�7R��ً� 	1���G+���!��<j�z���^@CNЕt����M?���5A�͒\
�h�K5tKdA�GK0K�@Am�sM;Б��mgȦ�S���ס�x\�s�t��Y?B�.\\y�?it�D���B)Bn���;�Ej9�˗����^��^	����5��A�osE��h�#.᱋�G!�{��u3�cu���e�*]ꌨ?�
'���~�c�Ejf����S'��a�){~���6�}g�*��F�AȹT5S3�W�.�U2AҦJ���;;�zD���mndv��f�����G�TN�X���2���ǋ�w���W�F�V�ۜ���t�x@���Ko}>�G�x�܊�<��*�!27$�d�%wiN���,s�	X�<��`�#�cL���.������U��I&iqq�/�ja&���pz�c����ͮ��J�z7�|F[ۋ�1֜�����e���,cƩ�M3M�h� #AssMrgPrt
� assem_merge_parts ��#UD~PSOL~GENERAL
� Pro_User_Data ��slot_id �data_id PSOL~GENERAL �data_type �data_length ��� Union_userdata(stream_val) ��stream ���PSOLDOCAT�R{MIDENT{"{C:\�Program Files\�MISUMI20�80904V1\�software\�.�\�data\�23d-libs\�misumi_usa_inc\�metric\�inspection_jigs\�tco2_asmtab.prj},162 {VTYP=3D},{STYP=AS�TAB},{NB=TC02},{LINA=TC02},{N�=TC02},{DATE=07.08.20�8 21:46:31},{GEOMDATE=05.02.20�8 1�:52:46},{VERSION=},{LINEID=10},{LINESUBID=-1},{WBVAR=} �{NB=TC02},{LINA=TC02},{KT=DUM�Y},{PN=TC02},{MATMB=JIS S�40�},{ST=Bright Chromate},{TARE=3�0},{SP=1960 \�{20�\�}�,{ANGLEH=150},{STROKE=30},{TYPE=TC02},{P�ATH=tco2_templ.prj}"}� �#IndentTag
� Fi��/��:�;�qD��`/�y%���1_�4�U�bַo�C�բ�!ii�!y�L~�8�/.��.W�^_:t��ɣ���%ǜ�f8��e�6��~êhtfS�4W���K �D��{�D�[�u�>D��Q�1��B`�г�����*zqZ7.D{���*,NR�|�`�p?�5r���k�F�Xd'����[�6';��V�a�)���:���ƊuE�X�x�ӑ?�ש5����q4� ��j��y�%���:�Τ�7���J�-�K�M<S��
�AX4���~B�C��l�}���ò�Q�y�m��ƦNV7'�5%�g>�D��L��=��Υ/p+x��CF�V�F)ݍ�w�Y��YBݯ6�o

�ё.H��y�6�3-G��rK��@�4���u%��[���L�@��ۻ���9s���}�O�m�q-�� ���Ge�������]'���[]�~��yyG�����O�\�Β!�~j3���0P��g�_,�V���X��Z{ �;"�0h��B���>W��^+�|ѽ��(0x	f��<M����w"ɴ<e��}-�B��%:�NmKyi�W���f ��&��"��e�1�zC��"�p�Z�r�<t�)2��#q��q�GE��/=�&ϡH[�r˄�Bf���� ϱ\k�L�U�e��bFf���v_ɶ=��'nlO��,=��k� �S�E����9]b���Z]�����>:����M�P ��������["<{P0�;� �j�<El=~U�]�/�\����	�������:]���t0o����6�@@/�뉟�	�0Hg����{w�
���^?�J-�ܯ :���ur�ģY�:�#FeatRefData
� Fe�4/�^)���:z8�O7bM��^!������%�#Y�/b�D�POD�-���"��`ǆ\��k`�m�=��u!�K�L��d 	�;�A�;��͆�v|�|75f$���ԥɸ~���^��L�8t�҃�ZZ&�C�'��ͣ����px��}Ȳ	<�)0V�E���ȴ��D��,�D�-�fT.��u�<��	L��5q):��N���^b�^Ł/�M������7c}�ɉ�c}���e#END_OF_UGC
