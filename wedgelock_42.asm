#UGC:2 ASSEMBLY 1322 620 8 1 1 15 2500 2005380 000017fd \
#- VERS 0 0                                                          \
#- HOST                                                              \
#- LINK                                                              \
#- DBID                                                              \
#- REVS 0,                                                           \
#- RELL 0,                                                           \
#- UOBJ_ID 1147742109 1233213249 1987995311                          \
#- MACH _Windows                                                     \
#-END_OF_UGC_HEADER
#P_OBJECT 6
@P_object 1 0
0 1 ->
@depend 2 0
1 2 [4]
2 2 
@model_name 3 10
3 3 WEDGELOCK_42_MID
@model_type 4 1
3 4 2
@revnum 5 1
3 5 2361
@bom_count 6 1
3 6 1
@dep_type 7 1
3 7 10
@stamp 8 0
3 8 
@major_vers 9 0
4 9 
@vers 10 1
5 10 0
@alias_name 11 10
5 11 
@minor_vers 12 0
4 12 
@vers 13 1
5 13 0
@reason 14 10
3 14 
@comp_ids 15 1
3 15 [1]
4 15 109
@comp_dep_types 16 1
3 16 [1]
4 16 2
2 2 
3 3 WEDGELOCK_42_PIN
3 4 2
3 5 860
3 6 1
3 7 10
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 102
3 16 [1]
4 16 2
2 2 
3 3 WEDGELOCK_42_BOTT
3 4 2
3 5 814
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 100
3 16 [1]
4 16 2
2 2 
3 3 WEDGELOCK_42_TOP
3 4 2
3 5 822
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 101
3 16 [1]
4 16 2
@dep_db 17 0
1 17 ->
@dep_db 18 0
2 18 [7]
3 18 ->
@to_model_id 19 1
4 19 -3
@dep_type 20 1
4 20 4
@obj_id 21 1
4 21 7
@obj_type 22 1
4 22 242
3 18 ->
4 19 0
4 20 2
4 21 109
4 22 3
3 18 ->
4 19 1
4 20 2
4 21 100
4 22 3
3 18 ->
4 19 2
4 20 2
4 21 101
4 22 3
3 18 ->
4 19 3
4 20 2
4 21 102
4 22 3
3 18 ->
4 19 0
4 20 4
4 21 8
4 22 242
3 18 ->
4 19 3
4 20 4
4 21 9
4 22 242
@model_db 23 0
2 23 [4]
3 23 ->
@model_t_type 24 1
4 24 2
@model_name 25 10
4 25 WEDGELOCK_42_MID
@generic_name 26 10
4 26 
@stamp 27 0
4 27 
3 23 ->
4 24 2
4 25 WEDGELOCK_42_BOTT
4 26 
4 27 
3 23 ->
4 24 2
4 25 WEDGELOCK_42_TOP
4 26 
4 27 
3 23 ->
4 24 2
4 25 WEDGELOCK_42_PIN
4 26 
4 27 
@usr_rev 28 0
1 28 [3]
2 28 
@name 29 10
3 29 ADMINISTRATOR
@mod_time 30 0
3 30 
@tm_sec 31 1
4 31 40
@tm_min 32 1
4 32 26
@tm_hour 33 1
4 33 5
@tm_mday 34 1
4 34 3
@tm_mon 35 1
4 35 11
@tm_year 36 1
4 36 104
@action 37 1
3 37 1000
@comment 38 10
3 38 Hostname: 'NC8000'
@rel_level 39 10
3 39 
@rev_string 40 10
3 40 
2 28 
3 29 LGIBMUSER
3 30 
4 31 14
4 32 20
4 33 1
4 34 20
4 35 0
4 36 105
3 37 1000
3 38 Hostname: 'SORBON'
3 39 
3 40 
2 28 
3 29 SMLEE
3 30 
4 31 47
4 32 31
4 33 7
4 34 12
4 35 1
4 36 107
3 37 1000
3 38 Hostname: 'SORBON'
3 39 
3 40 
@revnum_std 41 1
1 41 524
@flag 42 1
1 42 0
@name 43 10
1 43 WEDGELOCK_42-8_INST
@inst_hidden 44 1
1 44 0
@old_name 45 10
1 45 NULL
@user_symbols 46 0
1 46 [1]
2 46 ->
@class 47 1
3 47 1
@access 48 1
3 48 0
@changed 49 1
3 49 0
@copy_of_generic 50 1
3 50 0
@name 51 10
3 51 PTC_COMMON_NAME
@value 52 0
3 52 
@type 53 1
4 53 51
@value(s_val) 54 10
4 54 wedgelock_42.asm
@id 55 1
3 55 -1
@type 56 1
3 56 -1
@a_inst_obj 57 0
1 57 [2]
2 57 ->
3 2 [4]
4 2 
5 3 WEDGELOCK_42_MID
5 4 2
5 5 2361
5 6 1
5 7 10
5 8 
6 9 
7 10 0
7 11 
6 12 
7 13 0
5 14 
5 15 [1]
6 15 109
5 16 [1]
6 16 2
4 2 
5 3 WEDGELOCK_42_PIN
5 4 2
5 5 860
5 6 1
5 7 10
5 8 
6 9 
7 10 0
7 11 
6 12 
7 13 0
5 14 
5 15 [1]
6 15 102
5 16 [1]
6 16 2
4 2 
5 3 WEDGELOCK_42_BOTT
5 4 2
5 5 814
5 6 1
5 7 2
5 8 
6 9 
7 10 0
7 11 
6 12 
7 13 0
5 14 
5 15 [1]
6 15 100
5 16 [1]
6 16 2
4 2 
5 3 WEDGELOCK_42_TOP
5 4 2
5 5 822
5 6 1
5 7 2
5 8 
6 9 
7 10 0
7 11 
6 12 
7 13 0
5 14 
5 15 [1]
6 15 101
5 16 [1]
6 16 2
3 17 ->
4 18 [7]
5 18 ->
6 19 -3
6 20 4
6 21 7
6 22 242
5 18 ->
6 19 0
6 20 2
6 21 109
6 22 3
5 18 ->
6 19 1
6 20 2
6 21 100
6 22 3
5 18 ->
6 19 2
6 20 2
6 21 101
6 22 3
5 18 ->
6 19 3
6 20 2
6 21 102
6 22 3
5 18 ->
6 19 0
6 20 4
6 21 8
6 22 242
5 18 ->
6 19 3
6 20 4
6 21 9
6 22 242
4 23 [4]
5 23 ->
6 24 2
6 25 WEDGELOCK_42_MID
6 26 
6 27 
5 23 ->
6 24 2
6 25 WEDGELOCK_42_BOTT
6 26 
6 27 
5 23 ->
6 24 2
6 25 WEDGELOCK_42_TOP
6 26 
6 27 
5 23 ->
6 24 2
6 25 WEDGELOCK_42_PIN
6 26 
6 27 
3 41 524
3 42 0
3 43 WEDGELOCK_42__6
3 44 0
3 45 NULL
3 46 [1]
4 46 ->
5 47 1
5 48 1
5 49 0
5 50 0
5 51 PTC_COMMON_NAME
5 52 
6 53 51
6 54 wedgelock_42.asm
5 55 -1
5 56 -1
@appl_data 58 0
3 58 ->
@bounding_box 59 2
4 59 [2][3]
$C00A0E1F15846018,C044027B8AF1DF9B,C00A0E1F15846018
$400A0E1F15846018,404413C339065A7B,4011C4802E993A49
@ver_key 60 0
3 60 ->
@time 61 1
4 61 1148957643
@random_num 62 1
4 62 604519456
@hostid 63 1
4 63 1987995311
@last_read 64 1
3 64 -1
2 57 ->
3 2 [4]
4 2 
5 3 WEDGELOCK_42_MID
5 4 2
5 5 2361
5 6 1
5 7 10
5 8 
6 9 
7 10 0
7 11 
6 12 
7 13 0
5 14 
5 15 [1]
6 15 109
5 16 [1]
6 16 2
4 2 
5 3 WEDGELOCK_42_PIN
5 4 2
5 5 860
5 6 1
5 7 10
5 8 
6 9 
7 10 0
7 11 
6 12 
7 13 0
5 14 
5 15 [1]
6 15 102
5 16 [1]
6 16 2
4 2 
5 3 WEDGELOCK_42_BOTT
5 4 2
5 5 814
5 6 1
5 7 2
5 8 
6 9 
7 10 0
7 11 
6 12 
7 13 0
5 14 
5 15 [1]
6 15 100
5 16 [1]
6 16 2
4 2 
5 3 WEDGELOCK_42_TOP
5 4 2
5 5 822
5 6 1
5 7 2
5 8 
6 9 
7 10 0
7 11 
6 12 
7 13 0
5 14 
5 15 [1]
6 15 101
5 16 [1]
6 16 2
3 17 ->
4 18 [7]
5 18 ->
6 19 -3
6 20 4
6 21 7
6 22 242
5 18 ->
6 19 0
6 20 2
6 21 109
6 22 3
5 18 ->
6 19 1
6 20 2
6 21 100
6 22 3
5 18 ->
6 19 2
6 20 2
6 21 101
6 22 3
5 18 ->
6 19 3
6 20 2
6 21 102
6 22 3
5 18 ->
6 19 0
6 20 4
6 21 8
6 22 242
5 18 ->
6 19 3
6 20 4
6 21 9
6 22 242
4 23 [4]
5 23 ->
6 24 2
6 25 WEDGELOCK_42_MID
6 26 
6 27 
5 23 ->
6 24 2
6 25 WEDGELOCK_42_BOTT
6 26 
6 27 
5 23 ->
6 24 2
6 25 WEDGELOCK_42_TOP
6 26 
6 27 
5 23 ->
6 24 2
6 25 WEDGELOCK_42_PIN
6 26 
6 27 
3 41 526
3 42 0
3 43 WEDGELOCK_42__8
3 44 0
3 45 NULL
3 46 [1]
4 46 ->
5 47 1
5 48 1
5 49 0
5 50 0
5 51 PTC_COMMON_NAME
5 52 
6 53 51
6 54 wedgelock_42.asm
5 55 -1
5 56 -1
3 58 ->
4 59 [2][3]
$C00A0E1F15846018,C044027B8AF1DF9B,C00A0E1F15846018
$400A0E1F15846018,404413C339065A7B,4011C4802E993A49
3 60 ->
4 61 1148957663
4 62 447877595
4 63 1987995311
3 64 -1
1 58 ->
2 59 [2][3]
$C00A0E1F15846018,C044027B8AF1DF9B,C00A0E1F15846018
$400A0E1F15846018,404413C339065A7B,4011C4802E993A49
1 60 ->
2 61 1148957643
2 62 604519456
2 63 1987995311
1 64 -1
#END_OF_P_OBJECT
#Pro/ENGINEER  TM  Wildfire 2.0  (c) 1988-2002 by Wisdom Systems  All Rights Reserved. M170
#UGC_TOC 1 32 81 17#############################################################
SolidPersistTable acd 304 1 1322 10 2 4558 349f#################################
SolidPrimdata dd1 fb 1 1322 10 2 6684 c7f9######################################
BasicData ecc 32d 1 1322 9 -1 431b 968a#########################################
BasicText 11f9 32e 1 1322 9 -1 406d f81e########################################
GeomDepen 1527 16d 1 1322 9 -1 922b 7bb6########################################
DispCntrl 1694 466 1 1322 9 -1 f0c5 fa28########################################
DisplData 1afa 29 1 1322 9 -1 0cac b9ca#########################################
LargeText 1b23 1ade 1 1322 9 -1 12a4 bc23#######################################
FamilyInf 3601 4fd 1 1322 9 -1 c785 3e28########################################
VisibGeom 3afe 33d 1 1322 11 -1 8f56 f71e#######################################
NovisGeom 3e3b 20a 1 1322 11 -1 febd e32b#######################################
ActEntity 4045 6cf 1 1322 9 -1 c03e 5cb5########################################
AllFeatur 4714 26d7 1 1322 11 -1 39a5 91a3######################################
FullMData 6deb 16c2 1 1322 9 -1 62e6 6faf#######################################
NeuPrtSld 84ad 29 1 1322 9 -1 0cfc be04#########################################
NeuAsmSld 84d6 5a0 1 1322 9 -1 fede 9dc4########################################
ModelView#0 8a76 46d 1 1322 9 -1 d545 86c4######################################
ModelView#30 8ee3 350 1 1322 9 -1 57f9 9b46#####################################
ModelView#40 9233 3c3 1 1322 9 -1 9957 ed58#####################################
ModelView#41 95f6 408 1 1322 9 -1 9d10 b9f2#####################################
ModelView#43 99fe 419 1 1322 9 -1 affd ad12#####################################
ModelView#44 9e17 410 1 1322 9 -1 b906 e945#####################################
ModelView#45 a227 403 1 1322 9 -1 ab0f 2647#####################################
ModelView#46 a62a 38d 1 1322 9 -1 6f25 1d14#####################################
ModelView#51 a9b7 3c4 1 1322 9 -1 9d36 b6e6#####################################
ModelView#1073741828 ad7b 40f 1 1322 9 -1 a1aa 6bae#############################
DispDataColor b18a 23 1 1322 9 -1 08d5 57cf#####################################
DispDataTable b1ad 348 1 1322 12 2 9854 a41e####################################
AssMrgPrt b4f5 21 1 1322 9 -1 09c5 6a48#########################################
IndentTag b516 36f 1 1322 11 -1 b97e e4f2#######################################
FeatRefData b885 100 1 1322 11 -1 6ff0 8e22#####################################
################################################################################
#SolidPersistTable
�� �9���2r椙C��0bؔ0\��|IC  �a��!��2y� �4o~y#F�p��)cF&M:yx��M�6=��4��c��3��L��)R�S�y�N�y��gC%U6iܔY�'�԰�X�8��7r��|C.�4mQ�W,��t��SG�[�*���3M2��(�*H�a�q3f0X�)��al��7p2�I������a�.s�M����r��9�{s�Z x�ܛ���@��y��3'Ԇ@%����:\��1}�1��.���v��{��È*       ,�CP�1�v�P��Nn\&�rt�!Gb��}��qVZ �Gz|�Z"�� �0  � ���GZg|ڌu0�]��Qst��}��Vr�@<7���� ���>�4�]v�ӒM�E��Ie ��@݋b� ��@QG��W�)��<҉3��g �X@G�@��X��="$���`��fG��G� �c�i� \u�iZ�rxQ�	k�G�����OA0�N���^�y��{��gz.��\He���E�S�.��a��х8فY[ŉ�o����}��6��Y���1�:`�wA�㑬c��F�y�l�AZr�y��oB(!�)�k$�Jj����P�JV�ƙ�Q���0K-a���!�C�1w�==�j]A�ƤT���PE�f�OJ�i �gQ�q�+y�zz�	~ �^�`�e�-b*>h�����u �, #SolidPrimdata
�� �9��9iڐ	C'̗3r�ԁ�6e �(��A�
:�(qΗ0r� ����},x0�#�0� 6o�|��M�/`  8���2fL��G��{^n��ԩ���p-XFΜ4s��y�\�7b�P�#p	��-��T�fȖq3ほ/~IC��3V�&Ȇc޸a��Y �x��X��W�p���NC-wvt�Cg&�4t�������č�s @�q�X׊�w#BasicData
� Sld_BasicData ��tab_key  � tab_key_struct ��type H� tab_key_ptr ���� cmprs_tabkey ���type �� revnum_ptr ��geom_revnum ���cosm_revnum `� tol_default_ptr ���type 
�value �(�bM���(6��C-#�����h�#�������ang_value ���lin_digits ����ang_digits � ��standard  � iso_tol_tbls_ptr ��datum_count � �axis_count �insert_layer_id ��layer_count �rel_part_eps (S�]� ��model_eps �fixed_model_eps �assembled_count  �part_type �first_ent_id �$]�first_view_id  �outline ���(�\FC�\(���(�\��(�\-C��
=o�+��Q��dim_num � geom_lib_ptr ��attrib_array ��    �� A  ��max_part_size �
model_name WEDGELOCK_42 � disp_outl_info ��disp_outline ���`FD{��ߛ��`��`-D�9Z{�Ā.�:I� sel_outl_info ���outline ���`FD{��ߛ��`��`-D�9Z{�Ā.�:I�#BasicText
� Sld_BasicText �� assem_mass_prop_ptr ��type  �density �init_dens �rel_density �init_rel_dens  �aver_density  �vrevnum ���volume -���8p�cg �#��:�6P��1P�!ln{L C�inertia �-�m�Px{�A_M��-�A��K�s6A_M��-��A03�*��b-��xP\�A��K�s6-��xP\�-��`N ��eps #�����h��surfarea -�#��]���csys_id �� mass_prop_ptr ���   ����� assign_mp_ptr ��
mat_name_ptr �� unit_arr �����type �factor (�(P�B�
�
name MM �unit_type  ���-�9D�݁TONNE �(24Vx���SECS � p_unit_info ��� custom_unit_arr ����������� xp_custom_sys_unit ������_id ��_obj_type �
name Custom �type �
unit_names �mm kg  sec C rad �_unit_ids ��!�
principal_sys_units �millimeter Newton Second (mmNs) �_principal_sys_units_id 3�history_scale ��_version �_revnum � dsgnate_data ���#GeomDepen
� Sld_GeomDepend ��frst_geom_ptr  � fem_data ��first_quilt_ptr  � topol_state ��last_regen_feat_id f� incr_time_stamp ��time �D{�5�random_num ��H9�hostid �v~f�� asm_comp_map ���comp_id ��attrib  �
uniq_name �� asm_map_sys �� time_stamp ��vis_revnum ��invis_revnum �� geom_own_tbl ���num_mech_connection  �num_feat_regens �$�first_lo_ptr � �#DispCntrl
� Sld_DispCntrl �� color_prop_ptr �� layer_array �
����id 5�type u�user_id  � weird_wline_ptr ��
weird_wline 07_LAYER_ASSEM_MEMBER � layer_contents ��� layer_compiled �� layer_rbd �� cond_block ��saved_operation ��deftype_num ��attributes � ���<u �00_LAYER_AXIS �� item_data ����� item_data �����type � id_data �����id ����$�revnum ���
���������������������$������ �?u �03_LAYER_DATUM ��
����� �Bu �01_LAYER_CSYS ������������������
��������������������� �Hu �02_LAYER_CURVE ��
����� �Ku �05_LAYER_POINT ����� �Lu �13_DEFAULT_DATUM ����������������������	����
����������������������	������� �Uu �7_ALL_FEATURES ��������
������������ �Vu �1_ALL_PLANES ���������
������������ �lu �1211 ��
�����oper_arr ���� rules_arr �����sel_option component �rule_type � rule_value(ddt_basic_name_data) ��exact  �
sel_wnames �JACR* �comp_oper  �selopt_index ��lookby_index � scope ���scope_type �F�scope_id ��memb_num  �own_ref_id ��incl_submodels �sel_option �component ���� �#DisplData
� Sld_DispData �� disp_data ��#LargeText
� Sld_LargeText �� names_table �����id �type �
name ASSY_SIDE_YZ �	attr  ���	ASSY_TOP_ZX  �ASSY_FRONT_XY  �ASSY_AXIS_X  �ASSY_AXIS_Y  �'ASSY_AXIS_Z  �ASSY_CSYS  ��@  �5DEFAULT  ��@  ��DEFAULT  ��@  ��DEFAULT  ��@   ��DEFAULT  � relsymdata ��� relsym_set_arr �����id ��type �	ui_order_fixed ��� symtbl �����
name  �sym_class '�attr � � value ��type 3�
value(s_val)  � bak_val ���3 �access_fn_idx ��dim_id ��appl_int  �
description �� sym_rstr �� sym_bak_rstr ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �2�value(d_val) /Y ���2/Y ��� ����VOLUME '� �2-���8p��2-���8p��� ����SURF_AREA '� �2-�#��]����2-�#��]����� ����MP_X '� �2��2��� ����MP_Y '� �2�1P�!l��2�1P�!l��� ����MP_Z '� �2n{L C��2n{L C��� ����WEIGHT '� �2-���8r��2-���8r��� ����PTC_COMMON_NAME '� �3�wedgelock_42.asm ��3wedgelock_42.asm ��� ����MC_INTF '� �311/22/2005 326 ��311/22/2005 326 ��� ����DIM_L '� �2�-S�������2-S�������� ����PART_NAME '� �3�Wedge Lock ��3Wedge Lock ��� ���	attr  �id ��set_id ��unit_id ��origin ����� ������PART_NUM '� �3 ��3 ��� �� ������ ������L_NUM '� �2�/ ��2/ ��� �������DIM_M '� �2-6�ffffd��2-6�ffffd��� ���� '� �2����� ���� rel_data ���attr � relset_code_tree �� relcode_tree_child ��#����node_type � node_data(simple_data) ��type � node_val(loc_handler) ��own_id ��own_type �relset_idx  �sym_id �
src �?0 � relcode_tree_next ���#��#��$�� node_val(const_val) ��3� �,("" ��$�� node_val(func_call) ��code -�ext_func_id ��postfix_id ���,MP_MASS �� �node_val(op_code) ���,= �� node_data(line_data) ��code ���line_no �
src ) ��-��#��#���$�%��'�� ?0 ���#��$��.�3 ("" ��$��/�.��MP_VOLUME �� �3��= ��4��) ��#��#���$�%��'�� ?0 ���#��$��.�3 ("" ��$��/�/��MP_SURF_AREA �� �3��= ��4��) ��#��#���$�%��'�� ?0 ���#��$��.�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" ��$��/�*��MP_CG_X �� �3��= ��4��) ��#��#���$�%��'�� ?0 ���#��$��.�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" ��$��/�+��MP_CG_Y �� �3��= ��4��) ��#��#���$�%��'�� ?0 ���#��$��.�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" ��$��/�,��MP_CG_Z �� �3��= ��4��) ��#��#���$�%��'�� ?1 ���#���$��� ?1 ����.�4�value(i_val) ��,2 ��$� �3/ ��#���.�2�.9f�,25.4 ��$� �3* �� ��= ��4�����#��#���$�%��'�� ?1 ���#��$��� ?1 ����.�2-J�fffff53.3 ��$� �3- �� ��= ��4�����#��#���$�%��'���OL ?1 ���#��$��� ?1 ����.�2-3�����19.1 ��$� �3- �� ��= ��4��	���#��#���$�%��'���OL?1 ���#��$��� ?1 ����.�4�82 ��$� �3/ �� ��= ��4��
���#��#���$�%��'���OL?1 ����� ?1 ��$� �3��= ��4���� �  ��� relset_bak_code_tree ��#��#����$�%��'�� ?0 ��#��#��$��.�3� ("" ��$��/�-��MP_MASS �� �3��= ��4��) ��#��#���$�%��'�� ?0 ���#��$��.�3 ("" ��$��/�.��MP_VOLUME �� �3��= ��4��) ��#��#���$�%��'�� ?0 ���#��$��.�3 ("" ��$��/�/��MP_SURF_AREA �� �3��= ��4��) ��#��#���$�%��'�� ?0 ���#��$��.�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" ��$��/�*��MP_CG_X �� �3��= ��4��) ��#��#���$�%��'�� ?0 ���#��$��.�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" ��$��/�+��MP_CG_Y �� �3��= ��4��) ��#��#���$�%��'�� ?0 ���#��$��.�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" ��$��/�,��MP_CG_Z �� �3��= ��4��) ��#��#���$�%��'�� ?1 ���#���$��� ?1 ����.�4�82 ��$� �3/ ��#���.�2�.9f25.4 ��$� �3* �� ��= ��4�����#��#���$�%��'�� ?1 ���#��$��� ?1 ����.�2-J�fffff53.3 ��$� �3- �� ��= ��4�����#��#���$�%��'���OL ?1 ���#��$��� ?1 ����.�2-3�����19.1 ��$� �3- �� ��= ��4��	���#��#���$�%��'���OL?1 ���#��$��� ?1 ����.�4�82 ��$� �3/ �� ��= ��4��
���#��#���$�%��'���OL?1 ����� ?1 ��$� �3��= ��4���� �  ������ �����PRO_MP_SOURCE <��~"x �3�GEOMETRY ���3GEOMETRY �� ������MP_DENSITY -��~"x �:� value(NO_val) ����:��� �����G�������G���PRO_MP_ALT_MASS J��~"x �:���:��%� ������������PRO_MP_ALT_VOLUME J��~"x �:���:��%� ����@�����@���PRO_MP_ALT_AREA J��~"x �:���:��%� ����;�����;���PRO_MP_ALT_COGX K��~"x �:���:��&� ������������PRO_MP_ALT_COGY K��~"x �:���:��&� ������������PRO_MP_ALT_COGZ K��~"x �:���:��&� ������������PRO_MP_ALT_IXX K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IYY K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IZZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IXZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IYZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IXY K��~"x �:���:��&� ����N�����N���PRO_MP_MASS =��~�( �2�-���8r��2-���8r�� ������������PRO_MP_VOLUME >��~�( �2-���8p��2-���8p�� ����@�����@���PRO_MP_DENSITY ?��~�( �2 ��2 ��� ����G�����G���PRO_MP_AREA @��~�( �2-�#��]����2-�#��]���� ����;�����;���PRO_MP_COGX A��~�( �2��2�� ������������PRO_MP_COGY B��~�( �2�1P�!l��2�1P�!l�� ������������PRO_MP_COGZ C��~�( �2n{L C��2n{L C�� ������������PRO_MP_IXX D��~�( �2�A0O�<� c��2�A0O�<� c�� ����N�����N���PRO_MP_IYY E��~�( �2-�*}�c>4��2-�*}�c>4� � ����N�����N���PRO_MP_IZZ F��~�( �2�A0JO�*���2�A0JO�*��!� ����N�����N���PRO_MP_IXZ G��~�( �2O�K�s6��2O�K�s6�"� ����N�����N���PRO_MP_IYZ H��~�( �2F��xP\���2F��xP\��#� ����N�����N���PRO_MP_IXY I��~�( �2��2�$� ����N�����N�� ������OL�`   �����D58 "��2-L���������:� p_sym_data_arr ��;���type  � p_data(ext_handler) ��ref_id � loc_handler ��(���� �������D103 "��2-&�ffffd�g��;���< ���� ����D58 "��2-S������:��;���< �	��� ����� ���!��9�� restrict_table ���idx_array ����	attr �� p_alt_sets_tbl �� cfg_states ��� disp_mgr_data �� disp_app_arr ��F���appl_type �5� rep_arr ��H���gen_db_id �@  �gen_db_type �5�gen_db_base_id ��gen_db_gen_attrs  �gen_db_app_attrs  � gen_db_refs �� cra_cis �� item_data ��P��� rep_comp_data ��status � simpl_rep ��set_by  � lw_data ��attrs  � cra_rbd ��� cond_block �� gen_db_runtime ��� gen_db_data ��� rep_data ���spec_rep_id ��attributes � � app_ids ��act_id ��cur_id ��def_id �@  ��F��� disp_style_arr ��b���I�@  ���  ����P���Q��� � �����] ����@  ��� expl_arr ��c���I�@   ���  ��id ��type ���state_type  �cur_stage_ind �� stage_info ��h��� unexpl_subass_info ��� expld_comp_ref ���comp_id �� override_sys �� expld_state �� gen_info ���font �color 
��Y�� ��@   �@   �@   ��� presentation_arr ��q���I�@  ���  ��Y��� disp_ref_arr ���r���id �@  �type �5�aux_id ���r��@  �����@   �� ��@  \���] ���@  �@  �#FamilyInf
� Sld_FamilyInfo �� drv_tbl_ptr ��prt_type �
gen_name GENERIC �model_type � instances �����
name WEDGELOCK_42__8 � drv_tbl_ptr ��WEDGELOCK_42 �
dim_tab_file ��type � info_items �����type �id �owner_id  �
name  �invisible  �ui_col_width  ���    �    �    �    �    �    �    �    �    �    �    �    �revnum � �ui_mdl_name_width  �generic_revnum ���version � values ������type 2�value(d_val) /  � info_values ������2-���8r��2-���8p�2 �2-�#��]���2�2�1P�!l�2n{L C�2�A0O�<� c�2-�*}�c>4�2�A0JO�*��2O�K�s6�2F��xP\��2�attributes �� mass_prop ��type  �density �init_dens �rel_density �init_rel_dens  �aver_density  �vrevnum ���volume -���8p�cg �#��:�6P��1P�!ln{L C�inertia �-�m�Px{�A_M��-�A��K�s6A_M��-��A03�*��b-��xP\�A��K�s6-��xP\�-��`N ��eps #�����h��surfarea -�#��]���csys_id ����WEDGELOCK_42__6 �WEDGELOCK_42 ������    ���    �    �    �    �    �    �    �    �    �    �    �    ��  �������2/ ������4�value(i_val) ����4��4��4��4��4��4��4��4��4��4��4��4��������	�� items ��*����L_NUM  ������    ��    �    �    �    �    �    �    �    �    �    �    �    �����#VisibGeom
� SlD7+6 �<�8�Cq�M�5ü�;j8�H!{��vXy\�_���]f[;��T�6t����� %�	�����^����Xi�����S]��u�3�2�pF�Ow5��Bu�%�)�l��+SrJQ��*I��h��l�@�� ���`��=��4�����I>���ݓ&���6w���M&8ov�GV�����NAj,��K|��>x΢i���6)_'�CCկ	����Z�fh��|Bh�w�0�1��8�z@u=��	��ŨqE�!�!��E �9�_-�i�q�q�P�Ԇ4�ڐ��N��e�џ*:.��d:g�+��&��ԤWc��X�"��Z����y�;��z^�N�*�����6x}h�E�93��R�ӘP8���~�.����e�P�K���\�%�G�8���lR����Lj�`M�[�0����6S ��s�|e[������1QQ;W���V�EQyy���/�>���$��������)�5�k����;c�<j��W9�&� FQ�¡)��a&���ݨM�N���ݓ&���6w��c�K(���.i�}� �Y�¸")�8���kكzM}g.g\h��q���Ol}Tb�`iMl�9pxY�c�ds;�R�TXO�~�����dm������u���2��p�|S��0�+�v�`�Q]FT�Hqŀ����C��B���b�+���E-a�2A�4���
6�s��sX���ZL6.�������F�F�q���o��r�ZSP~�Z,G&� �ʗlb��z-7N�m_Q���r��.�}u`wb�#NovisGeom
� Sl(�]�O���R˦��
�;�Q�A�'�%W����l�f{�(8�-�� ��V)�}��ȇ ����C����(�zk0�$y�4��G�8��>����=px��'X�����z�m���t ཭�3H��p���/oTu�NBth1�vtt�o�ԈA��� �L������7�&h�e�(Z<J���;�eg6T'VH(�J2��qL�. ����}�J�#9ߛ���n�8��q��1�\�����3X�Ǔ�6���k�D.o����Q���FH�~t_��η�
ˏ�&�v?b��] �E�T驢���"~7H�(4�*�k�f�|�&uޫ~���[bWͷa"v���tǇ.F�_x���t�0|$��0)��n"6�ࣜi룭�{���(1(�nj�[���Btگ�Y^uR�����^&�A�JE0僥nB8�$�7���PH�t"����#�vFO�c��K''`�Rb挏���$�o�y����������b�td#ActEntity
� Sld_Entities �� act_feat_ents �����feat_id �datum_id �� entity_ptr(coord_sys) ��type �id � gen_info ��color �sys_type ���ref_csys_type ��radius �local_sys �����feat_id � entity(text) ���text_type "�
text_value ASSY_CSYS �txt_attrib �coord �-`=��t|/z�F���#B�owner_id �height �width_factor � format_info ��thickness ����� entity_ptr(cosmetic) ��type 2�id �� gen_info ��owner_type ��owner_id � text_ptr ��text_type �
text_value ASSY_SIDE_YZ �txt_attrib �owner_id �height �width_factor � format_info ��thickness �� cosm_disp_ents ��� first_disp_ent(line) ��id �� gen_info ��flip ��end1 �FD{��ߛ��`�end2 �FD{��ߛ�Ā.�:I� entity(line) ���*��FD{��ߛ�Ā.�:I-D�9Z{�Ā.�:I���-D�9Z{�Ā.�:I-D�9Z{��`���-D�9Z{��`FD{��ߛ��`� plane_ptr ���attr  ���	�2��	�ASSY_TOP_ZX �����	��`���`��`��/���	��`��`�Ā.�:I��`���	�Ā.�:I��`�Ā.�:I��`���	�Ā.�:I��`��`���0� �
�2���ASSY_FRONT_XY 
�������`FD{��ߛ��`-D�9Z{��/�����`-D�9Z{��`-D�9Z{�����`-D�9Z{��`FD{��ߛ�����`FD{��ߛ��`FD{��ߛ��0� ��� entity_ptr(line3d) ��type �id � gen_info ��geom_id ��end1 ���(�\�;��J�L��end2 ���(�\�;��J�L�A6��C-�orig_len � entity(line3d) ���3���	�6��(�\�;��J�L���(�\�;��J�L���ASSY_AXIS_X -�,^T���-j&U�ܠF�-�EZ&��������6-C��
=o�;��J�L�A6��C--C��
=o�;��J�L��:����	-C��
=o�;��J�L�FC�\(��;��J�L���ASSY_AXIS_Y -t���1!-��@�F�ט��S��$��&���6%�;��J�L���(�\A6��C-�;��J�L���(�\�:�'���	%�;��J�L���(�\�;��J�L��+��Q���ASSY_AXIS_Z -�JK!�V-n<I�ͷ�F�/��*92��#AllFeatur
� Sl>��Ҩ#@�����N���Hh�t��CnU*y	[�����.��^q��m�|nH3o�)�g�2P~{� ����B��֕}��f�r����I�r��%;<
$[���5?�^��H.]��I�1��,@9H����?Zõ�W�%��2�d�*uw2��U�woŕ��ωR��#�#y����8��c�똃�?kx��_!���8?E=^RΦ"͞��m��8!&˘R��)q{����)��������t�y�C �4m7]����,�528��_�-m��ҁ��a�񧥄��.�v�� &��������x^��E)_�U����]ȫM�(��w���<�.C/�)6Ͳo
#w���dn�:NUӐ���3݇��1�]/Y�����ln>fXͨw<��	�����s5��ؽ�J�<�W�A�C��iS�8]_|iЬv&�V����u�s՘��C~��U�gh �^˃46&͝�ߟĆ�r4�d/����1����-��=n������,6�áP �J����`�����G�R��#��dM~��u��_�X5�yd�%�勉y����r���[���7�Wnx禝�oUU��@� �ջ���X��oY�l@������2/_�E�a�	1҇yz�
Th�K� $T��־��='�'�j�)h��Ԧ���yڲ�케3x3�v*V�j�S5��'�n� Q�q��Y����'p�m�~�H߄� ��w{3���}BR� �1�u��esP�#��u�ZJձ��BƎ�־���jٟV��ê�d�����1o�)+R~�����Lqx����u�胡�!�L�v�=�e`ȡ;#{kc���*��N?��n���=M[���E�n#/��l^���|ӓ�^�旦 N�D�G�u�`f��60�̣����w�uJ܉T��%�ŬR��]�%7��#	��A�)�
L��E~�E@-²��x�>���׼�n�c�sp���ƙ�EIk`#X-��Q� ���Np*O
ݴ6o7�����BBй��UCl�p4��~t^���*�$U�[�}�c�#]��O�.��?�	�܇�1��FK�j��"�B41�$���D�._�u�~�+Q�Y���c�u"�ҸQ�*�����*��f@k�� R���5ʬe��A������|+�M'����G���Z>�t	��V���~�jrQ��z�&�"�7w�є�S6^aG��V�k\o]�]�N�F�L � t����ږ�eA�U��I����EX�3D_%�>q�'�)��IF�M(����֍0� ��. ��� ��a?������#U�Bv
LA:''dm�8Z��]��%]��k�ո�z��&�pb \��zn��e���+\!��/d9%*��),���G���6�s���<^���(pcɆk8�%j3���=����bp}�꥔�x��#ږ0Lڪ=�)s��&A�|jF���X���ӉrK�3ن/x�������m����Z����	�Ƹm�o�-�Kn��`�%�ڔ�p�n T�!�7"f��lE�Ln�+��f08�r��o�h��Hd]�Uol�aF-��㖎#T��󃔧�+b)Ӛ���� sD�D2xA��*�{r����Z�	�ξ�3r �ts�R}o�2j�_�T睴��3��*��'��S<Ŝ��U*`>	�Ph��H�T_������sw��w3�f�xf^r�����N�X� ��A�7��K&�0l�gM�65�"��.�&��?���Y=�]�h�Q�$����ǰ431�4���9Y��T&ڰ.4��ğ����vR�H�Y�X���;�\~|����;�g,.Zj4�0�SF��M�]��N�8�xT�@I����|R!70e�����A�Bc�î\�9E���-�� 2����S5�Q$�Kx�&�	9x$���q�Xߝ
���B�|�,��S_y��sp��Z(��f��&~Ұ'N�_ �Z«D����$�����g�G8G>��Da�4K���^RR*���Mշn=ԅ�-���5c��H-����h�!>���K5{K��K�XV}��WHgy�Ă�^�7mqn o�K%5��k@Z0�(�'�!�< �%��� �"-7</��bh ����Йl �0K��:J�xmy�/��u�G(�v')V��AK*!l�z ��gԭ"�}��V���.�C�'�Y��>��`2fǿ�E.&%�'���8UPえ�!:��P��q��1��,��1]��2iU_�����膺ϝ��+����q���z�"7O�O�+.�*j�I��!�����Ԥ� �%�%D�����_"���w�Y��)'�smm�ɷl?\���+iZ�~�P���X5�ij��J�eAӞ�e���=�>Ԝ� ��r��>����-o�������*�@�� h��M���Y��K�=N5_��g��G��`��U������k��6��t�js�-}���E�Ջu4���h"?�6Z�n����kŇ:�p1��#)��`n��ը۽��VS)��5쭎8���T�X�LYJw��u�w��>y�*�o�1���!q0�cf������Ih遴���W)����FMe�S1~;��s��LG��1/��T��c�al�l	S������Z�Ey���JTN{�I�t/smN����!��]k��E���h���:6<f9�߈��q�����`�Y��3
s��4H/@��n�#J��tphl�ٌ�'b4{Ͱ�yY%��lܹ�S����[�N�u(C$��[YS����SF ~%��V�^�j��A0P|rV�D��zp�rbv�R��P��NUa��B/�{�݄�}��4JL�� �gH��D��n��	�57���J
�x3��b��\�[� <��֧��s�6+,��3^Ctn��е�!³�3 R)t1|Re!�q2������K�=N5_��g��G��`��U������k��6��t�js�-}���E�Ջu4���h"?�6Z�n����kŇ:�p1��#)��`n��ը۽��VS)��5쭎8���T�X�LYJw��u�w��>y�*�o�1���!q0�cf������Ih遴���W)����FMe�S1~;��s��LG��1/��T��c�al�l	S������Z�Ey���JTN{�I�t/smN����!��]k��E���h���:6<f9�߈��q�	u��Ӷ�8���_�47@����2"{�X��'���³6����/H�yY%��lܹ�S���F�Ǵ�aq�_�e���y���z�Iӫ�*z�V�^�j��A0P|rV����<[�@�ݚv. @5ߢ,����d�4	�"Ɉh��կ[��S����v7�'�φ��%��66.�C��s�>̇�7X�4�P)�C_�+!�3l�~��I�����R汲]]�υ��JYnh�~f"T�$hX���������P��K��u��Vq�C�M5`�9����CR��ϸ���wd#�	t�6T�ߝ!O��cf�t�����@B��T��Z�>Iv4	V�*G;��ۅ�):�2��ZsJU&�ފ��hu�0��i��w2t�Օ�6�B���"<HӮ_x�qћ��O�zNa������1�ϸV�#fD�q��(����]3ky!�J�>Rs@��sڟ�F�B��c*r�٧֣]��r�h�]Ў�8��	+�8NQ�B�f��Lɉ~�F��5msґ����w�����{�{aHG����u��Hvm`�g���z^�]H�/<z�^��>�yY%��lܹ�S������J})�^�"�^ʹ�`�G�t<p���s��u�����-ߛ��&�0ճ_�� Ĝ�N -JB����QCyǘk �U!Ԁ�|���Q���É�	lNC���Wo�p�@Srd�8����V�dbt蓷Τ?z��J��� WA`i��K��* }۱��s�\uG���y;`�V�����Y�^����y{*6Y�~N/�8r�촹��.�|͞��n�L�^r��/̌Hf��G��M�3���y8�B37z{���?5�j�3�	?�Z���Y������k�-ޢa<Q�1)��&_��غl���j�Q�T+��	{ޜ�h��O��O���Q��N�9�,=bӵ�f����}dL��bbdRg��ԝ��<��[ʲ�9��9!�PV���ܩ�L3���ܳc5�9���^
��kR@)y��f5B��ز@@�co�B#��vCv~e�"5��+��Б�4a�8��|�t��\��  ���
K�����.��F�).��[�v��_�-b����hn�P�����C� �y�qp�V�g�sd2|�� 8�F�,�Y��#[��5���ݹ
��}�6�3gBy���9�:Z+�qbyAE2��Ϫ��3�Ia�O��(�LY��O"F�(��R�{A��
��l؃�g+ TP����i98+#f��B�V��qV0�s�����k���)��	G#�_�0���]#���`U�ڜz���Y���d��5����-�e��K��/�o4�&�[��$���E�=��ò��=�&,~���RP
�hFAc�x%Oy2�C���G��(ѧ������]8���м��8N+����[��_��rlT^	�\w�Y�����n�"����_efu8\B+#T�r6,�����-���p_�l�a )�At��Oo��
����;2I.N�"7%:|��iU�<3砙����sS&�p�6�~�DT�4��2�銘i��ܴ���K�NkYGh�//fYb�1��	�p�z!~H��,�*�^S_'��J����nVъх�Gho��L���9�ٲ:Z#S���s��֖|z�F�mr,�Ʃf��o�nH�{{Ys�����>נ�'��b�oY�'_CI(IN��37�����4z~��# A�Σ�wߢ�XO��{��3�+�����������;�x�����#6i�~D2��Ϫ��3�Ia�O�A�<����.<�y��{#�����A�	 ~^A�c���hK,�0Z�H)-�	�:A#}��'��#��Z:�w~�i����Q���="4o2~^!{n����,q\@�n@�ѕ�\�N�Uڄ\[�ݥ�XG9V����ɠⓑ���d��b��O�:NS?_�hf\)K��"\�}�����8��yx��h���TYH$gVƢ7x��)�T�f����M𗐉��� �}3�N�&.c.�P5,q��&-*�Ѥй�<m-�91�Ո5K�����5H�[ٸ�au wƥ���Q Q���*�p���=��q\Yb$���}�V��Gm��T��t��z���U�OPF��$l�D��ԡ%;�ڗ���<V*�wʜ��5����m����k҉�o���4ޞ'�QT��1jX��e�˴�H-p��,���aW]{�=Z����*+�ݠɴ�V�
:���h�¢�e�������N_^3�\����ж�.������4��(,��9?g��m��k�9)�$^�_?�%َ�#nC𫬆�ts�GA
��\����q�<�ܔ�ֵg}W�tީC�YO|�{߈W��ν�!���9{�E)��h��FѧJ��m���C�9 T��'*�T����b�Q���I�;��>50�a��\$~`��k��s/�]��{a�W�����E1�����0nƪ��j�~0�"C0�jO�������ˆ�`Z��-�1}�o�y�K��(���f�a0]�ʻ�1ܥ�s�J������W��n�:�ߡ�����a6R�AhXp�b�M��N��W#2���,g��B��l���+�_�+[�df1eע,..Z{A��0��_JCbZ>�cvp��;BβxN��!�6��]�pZ@	�:τw�<rP�b�]s(x��k�Ȩr�ᚃ ������^R.?�ο)\�o櫂��U-!��L��6B
�5[\��WFU42a3�D�-�|�Z�=�]�-XD'/���6*?;��D

\�m��������X�d2�I����v�BV���J�OÊ�WK�(��O��ڮf�������i����
���Q���/)k�W�O�	�}�(*	nY�=�b�s�2���ŲȠCE�_���ب �}3�N�&.c.�P5,q��&-*�Ѥй�<m-�91�Ո5K�����5H�[ٸ�au wƥ���Q Q���*�p �XQ�V���1;֫��V��Gm��T��t��z���U�OPF��$l�D��ԡ%;�ڗ���<V*�wʜ��5����m����k҉�o���4ޞ'�QT��1jX��e�˴�H-p��,���aW]{�=Z����*+�ݠɴ�V�
:���h�څ�}f��MX����q�a�Ǧ#ե���a����c �q?�VZP_07V�&{�lI��ϯ�3��=!ğ�B��l�3�`�d�脹v���GP9�hf��Jm�-�
eA����[��iW�4�Qޏ&���S�����'!"��E
�'ZE*�Ɖ�kL!G�BL�#�61�u�F���ĎA�b�>P��މq��_j�����\@�pc�x(ۗt�-(�?f��s�� :z�T������uT��p��0�$P
���U��5�-��\#:��!)��E����PG-���xlM�~�G��C�]�骽]|-�.8�ME�ܸTTr���L/74��W.�O&���u±'� ��#�k��)� f�k7F�S,~���RP
�hFAc�x�n<	qYL�d�Mභyq��d7ml���0(1�����Te<��	U$h�>���~���iƽ"(��SFd֥��W�4����&zE�Jv[���Ց"6���(vX�+�l���aZ�{�iOp�f�Kky�8o V5�G��ov%�(�Lçו�M妪z��
8�q�áwm�uR�a�`ǅيM:aao���v��b�`�%Ѳ��>V����9՝���+��14���� Q��\[Ϩ�r!��y�n�;�G�~�zP*}�[.F�"��pg��~�r�=�k���`�)��ǂ_C���ix0�nzt��듃����"��/�}E��r�@�$�S���]��ރÀO��a��:�
kX�qV�s�՛�<��u�[C��*{��������C���g!�ns�0�;s=����C %�ᘲ��j)sh�����n��Ur	:�E؛� �Γ�F�@r"��E��B+��P#$d������e�U���9s:�x>���`�:��ϋ����Ģ��S9R^�,�1��T!��Y/-_�a���Rʋz��zhٴE�;*��3��l)>�e�_,6��k�B��Υ����l�ē�3�c�Ocsb�g�_U˹�4���tF��fV��CG���f"�nԎ��N6n��9
������%��^?�.H���kIq���(���-�����ľ�f���a���ӱN�KA^m�ДT��!)B5��h���C�j�A��'aT���5��5��A��gN�8+{�>v�Bcy�Pv-�j�����*.�������7�zX��$��T�	_�o2æ|)FR�k����)T�P �J����`�����G�R��#��dM~��u��_�X5�yd�%�勉y����r���[���7�Wnx禝�oUU��@� �ջ���X�鍣rxFX��U=}��x�a�i�=8����
�4��P�t���(�\�x�r�=.�X�"�TN�.ҍ%G�(Dc��D�$�<m�������?��!���&uҢ�>� ӽ���5g�uV`�8;���f�V&D�7�w"�_�� #�/��qG㈨��?\I�z"Y���:E��y���&�U(I-��Q���Q�}*Y7������GK�F8J
u���٫���vN��C�x7
�|MG�X�Hvp��/�T�>E�ƺ���Xj��I#k�a�7B�Rv¯�v�E%[�(��5���Sz#�����T u�/U�	ӎl�){
]���!�k0t,� ��j�2y-��Yx�M�K�G�Sv��fy�;`T�Y���b��}H�F�n��Ȅ�6��������V3���}%��$F�\x�R��g�b��$ ��:���'J���2QGdj*u��M�%���Nœ��=�A����5��Z
ġ�!�޿Wxp�� ��I�\����|�%�1�%+�y��ݳ��y��Kװp�>���8�'�qJf0��`��3�����*��2��J"jOi�xP^EQ��s6&��o�B�)���n��u�a�S��ЭX%Zc��Ӗf�	�A���sF����E4嬼2��O�`k^ǈa�zf��lG�C��f�jl��B9}���"BH��HՁ����E���u����	����
5>�$:9d�L�ٟkj<��+��Wsp<,T�݌K�CG1-M[Q&S��O�V�݆��:/���L:"�L��-3����2�hP��ثO^��k'�Uq]˝|�O�7?�w=���ۍ�KnZ�Mn;<|��b��3ǀe�����l)��?�@;��Ǒ�.�K�����!�E��l��W���71���*{��������C�i�h�A
i�;aU�b�״�su|�w���i#v�֧�p 9���2.m�u���3���.y9y+�~Dll�zE���FF�n{���r*�RP�-n�`��E���	���+`k&�z+���P���=o`�#٨%7�W����O6a@���ֻ-����/v��0xVNWO	���aL�o>75^C�$�}���ӯ���zu4I�˨a�D� �ؗS���>$�,�&�ȗ�C��U����Nq��{��O�����ig��~��P(f���Un�;hq!���	xr�d���m��:�t�}S�mQ8f�T�0�Q�N��,�aG]?P �J����`�����G�R��#��dM~��u��_�X5�yd�%�勉y����r���[���7�Wnx禝�oUU��@� �ջ���X�鍣rxFX��U=}��x�a�i�=8����
�4��P�t���(�\�x�r�=.�X�"�TN�.ҍ%G�(Dc��D�$�<m�������?��!���&uҢ�>� ӽ���5g�uV`�8;���f�V&D�77<��$����@�zxyμ�����(b
9{Æ���1��C+��%���F	��w�M�a,��P����(�@P�f��*{��������C���g!�ns�0�;s=�����!�q��<L�G�Mb��k���JO&���r �Zċ3�UO�dc|��4B�do
��bQ��@�E�~�i��H_�l��|�t��2@o�?i6"�3�ޝ�W
L�êg�d@�)_�� �#��?�)� *~�#�1��0���7z�:E��"zr�E�k�\*
�Iՙ�����MضD�� ˱E�$@N.�����F<��ϪU���xD�R��$m�HK���pt�4=L|����q����حm�s�k��ً �&��y���m^�AA,b�ꙸ���hP���y�'�y
6�ߡ3Ո��<*,�i�B���X�7Ɍ[О�	�0c)��U�r���pNK�ϣ��j�L�ka�K��vj_�W�􊼕�rmc�x��x�?Z�TZH�.Z�h	PD&�5�5���m�C�	�ݶa����<�f�SKM�B�g��f�՛�H����}fPT�t�;|71E��~�x+~�y3,�˘��S��B*�7�Ճ����▖d#FullMData
� Sld_FullData �� first_member_ptr �� alumni_members �� ref_part_table �
����
name START_ASM �type �revnum  �memb_id_tab �����dep_id ��attributes � simpl_rep ��feat_id �� next ����STARTASM  ��������STARTASM  ��������STARTASM  ��������STARTASM  ��������STARTASM  ��������STARTASM  ��������WEDGELOCK_42 ��	  ����WEDGELOCK_42_MID   ����WEDGELOCK_42_PIN   ���� first_xsec_ptr ���id q�type �faceted_rep_id ��revnum ���plane_id 	�feat_id ��quilt_id ��
name A � idtab_ptr �� srf_id_tab_ptr �����fc_hist �r��q ��〆��s 〇��t � edg_id_tab_ptr ������ed_hist �s�߁�r �����t�߁�r ��� save_geoms ���cr_flags_xar �u��������  ��� ���   ���C ����  !�� ���@  ���������������@ ����   ���  ���`  ��@������������ ��@ A������� ��@  ���� ���� ���������������@ 	�� ���� ��������� ��	����� 	������A��� ���� �C��
>��2�ҹ���O��m0Ud��{��� X�� ���PB��� cvt_feat_dat �� feat_dates_ptr ��creat_date �к�regen_date �к�assem_path ��f� xsec_xhatch_info ��xsec_id q�type � lines ��&���angle -�offset �spacing �K�ҧ1� style ��font �color �xsec_type � � next_xsec_ptr ���p��	��������������  ��� ���   ���C ����  !�� ���@  ���������������@ ����   ���  ���`  ��@������������ ��@ A������� ��@  ���� ���� ���������������@ 	�� ���� ��������� ��	����� 	������A��� ���� �C��
>��2�ҹ���O��m0Ud��{��� X�� ���PB�����к�кe��- �o��	�������������  ��� ���   ���C ����  !�� ���@  ���������������@ ����   ���  ���`  ��@������������ ��@ A������� ��@  ���� ���� ���������������@ 	�� ���� ��������� ��	����� 	������A��� ���� �C��
>��2�ҹ���O��m0Ud��{��� X�� ���PB�����к�кd� �n��	����������u��n ��〈��v 〉��z 《��y 》��x 「�က 」�ခ 『�ဂ 』�ဃ 【�င 】�စ 〒��{ 〓��| 〔��} 〕��~ 〖�� 〗��w 〚�မ 〛�ဘ �������v�߁�u �����w�߁�u ���x�߁�u���y�߁�u ���z�߁�u ���{�߃�u u�|�߃�u u�}�߃�u u�~�߃�u u��߃�u u　�߄ u u、�߄u u。�߄u u〃�߄u u〄�߄u u々�߄
u u〘�߅�u ��〙�߅�u �������������  ��� ���   ���C ����  !�� ���@  ���������������@ ����   ���  ���`  ��@������������ ��@ A������� ��@  ���� ���� ���������������@ 	�� ���� ��������� ��	����� 	������A��� ���� �C��
>��2�ҹ���O��m0Ud��{��� X�� ���PB�����к�кm��$n��&���'��l:��(R�� �� lang_ptr ��interact_on �� first_csys_ptr ��� dim_array ��2���type �dim_type ���feat_id �attributes �� dim_dat_ptr ��value �bck_value � tol_ptr ��type 	�tol_type �dual_digits_diff ���value ���digits �nominal_value ��bound �bck_bound �� bck_tol_ptr ��value ���datum_def_id ���places_denom �bck_places_denom �dim_data_attrib  � dim_cosm_ptr ��� p_sld_info �� attach_ptr ��parent_dim_id �� symbol ��type �text(i_val)  ��2������	������� ��������
���	������� �������2���������3���� ��	�������� �������2�������3��e��G�G��	��(�bM����G���(�bM�������������d��G�G��	��(�bM����G���(�bM�����������ipar_num � � smt_info_ptr �� cable_info_ptr ��id ��type ^�ref_harness_id ��cable_assem_id ��cable_assem_simp_id �� logi_info ��work_subharn_id ����parent_ext_ref_id �� p_frbn_info ��
log_info_src_name ��log_info_src_type  � cable_report_info ��
cable_jacket_report_name DEFAULT �
cable_shield_report_name SHIELD �
cable_node_report_name - � comp_info_ptr ��� prt_misc_ptr �� shrink_data_ptr �� expld_mv_info ��� state_info_ptr �� alumni_feats ��id c�indep_attributes � � feat_type_ptr ��table_id ��type ���
feattype_key ��form  �attributes ��   ��placement  �intersection ��internal_udf_type �� param_choice_ptr ��blend_choice  �depth_choice  �angle_choice  �pat_choice ��round_choice ��subsec_choice ��sweep_choice  �dome_choice  �draft_choice  �misc_choice � ��assoc_type � �symbol_key ���
sect_name ��model_type � idtab_ptr �����������  ��� ���   ���C ����  !�� ���@  ���������������@ ����   ���  ���`  ��@������������ ��@ A������� ��@  ���� ���� ���������������@ 	�� ���� ��!�������� ��	�� ��� 	�����>? �   �   �`�   �  �   ����Ю�к� group_info_ptr �� intrs_pairs_ptr �� place_rec_ptr ���local_sys �����own_ref_id �orient  � place_instruction_ptrs �������type ��offset �dim_id ��ref_id  �geometry1_id ��geometry2_id ��memb_num1  �memb_num2  �attr � �� plin_info_arr �������type � data(plin_info_ui_step) ��step_ids ��d�attr  �uv ���� trf_arr ��������type �idx  �transf ����� next_prop ���first_geom_id ��cond_tbl_idx ��
comment �� merge_info_ptr �� dat_ptr(comp_data) ��type {� model_ptr ��object_type �
name WEDGELOCK_42_MID � simpl_rep �� inherit_rep ��placed_flag ��subst_asm_ref_id ��instance �� stamp �� major_vers ��vers  �
alias_name  � minor_vers ���vers  �dep_id ��� interchange_info ��ref_contr_flags  � topology_increment ���state_ind �� alumni_info ��replaced_by m�repl_lead_id ��repl_type �
group_name  � moved_comp ��revnum �� rev_rec �� revnums �������owner_type �owner_ind ��ref_id ��owner_id ��ref_revnum ��ref_status ��num_plins ��largest_prev_rev ��� expl_state_ref ��� feat_outl_info �� parchild_table ��	Structure_Invalid ���� copy_info_ptr ��� next_feat_ptr ��� p_mdl_grid �� mdl_status_ptr �� appl_info_arr �������id ��type 7� post_reg_outline ��outline ���`FD{��ߛ��`��`-D�9Z{�Ā.�:I������������� � ribbon_info_ptr ��� p_xref_data �� first_analysis_feat ��� da_info �� setups ��first_free �� id_map_info_ptr ���faceted_rep  � sw_info ��offset �-��l��-D�F�$Sd�-����-��l��-J`�o��-��l�-J`�o��-���-J�Cϊk�-���-��l��#NeuPrtSld
� Sld_NeutPart �� neut_part ��#NeuAsmSld
� Sld_NeutAssem �� neut_assem �� assem_top ��revnum  �accuracy �outline ���accuracy_is_relative  � mass_props �� time_stamp �� colors �� attributes �����
name VOLUME � value ��type 2�value(d_val) -���8p�ref_id ���ref_type ���SURF_AREA �2-�#��]����MP_X �2��MP_Y �2�1P�!l��MP_Z �2n{L C��WEIGHT �2-���8r��PTC_COMMON_NAME �3�
value(s_val) wedgelock_42.asm ����MC_INTF �311/22/2005 326 ��DIM_L �2�-S�������PART_NAME �3�Wedge Lock ��PART_NUM �3 ��L_NUM �2�/ ��DIM_M �2-6�ffffd��Num_Instances �4�value(i_val) ����InstanceName0 �3�WEDGELOCK_42__8 ��InstanceName1 �3WEDGELOCK_42__6 �� layers ��
����id l�
name 1211 �user_id  �operation ���<00_LAYER_AXIS  �ids �$��B01_LAYER_CSYS  ��H02_LAYER_CURVE  ��?03_LAYER_DATUM  �K05_LAYER_POINT  �507_LAYER_ASSEM_MEMBER  �L13_DEFAULT_DATUM  ��	�V1_ALL_PLANES  ��U7_ALL_FEATURES  � item_names ������
name ASSY_SIDE_YZ �obj_id �obj_type ���ASSY_TOP_ZX 	�ASSY_FRONT_XY �ASSY_AXIS_X �ASSY_AXIS_Y �ASSY_AXIS_Z '�ASSY_CSYS �DEFAULT �@  �5�DEFAULT �@  ���DEFAULT �@  ���DEFAULT �@   ��� members ������id m�type �
name WEDGELOCK_42_MID �
family_name WEDGELOCK_42_MID �e1 ���e2 ��e3 ���origin �����dWEDGELOCK_42_BOTT WEDGELOCK_42_BOTT ��FA�z�G�}�\(��eWEDGELOCK_42_TOP WEDGELOCK_42_TOP ��-A�=p��	}�\(��fWEDGELOCK_42_PIN WEDGELOCK_42_PIN ��-C��
=o}�\(��#ModelView#0
� View ��id  � viewattr ��attr_arr � �� mpoint ���id  �coord �9  LG�z� m�\'�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� -?�����/z^F�7�W.e'��� spoint �� �4� ���orient �xyT�ۢ�c�:�+gk���RFW n�:���rv�P�A��?6���]�AmR@�8Q���local_sys �-&A�c1,����N�F|	4%\�-
���Ny-!o2*�b��e����&F"��- !,-��n��-_��x#�-z��3F �F�8 Ss��� world_matrix_ptr ��world_matrix �-&A�c1,����N�F|	4%\�-
���Ny-!o2*�b��e����&F"��- !,-��n�����x#���gр$F���с��model2world �-&A�c1,����N�F|	4%\�-
���Ny-!o2*�b��e����&F"��- !,-��n�����x#���gр$F���с�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� -(*�����Y�� ��matrix ���-?�����/z^F�7�W.e'9  LG�z� m�\'�����outer_xsec_index ��next_view_id �simp_rep_id  �
name no name �fit_outline �/Y /UF�vX�O��/� /��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix ����FsXny"��position ��-��qЛ4��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#30
� View ��id � viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr ��world_matrix �y9�|��k�y9�|��k��9�|��k��model2world �y9�|��k�y9�|��k��9�|��k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���	� ���� ���outer_xsec_index ��next_view_id (�simp_rep_id  �
name TOP �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#40
� View ��id (� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys ��9�|��k�y9�|��k��9�|��k/@/z^F�J��7�� world_matrix_ptr ��world_matrix ��9�|��k�y9�|��k��9�|��k��model2world ��9�|��k�y9�|��k��9�|��k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��s��2đh{�MR�x������A��0g��,r�� PGpxS��щomË8�)A�i���qm� Z��������� �����	� ���outer_xsec_index ��next_view_id )�simp_rep_id  �
name 5_BACK �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#41
� View ��id )� viewattr ��attr_arr � �� mpoint ���id  �coord �-JL�����4� � first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �-�C"��F�C"��-�C"��-UmswS2�/z^F�B>t�8� world_matrix_ptr ��world_matrix �-�C"��F�C"��-�C"��Fy�"+3M��Oٍ<�model2world �-�C"��F�C"��-�C"��Fy�"+3M��Oٍ<� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� -�C"���}�� ��matrix ���#9��i�����A��h�+ i����-w��<)�(�v����p(�t�AT�(��q�GM`��#�>�����	� ������ ���outer_xsec_index ��next_view_id +�simp_rep_id  �
name 6_BOTTOM �fit_outline �/Y /UFJ�T4��@/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�B>t�8�view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#43
� View ��id +� viewattr ��attr_arr � �� mpoint ���id  �coord �� -De�Q�� � first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ��F"�%��C�-"�%��C�-"�%��C�/@-A����F��	�U6� world_matrix_ptr ��world_matrix ��F"�%��C�-"�%��C�-"�%��C�7��=-Fx���>��%��C�model2world ��F"�%��C�-"�%��C�-"�%��C�7��=-Fx���>��%��C� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� -"�%��C��}�� ��matrix ��A�+��+yUVB���p��Ol$*Xݕksuzyآ��P��L�ym7TjcA�Ԋ�8w�AϮ-ԗ7P������ �����	� ���outer_xsec_index ��next_view_id ,�simp_rep_id  �
name 4_LEFT �fit_outline �/Y /UFP�T.@/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��	�U6�view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#44
� View ��id ,� viewattr ��attr_arr � �� mpoint ���id  �coord ���
=p��m�\(�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ��-"��W�f�-"��W�f�F"��W�f�-���
�-z�gi��F�-��b@� world_matrix_ptr ��world_matrix ��-"��W�f�-"��W�f�F"��W�f����¢-AY�~���model2world ��-"��W�f�-"��W�f�F"��W�f����¢-AY�~��� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� -"��W�f��}�� ��matrix ��x�zGϩ2F9f�[��*�<YZ�ɤ�<xh��a1���z��D���o�;�\\J\w˼�TKP������ �����	� ���outer_xsec_index ��next_view_id -�simp_rep_id  �
name 2_RIGHT_YZ �fit_outline �/Y /UFO^3ߜ|�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�-��b@�view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#45
� View ��id -� viewattr ��attr_arr � �� mpoint ���id  �coord �LG�z� m�\(�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �-(*����-(*����-(*���/@-zP�K��F��Mt�]� world_matrix_ptr ��world_matrix �-(*����-(*����-(*�����h���F�|�Ȃ�model2world �-(*����-(*����-(*�����h���F�|�Ȃ� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� -(*�����}�� ��matrix ��z���A���}/!(��Q�� Y���(�x�}LC;UA�Ag�V�(�d�<|�\�Qy���X������� �����	� ���outer_xsec_index ��next_view_id .�simp_rep_id  �
name 1_FRONT_XY �fit_outline �/Y /UFWA'�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��qЛ4��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#46
� View ��id .� viewattr ��attr_arr � �� mpoint ���id  �coord �LG�z� m�\(�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �-(*����-(*����F(*����/@-zЉ�g"F����E7� world_matrix_ptr ��world_matrix �-(*����-(*����F(*����-�|�Ȃ��h����model2world �-(*����-(*����F(*����-�|�Ȃ��h���� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� -(*������	� ���� ���outer_xsec_index ��next_view_id 3�simp_rep_id  �
name 3_TOP_ZX �fit_outline �/Y /UF�EaL[��/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��qЛ4��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#51
� View ��id 3� viewattr ��attr_arr � �� mpoint ���id  �coord �-J��K�-A�{�{FD�f�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����Y�%�c0o�׏�)A�Q.�v��jL"�w����<7���n��9��m�5/6���u��U7���local_sys �ӟ��!W�̨��7Bi�1��ZW�AĹ���\� ��*Kհ���x��?��x|kA��:��Ŵ���]�-�Ț^��-w�2y� LF�Ҽx)��� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���}�� ��matrix ���=���l�Z�C	�(����S*M��7[��cuW	���A���o��(�8����tAϭ���� ��+�����-�� ��B���������outer_xsec_index ��next_view_id �@  �simp_rep_id  �
name 0_ISO_BACK �fit_outline �/Y /UF�i�5P�/� /��� det_info_ptr �� named_view_pzmatr ������page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��B�F��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#1073741828
� View ��id �@  � viewattr ��attr_arr � �� mpoint ���id  �coord �LG�z� m�\(�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �xyT�ۢ�c�:�+gk���RFW n�:���rv�P�A��?6���]�AmR@�8Q���local_sys �-&A�c1,����N�F|	4%\�-
���Ny-!o2*�b��e����&F"��- !,-��n��-_��x#�-z��3F �F��1YN_� world_matrix_ptr ��world_matrix �-&A�c1,����N�F|	4%\�-
���Ny-!o2*�b��e����&F"��- !,-��n�����x#�"�gр#nF���у��model2world �-&A�c1,����N�F|	4%\�-
���Ny-!o2*�b��e����&F"��- !,-��n�����x#�"�gр#nF���у�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���outer_xsec_index ��next_view_id ��simp_rep_id  �
name DEFAULT �fit_outline �/Y /UF�vX�O�T/� /��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��qЛ4��view_angle � explode_state ���model_cosm_rev_num  �zone_id ��#DispDataColor
� ref_color_table ��#DispDataTable
��Z�)�rŘW��?Etpۇ+UAGvRT-w���c���X��̕�cv��2�v E��O��ȴ�����(��H�.-ڒ����Ѷ�RGZ�x~`�H��xI?�L1~X1�����C?��JC�<��d�j�����Φ�y~��3� ��$:�����uv1�:�����ƾg>��%0�pߎ�wg{+��;��Ce���	�xN�>�K�a[	gl�5v^���F�Ε@��)`�t��@��Vxv]����K��%j_�n2"���S{x?7�IY�A�P@�U2$�.=t|r��,�̷����Q6�il�o3�Z~�׭��Hf~�p�M}����M���n�d}� �a����s�y����s��]��a�/�\?,&�05�f�������uGBz����g��i���c؇�N뢓Ҁ�P#����M�!���5�4t�f{Aq�b�Ѫ��y?�s�I�w3���A�8��φ�~Á���6��5���,V��1W��B�oアI*C�s�N�^cH|f�]�AQ�g�H���M�3�����Mx�6wQw.
پ���k�B��!t�b��(��h��?��,��^����c�T�^Cζ�ԟ���� �A�Ӱ��Df��[��~�T�=�NY
X<��d��_�(4֞�)�M�Mos���[��ب����,�H���ɇ{l�
�W�}�����N6h�����[�b
�t�g8���<�x�Q��bE����e�dyi�b��W��%�D�M�� �:J����LR�i��W�l�{%��@JN�MO#AssMrgPrt
� assem_merge_parts ��#IndentTag
� Fi�9��������D�QZ�2~�Պ,���R�^��a���2�e*��V�Z[
��O:iH�α��*����5�kd�>� ���͉di(����_(_��s:�r1�)RׇIA��9�)�#�<_S�� �5����O���q�V�5��I�y�����nw��ŝ۵�Ⴠ���E�NT���	=������S߿1u�\T������	�P��H�r~T�f�A;�C9 ���f�8��S�Cy�� ;l"�J���Q���6�p���0��	~�|��f���*���W�,
�A�o��R������:	��(n���I�R�׊����8�� G����
ih�R�&���{IL+�ٛz�7�Q�s4��o|�γ�dCC����p �s9���dw��"=�"[�a��x�*�T�7� ��Ө�`Hޱ�B�=U�z쒭��
�?V����=�����o&�d𩆽�x�վ��+��*����5?���OT����*�4�GŗQ�U�����g�[�4���jg�fK�E?�II�~C��;q9,S�i��v��'����;VH\�/�% sQ&������W�(�!�ºM�1�#����O�~���	�Rԩ�'c�V�Z6���R�pf*�H��3�����M��}#\�SB��9o7N,/���ZiC�p;�sH���� 8_��Q8�ia5}ri�ac�� ٕ]�#�a׸�������b5��܃b�oЖ;�;�sdO��Qg�L];d$��C�c�g�����4C�*8��/Z��va����LC��xؼjj���إF̟S�8"g̫_��/��f��1��D��,՟ELj�a�u#FeatRefData
� Fe�A��}�*�,�:�Z�T�HQ#�VX5�UL)�"U�b��y�eje�8kX�MO�D~�����I�ë %YK�5�m���#���~T�Df��h�v�>@�I�׵G/g�5RmO2����"��3"rP6D��K��G��Nai�r��`^٠Wrh�8=%�2�K4fZb2�yMG��L�����f���Ak
���[M���S�g����H��>�c}�L��c}�g��e#END_OF_UGC
