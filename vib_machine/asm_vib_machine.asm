#UGC:2 ASSEMBLY 1315 620 8 1 1 15 2500 2004320 00000bda \
#- VERS 0 0                                                          \
#- HOST                                                              \
#- LINK                                                              \
#- DBID                                                              \
#- REVS 0,                                                           \
#- RELL 0,                                                           \
#- UOBJ_ID 1111046356 769734789 1987995311                           \
#- MACH _Windows                                                     \
#-END_OF_UGC_HEADER
#P_OBJECT 6
@P_object 1 0
0 1 ->
@depend 2 0
1 2 [4]
2 2 
@model_name 3 10
3 3 BODY
@model_type 4 1
3 4 2
@revnum 5 1
3 5 864
@bom_count 6 1
3 6 1
@dep_type 7 1
3 7 2
@stamp 8 0
3 8 
@major_vers 9 0
4 9 
@vers 10 1
5 10 0
@alias_name 11 10
5 11 
@minor_vers 12 0
4 12 
@vers 13 1
5 13 0
@reason 14 10
3 14 
@comp_ids 15 1
3 15 [1]
4 15 77
@comp_dep_types 16 1
3 16 [1]
4 16 2
2 2 
3 3 FOOT
3 4 2
3 5 790
3 6 4
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [4]
$78,86,87,88
3 16 [4]
$4*2
2 2 
3 3 PLATE
3 4 2
3 5 1627
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 91
3 16 [1]
4 16 2
2 2 
3 3 MAIN_JIG
3 4 2
3 5 765
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 92
3 16 [1]
4 16 2
@dep_db 17 0
1 17 ->
@dep_db 18 0
2 18 [8]
3 18 ->
@to_model_id 19 1
4 19 0
@dep_type 20 1
4 20 2
@obj_id 21 1
4 21 77
@obj_type 22 1
4 22 3
3 18 ->
4 19 1
4 20 2
4 21 78
4 22 3
3 18 ->
4 19 1
4 20 2
4 21 86
4 22 3
3 18 ->
4 19 1
4 20 2
4 21 87
4 22 3
3 18 ->
4 19 1
4 20 2
4 21 88
4 22 3
3 18 ->
4 19 2
4 20 2
4 21 91
4 22 3
3 18 ->
4 19 -3
4 20 4
4 21 7
4 22 242
3 18 ->
4 19 3
4 20 2
4 21 92
4 22 3
@model_db 23 0
2 23 [4]
3 23 ->
@model_t_type 24 1
4 24 2
@model_name 25 10
4 25 BODY
@generic_name 26 10
4 26 
@stamp 27 0
4 27 
3 23 ->
4 24 2
4 25 FOOT
4 26 
4 27 
3 23 ->
4 24 2
4 25 PLATE
4 26 
4 27 
3 23 ->
4 24 2
4 25 MAIN_JIG
4 26 
4 27 
@usr_rev 28 0
1 28 [1]
2 28 
@name 29 10
3 29 SMLEE
@mod_time 30 0
3 30 
@tm_sec 31 1
4 31 12
@tm_min 32 1
4 32 27
@tm_hour 33 1
4 33 12
@tm_mday 34 1
4 34 18
@tm_mon 35 1
4 35 2
@tm_year 36 1
4 36 105
@action 37 1
3 37 1000
@comment 38 10
3 38 Hostname: 'SORBON'
@rel_level 39 10
3 39 
@rev_string 40 10
3 40 
@revnum_std 41 1
1 41 302
@flag 42 1
1 42 0
@name 43 10
1 43 NULL
@inst_hidden 44 1
1 44 0
@old_name 45 10
1 45 NULL
@user_symbols 46 0
1 46 [1]
2 46 ->
@class 47 1
3 47 1
@access 48 1
3 48 0
@changed 49 1
3 49 0
@copy_of_generic 50 1
3 50 0
@name 51 10
3 51 PTC_COMMON_NAME
@value 52 0
3 52 
@type 53 1
4 53 51
@value(s_val) 54 10
4 54 asm_vib_machine.asm
@id 55 1
3 55 -1
@type 56 1
3 56 -1
@appl_data 57 0
1 57 ->
@bounding_box 58 2
2 58 [2][3]
$C0773056E2EBC69,C050815B8BAF1A42,C0773056E2EBC69
$40773056E2EBC69,4081B02B7175E348,40773056E2EBC69
@ver_key 59 0
1 59 ->
@time 60 1
2 60 1111148822
@random_num 61 1
2 61 769856656
@hostid 62 1
2 62 1987995311
@last_read 63 1
1 63 -1
#END_OF_P_OBJECT
#Pro/ENGINEER  TM  Wildfire 2.0  (c) 1988-2002 by Wisdom Systems  All Rights Reserved. M050
#UGC_TOC 1 32 81 17#############################################################
SolidPersistTable acd 305 1 1315 10 2 443b f011#################################
SolidPrimdata dd2 fb 1 1315 10 2 6816 ebe7######################################
BasicData ecd 31e 1 1315 9 -1 43a8 31dc#########################################
BasicText 11eb 334 1 1315 9 -1 49f4 2a7c########################################
GeomDepen 151f 16c 1 1315 9 -1 929d 7120########################################
DispCntrl 168b 2c9 1 1315 9 -1 3e25 c169########################################
DisplData 1954 29 1 1315 9 -1 0cac b9ca#########################################
LargeText 197d 14ba 1 1315 9 -1 328a b62b#######################################
FamilyInf 2e37 2d 1 1315 9 -1 0e87 f0a9#########################################
VisibGeom 2e64 2fc 1 1315 11 -1 70d1 33a9#######################################
NovisGeom 3160 194 1 1315 11 -1 bb40 c517#######################################
ActEntity 32f4 6a6 1 1315 9 -1 d5c8 7a4d########################################
AllFeatur 399a 3364 1 1315 11 -1 9fd0 c4d2######################################
FullMData 6cfe 8ad 1 1315 9 -1 b6e7 77a8########################################
NeuPrtSld 75ab 29 1 1315 9 -1 0cfc be04#########################################
NeuAsmSld 75d4 445 1 1315 9 -1 7942 27e9########################################
ModelView#0 7a19 4e8 1 1315 9 -1 206c 4cf4######################################
ModelView#30 7f01 350 1 1315 9 -1 57f9 9b46#####################################
ModelView#40 8251 3c3 1 1315 9 -1 9957 ed58#####################################
ModelView#41 8614 36f 1 1315 9 -1 6f62 2bde#####################################
ModelView#43 8983 36e 1 1315 9 -1 6cf0 10d2#####################################
ModelView#44 8cf1 36f 1 1315 9 -1 6f42 d074#####################################
ModelView#45 9060 3e3 1 1315 9 -1 9e81 a41d#####################################
ModelView#46 9443 366 1 1315 9 -1 5f7e 2ee5#####################################
ModelView#51 97a9 3c4 1 1315 9 -1 9d36 b6e6#####################################
ModelView#1073741828 9b6d 3fe 1 1315 9 -1 a7e4 e9c9#############################
DispDataColor 9f6b 16d 1 1315 9 -1 98a8 8353####################################
DispDataTable a0d8 3ac 1 1315 12 2 c2ab ba21####################################
AssMrgPrt a484 21 1 1315 9 -1 09c5 6a48#########################################
IndentTag a4a5 36f 1 1315 11 -1 b32f 3d22#######################################
FeatRefData a814 fe 1 1315 11 -1 7848 7707######################################
################################################################################
#SolidPersistTable
�� �9���2r椙C��0bؔ0\��|IC  �a��!��2y� �4o~y#F�p��)cF&M:yx��M�6=��4��c��3��L��)R�S�y�N�y��gC%U6iܔY�'�԰�X�8��7r��|C.�4mQ�W,��t��SG�[�*���3M2��(�*H�a�q3f0X�)��al��7p2�I������a�.s�M����r��9�{s�Z x�ܛ���@��y��3'Ԇ@%����:\��1}�1��.���v��{��È*       ,�CP�1�v�P��Nn\&�rt�!Gb��}��qVZ �Gz|�Z"�� �0  � ���GZg|ڌu0�]��Qst��}��Vr�@<7���� ���>�4�]v�ӒM�E��Ie ��@݋b� ��@QG��W�)��<҉3��g �X@G�@��X��="$���`��fG��G� �c�i� \u�iZ�rxQ�	k�G�����OA0�N���^�y��{��gzG�~$��F �RL���v�qG�dfil'��WG���� @gARB��k�����G��1u\��Np�i�m�т�9���T��H*���"d@n(X��F���,��V��mL�1w�==�j��)ĐR��
^@њ�<)u�o� �E�jđ�����='�i@ (0hz��i�����8��3X>���г #SolidPrimdata
�� �9��9iڐ	C'̗3r�ԁ�6e �(��A�
:�(qΗ0r� ����},x0�#�0� 6o�|��M�/`  8���2fL��G��{^n��ԩ���p-XFΜ4s��y�\�7b�P�#p	��-��T�fȖq3ほ/~IC��3V�&Ȇc޸a��Y �x��X��W�p������	X v�Cg&�4t�������č�s @�q�X����w#BasicData
� Sld_BasicData ��tab_key  � tab_key_struct ��type H� tab_key_ptr �b� cmprs_tabkey ���type �� revnum_ptr ��geom_revnum ��cosm_revnum � tol_default_ptr ���type 
�value �(�bM���(6��C-#�����h�#�������ang_value ���lin_digits ����ang_digits � ��standard  � iso_tol_tbls_ptr ��datum_count � �axis_count �insert_layer_id ��layer_count �rel_part_eps (S�]� ��model_eps 
�fixed_model_eps �assembled_count  �part_type �first_ent_id �9��first_view_id  �outline �Hv�HN Hv�/v�/��/v��dim_num � geom_lib_ptr ��attrib_array ��    �� A  ��max_part_size �
model_name ASM_VIB_MACHINE � disp_outl_info ��disp_outline �Fw0V��ƐFP�[��BFw0V��Ɛ-w0V��Ɛ-��+qu�H-w0V��Ɛ� sel_outl_info ���outline �Fw0V��ƐFP�[��BFw0V��Ɛ-w0V��Ɛ-��+qu�H-w0V��Ɛ�#BasicText
� Sld_BasicText �� assem_mass_prop_ptr ��type  �density �init_dens �rel_density �init_rel_dens  �aver_density  �vrevnum ��volume �A���lȸ�cg ���SN�-o<,xa�7(�+qRg��inertia ��B���F��A^ ���-������A^ ����B��_{�AU��DK_�-������AU��DKA��B���m��i�eps #�����h��surfarea �AG�)�~�csys_id �� mass_prop_ptr ���   ����� assign_mp_ptr ��
mat_name_ptr �� unit_arr �����type �factor (�(P�B�
�
name MM �unit_type  ���-�9D�݁TONNE �(24Vx���SECS � p_unit_info ��� custom_unit_arr ����������� xp_custom_sys_unit ������_id ��_obj_type �
name Custom �type �
unit_names �mm kg  sec C rad �_unit_ids ��!�
principal_sys_units �millimeter Newton Second (mmNs) �_principal_sys_units_id 3�history_scale ��_version �_revnum � dsgnate_data ���#GeomDepen
� Sld_GeomDepend ��frst_geom_ptr  � fem_data ��first_quilt_ptr  � topol_state ��last_regen_feat_id \� incr_time_stamp ��time �B:���random_num �4�6��hostid �v~f�� asm_comp_map ���comp_id ��attrib  �
uniq_name �� asm_map_sys �� time_stamp ��vis_revnum ��invis_revnum �� geom_own_tbl ���num_mech_connection  �num_feat_regens q�first_lo_ptr � �#DispCntrl
� Sld_DispCntrl �� color_prop_ptr �� layer_array �����id 5�type u�user_id  � weird_wline_ptr ��
weird_wline 07_LAYER_ASSEM_MEMBER � layer_contents ��� layer_compiled �� layer_rbd �� cond_block ��saved_operation ��deftype_num ��attributes � ���<u �00_LAYER_AXIS �� item_data ����� item_data �����type � id_data �����id ����$�revnum ���
���������������������$������ �?u �03_LAYER_DATUM ��
����� �Bu �01_LAYER_CSYS ������������������
��������������������� �Hu �02_LAYER_CURVE ��
����� �Ku �05_LAYER_POINT ����� �Lu �13_DEFAULT_DATUM ����������������������	����
����������������������	������� �#DisplData
� Sld_DispData �� disp_data ��#LargeText
� Sld_LargeText �� names_table �����id �type �
name ASSY_SIDE_YZ �	attr  ���	ASSY_TOP_ZX  �ASSY_FRONT_XY  �ASSY_AXIS_X  �ASSY_AXIS_Y  �'ASSY_AXIS_Z  �ASSY_CSYS  ��@  �5DEFAULT  ��@  ��DEFAULT  ��@  ��DEFAULT  ��@   ��DEFAULT  � relsymdata ��� relsym_set_arr �����id ��type �	ui_order �	ui_order_fixed  � symtbl �����
name  �sym_class '�attr � � value ��type 3�
value(s_val)  � bak_val ���3 �access_fn_idx ��dim_id ��appl_int  �
description �� sym_rstr �� sym_bak_rstr ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �2�value(d_val) /Y ���2/Y ��� ����VOLUME '� �2/Y ��2/Y ��� ����SURF_AREA '� �2/n ��2/n ��� ����MP_X '� �2/ ��2/ ��� ����MP_Y '� �2��2��� ����MP_Z '� �2H ��2H ��� ����WEIGHT '� �2/Y ��2/Y ��� ����PTC_COMMON_NAME '� �3�asm_vib_machine.asm ��3asm_vib_machine.asm ��� ���� rel_data ���attr  � relset_code_tree �� relcode_tree_child ������node_type � node_data(simple_data) ��type � node_val(loc_handler) ��own_id ��own_type �relset_idx  �sym_id �
src �?0 � relcode_tree_next ������� �� node_val(const_val) ��3 �(("" �� �� node_val(func_call) ��code -�ext_func_id ��postfix_id ���(MP_MASS �� �node_val(op_code) ���(= �� node_data(line_data) ��code ���line_no �
src ) ��)������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�.��MP_VOLUME �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�/��MP_SURF_AREA �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�*��MP_CG_X �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�+��MP_CG_Y �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�,��MP_CG_Z �� �/��= ��0��) � �  ��� relset_bak_code_tree �������� �!��#�� ?0 ������ ��*�3 ("" �� ��+�-��MP_MASS �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�.��MP_VOLUME �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�/��MP_SURF_AREA �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�*��MP_CG_X �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�+��MP_CG_Y �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�,��MP_CG_Z �� �/��= ��0��) � �  ������ �����PRO_MP_SOURCE <��~"x �3GEOMETRY ��3GEOMETRY �� ������MP_DENSITY -��~"x �:� value(NO_val) ����:��� ���	attr �id ��set_id ��unit_id G�origin �����6��G���PRO_MP_ALT_MASS J��~"x �:���:��%� ������������PRO_MP_ALT_VOLUME J��~"x �:���:��%� ����@�����@���PRO_MP_ALT_AREA J��~"x �:���:��%� ����;�����;���PRO_MP_ALT_COGX K��~"x �:���:��&� ������������PRO_MP_ALT_COGY K��~"x �:���:��&� ������������PRO_MP_ALT_COGZ K��~"x �:���:��&� ������������PRO_MP_ALT_IXX K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IYY K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IZZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IXZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IYZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IXY K��~"x �:���:��&� ����N�����N���PRO_MP_MASS =��~�( �2��A���lȺ��2/Y �� ������������PRO_MP_VOLUME >��~�( �2�A���lȸ��2/Y �� ����@�����@���PRO_MP_DENSITY ?��~�( �2��2��� ����G�����G���PRO_MP_AREA @��~�( �2�AG�)�~��2/n �� ����;�����;���PRO_MP_COGX A��~�( �2��2/ �� ������������PRO_MP_COGY B��~�( �2-o<,xa���2�� ������������PRO_MP_COGZ C��~�( �2��2H �� ������������PRO_MP_IXX D��~�( �2�B�]V+�@]��2-�
������� ����N�����N���PRO_MP_IYY E��~�( �2�B���Z2���2.@�� � ����N�����N���PRO_MP_IZZ F��~�( �2�B�]V"%����2-�
������!� ����N�����N���PRO_MP_IXZ G��~�( �2��2H���"� ����N�����N���PRO_MP_IYZ H��~�( �2��2Ho@�#� ����N�����N���PRO_MP_IXY I��~�( �2��2/o@�$� ����N�����N���� restrict_table ���idx_array ����	attr �� p_alt_sets_tbl �� cfg_states ��� disp_mgr_data �� disp_app_arr ��A���appl_type �5� rep_arr ��C���gen_db_id �@  �gen_db_type �5�gen_db_base_id ��gen_db_gen_attrs  �gen_db_app_attrs  � gen_db_refs �� cra_cis �� item_data ��K��� rep_comp_data ��status � simpl_rep ��set_by  � lw_data ��attrs  � cra_rbd ��� cond_block �� gen_db_runtime ��� gen_db_data ��� rep_data ���spec_rep_id ��attributes � � app_ids ��act_id ��cur_id ��def_id �@  ��A��� disp_style_arr ��]���D�@  ���  ����K���L��� � �����X ����@  ��� expl_arr ��^���D�@   ���  ��id ��type ���state_type  �cur_stage_ind �� stage_info ��c��� unexpl_subass_info ��� expld_comp_ref ���comp_id �� override_sys �� expld_state �� gen_info ���font �color 
��T�� ���@   �@   ��� presentation_arr ��l���D�@  ���  ��T��� disp_ref_arr ���m���id �@  �type �5�aux_id ���m��@  �����@   �� ��@  \���X ���@  �@  �#FamilyInf
� Sld_FamilyInfo �� drv_tbl_ptr ��#VisibGeom
� Sl|�Ɏ=�=�|

R;��I;�I���b�%��s��_��ӆ!�(~��T�����僙���p���!3�R�h=�)0����tYj5xx����u9�a!\�JO7��j�UX��w��)y�A�w+�l��"6b�%c*�>J�[�w�=��c��!�cz@��C�A8��嵕�
�-h�a��Sp���,���U8X��8�_�6�1��2aM��%�� 4u����Z�O��f?ƨ�K����p_��w�M.�C��I`��J��b�iM�C�;�w��V=}�P�#c� 'i/�@x�A� {��h.4�<����̞η�y{�͡@Wp�}�W����R�N~���a�JgO��̖`G�W��D�ܙ����1i������( ���ƀ�<�ʔZ�Ɵ0w~�	~��3�9��������x@Z�����!����me�k[0ɓ��4?�,�$/*�0���}�1Efl[� 0� �L�#{ϼ	Vŭ=!m,��bZ�?�o�h2<��sI��.��^�U|t._8	*���<�i�Ǒ��!|; ��2�o�%�^�L*_�C�R�-~Hu�S tw�&�̴��i5��~���V�֧�oh�yDI�Z:^����N�d�;!�Yl�`��~�8x�y�6|�����T�C�A\Q�����̸���!�r�.���=�x.]Z��8C�S~��Vs�u(p�`~�*ӃÇg�eE���W%���P�}��1��6��Bt}�ջr��Fu`b�#NovisGeom
� Sle�t-#�(���L������ �v�7(J*��Y/�A���#5�ЙG1�����?�% ə�N��B��%�����T����.�Uz�i{b)%�xŵ�+Í� �oj���.*�G��j�B�!�-ew���N{����{��� [5���,� 1L{�K�O����n���E��s�}�����Nma�.�=Kɜp
G���꨹�	��W�&�@�#V� 5��ˇ(f��wt]ʇ��cq�
CmSp�,�
��0ɳ�
&tb��b�"�ߍ��P*���X>u{�\�$�j��G-�)LqD7J���[EW��C�1&�T���[�ku,Gӓ��L�|j��^*��sE�0�=����:�w<b�<1D���ꈅ
����<.�b�td#ActEntity
� Sld_Entities �� act_feat_ents �����feat_id �datum_id �� entity_ptr(coord_sys) ��type �id � gen_info ��color �sys_type ���ref_csys_type ��radius �local_sys �����feat_id � entity(text) ���text_type "�
text_value ASSY_CSYS �txt_attrib �coord �/�/z�F�J��7��owner_id �height �width_factor � format_info ��thickness ����� entity_ptr(cosmetic) ��type 2�id �� gen_info ��owner_type ��owner_id � text_ptr ��text_type �
text_value ASSY_SIDE_YZ �txt_attrib �owner_id �height �width_factor � format_info ��thickness �� cosm_disp_ents ��� first_disp_ent(line) ��id �� gen_info ��flip ��end1 �FP�[��BFw0V��Ɛ�end2 �FP�[��B-w0V��Ɛ� entity(line) ���*��FP�[��B-w0V��Ɛ-��+qu�H-w0V��Ɛ���-��+qu�H-w0V��Ɛ-��+qu�HFw0V��Ɛ���-��+qu�HFw0V��ƐFP�[��BFw0V��Ɛ� plane_ptr ���attr  ���	�2��	�ASSY_TOP_ZX �����	Fw0V��Ɛ�Fw0V��Ɛ-w0V��Ɛ��/���	Fw0V��Ɛ-w0V��Ɛ-w0V��Ɛ����	-w0V��Ɛ�-w0V��ƐFw0V��Ɛ���	-w0V��ƐFw0V��ƐFw0V��Ɛ���0� �
�2���ASSY_FRONT_XY 
�����Fw0V��ƐFP�[��BFw0V��Ɛ-��+qu�H��/���Fw0V��Ɛ-��+qu�H-w0V��Ɛ-��+qu�H���-w0V��Ɛ-��+qu�H-w0V��ƐFP�[��B���-w0V��ƐFP�[��BFw0V��ƐFP�[��B��0� ��� entity_ptr(line3d) ��type �id � gen_info ��geom_id ��end1 �/v��;��J�L��end2 �/v��;��J�L�A6��C-�orig_len � entity(line3d) ���3���	�6/v��;��J�L�Hv��;��J�L���ASSY_AXIS_X -�: >��-j�d��R�F㺆*W���������6/���;��J�L�A6��C-/���;��J�L��:����	/���;��J�L�HN �;��J�L���ASSY_AXIS_Y -�#V-v(�9�F㢳c'�,��$��&���6%�;��J�L�Hv�A6��C-�;��J�L�Hv��:�'���	%�;��J�L�Hv��;��J�L�/v���ASSY_AXIS_Z -�VC�	-x�(�2
�F��ǋA����#AllFeatur
� SlI �߅6q��x$5�d��S^�i�]����T��Y������N��g/�/���]W|�L~}y�E�G��)�tC�c�*�~ ���5�O��F�B��'���{8E���`��ϐ��!"OPC��r�>���'e� )�<G�Cd�t�a[��R0�m^mw��
M9���mU0��W�������'�"F1-���V��1��~���rı
�O[�k��m�^/��c���(��k�8a���G.��魰�M��H��� ww�Ԛ��=�p�ݢt.�
��>h�B;�-�����Yx�@�h��		�nCx�D����"F1`���	[}_��Tm���������-�7�E��p����Cq�-wi�̥���C��kX訁:�g���b���@���zF]Q
B����8������q�j�=�A�R��"ƫ�>��%;3�.6��_T�=_���}Hx�4�e�����m��3����׊8�,a�A)}B���,�zeFv��Ƶ�����Q�P�G��z���xe1:ڞђ���	�9A����.q8!쬸��Z���]�h�bz��ѫ���1�
O%��~\;v��w�e�*3��4]2�����ŕУ]�z?(g˿c�<�&�G@;|�N�B.�����%�	:�z6�C�+�x����$ݮ~{�7	E�s<��m�A�[�\[^�f��eD�T����R�uj~��A��s��qo�[�a���=����S%���~\�������օ�2ׅE�j��nB W���©%dO6�HD��x��}).ĳ�� Yڵ|�[�4Z#��a��gNή��7V�1a������J Fy�nQ%E��	냶��q��k�OEV���|�x1��Iq}�)q�"�
Z�l\����į�����e��)���x]��1#x���>��� ���pjiaշ�;���;���zR�����6Ȯ�܏�ʛ6��m�ﳿ��g�:�fJb��V���1��'�s
�2X%��A�BF���F8�p��j�Ik'��kɌ6>w�2�([�� �x��],�~wc��S��~�g�`ɖ�	X�fA�*OL���؀����	��/)g��{)>��t��=��5h�$W|�4�XL�4��Z���pε�q���5Ӷaw��6�v���}������1M�5���Q����2������v;��QK+!S��0Z�[��P�{vY���m��m�S���&q��z{I?�:8D��?� �yG�������������������XwM%6�dd	�i?e^�R_T�M8��r Ǖs���M>����O�w���.Y����JG5D�i��#�\�-�}6��� �>����&�Q����+��<!��ٰ
�h��GmZ��0���ES��o�@���]R�I9%��z���VN�����n��RT!��{?�����=,�-�|"�}\�T��U|���w!AS1L�8���z@��%Kڂ�Sp!��~�a���*�B���,��R&O�0y���]��1}Cu���T���[0߻�F.�����ǩ7;�F9P��� �x� A�C
�Võz���0uTC� ���/b	��p�ztaO$42�$�}&��hɈ9�~+ ߈����hF�M3��"�/��A��nN+4OΦ=� ��h#��#v�h������׹�z(������YV��Y��pK�NKh���eLZ����^:��J�����Q�f�� ��g12oE���k�fo�0��4\{�ŝ�	��U��?�Cߵ�Ɖ�Y@DVj�"
B*�)3U��J�;��,a�Ik��= O#���:�����7�Πx��M\�Ԫ,����Ǐ�N�(Pm�q�H=�Sh��%Ų	��9I�oKS���'��I��/��� "��h���NX�gb�,�u�� Yɳ's=u��_���L052>��������$�F������5Ĥ��Q�9�A+�g�����AT�b���鍱ZI�s���w0�Z�Tz�]�Iގ.���-��
dL����C��'Ż�J�����e�:��i��~�*��Xصg���֌�g���zm(�ۖs�M���M+���
{����R3�I�wz�Pq��ɾ�0�Ɯ�R%���#rhu�ꠃ���〷�`����D��fn�m����	z�
�ov����7��s��dSS��-iи��&&(l�ʎE���dcq��TP��x�!��p�퀀�%.��z�OM���.>�ѓ������H��	䤢�w'�b�>;Lr�Ri�H���Ŀw�2ްǂZѩ�N��=�|��*;�4��hpԉ7M�tm�PV���=��x���⥓�*|���%S>���*Z��ҵ/�(:����ژ|E)`%�mo�F�^MX���|���Y��S}�~nw�Q[\:`k�Պ�;�ܱ&���t�Ґ��Ʋ)-�(�\��b6>�vq�Wd��
��O���]Ui���D��}���;��_���3�A��Bv��_cμ��߶xa����1�̔��f�d>�r��WjDE�$�V��g�[�P������ʱ�.'N����t���.���T��]X��:�rkB���UIM���4�{dj/��Ie��Ȅ4�=����M�*�Y�0�vK*����H�F��L,�X���+M�8/cG,u�Sz,��U�Z���ph� �(���ai�KV���D�c��Ҧ�Wfi�dj'�6b�_�n��W�V>��}_�������8=.i����cv~C�"��ٻ���>���^�?r{G�;`�	�-�����c�	���y ���.*(:��]����hΪ-���R~���.�{\0%-�u���Ϛh�^1C{h��ۖ��X��GنM���]�LE��_w��(������h�w(΄;w�R}��Ks��)?g-V�]���̄����}�B�?�����%�,�٤<�6�|�)�JAM;LaS"�E����Vk�"����5�K�q�/#&�ëy��+%�Ab[�<������/N$=Xͣ�S���i��]%�*{*�-or��-�����Ith�b���cZ!+.�E��(�����6��		z��ř�H�
"���D���ĥ���+��9>�E�������v��@ٮ�~���J�+�|n��xl���pF�������a�@^v�;��2D��Yԑ*Z�D�LUg�Ν�1H�%�r@��}]3��%jd�k�-=���s�H~
�!k���#8?&ӰB��P�F�j�> T˞@�o��"nֲ���}�#�y�f�%�O�6���m�^+09�@΂�v���qL�B��HW{��zO���R�M�z�`�ъw��������Nt��lg^IL`�T��~�+D��CKp���R�̐X�p������<��hk#ԃ����&V����i1��[#Y3!,�Q���C`�uv����d�#��>R�;��S �F�OeIkC��z\��/�b�2S.J��&�H�荀 ���o		f������Q��TDP��g&  �Y�@����:�o�RF�|\�z� 4��=ΑF�[z5�I&S%N/ �r?o��q�C?#��l�qY	�҄|c��~:����F��F3��$������(��с�>�Ft�*L�\@� D.su��7��������A�B/�e7 @DSk�7�����\�Y.�4H�L�Y�N���[V��gW�n�r�m_�{ �aWz���|;��z]��9.�S�O���'�=�K�fuފj���J�S�.�BYtϲ_]��S*�Ù�1����p��=��� ;�/3-��6c�q'��~Y�<t䶞��/"�W��I-����`���%��x+D�tate^yM�Rki16�+`�엖]9��=��աYX8��w7�#���2�	����7a�,ե��3or�F�S���x�&��v�������Q�tD/z��U{�+sui�d��[l⪥��3H����P���E7Bp�#RJeRu�-�و��8�'<A��tfyGڏ��r���T�s\�#�q���I_�<z��`W ½<�:�>�Bg��Q]û���U����s��{ЗORe]�[�"fl{�VL[��u�Nh���98���c�:�^�!>����U�.�rXyI������D�eo�z�/��9�N?�Z��İԀ(_�/.�������$������(��с�>�Ft�*L�\@� '���3- �_�5=�nf��B/�e7 @DSk�7�����\�Y.�4H�L�Y����6���0+ִLP�i��!�)�Ɋ�u�Z�ma���l�����[�Y�9\����5̉��Q�b�<�-���S��^53#�-&]�Fs�ܧ��J������ɏ��?�]T[��os�W7w��i�?Xm��1W!�Wis�$h ��I���i#���n@AS;	k5�g�0�m�QO�E�A���Q���>�=ۺ� g�����.�b���m� g�)[�����E���Ѫ����};����w9��e�U;-9#�e��]{�\�{�==Y�������k�93ݖ���
��8ȷ�<.v�}��WB��lLt�'B��G T=eI���6|�%Y��7^�t�E{ ��_*꽜JW�*��;�k�o0�ȧ��v�Q>	�Bᡫ�:������T�{�
͗���%�?F?Y7�9���y�k����]4gh a5	B�.s ��׶"��X�_N��~|��>��&@��S���"E��������k�B��CE�����)E#����"aO;������iKh�gvfy~�G�0���	����Ѐ�a"v�;X%��5�Xv5��҄^H԰z'�}(��"��]É�6�k@�Grv�+���q���Ի����M)��rKKmW��۶��=
B�M-� ��wy���+r��S<*�1㍼ ���3�M}/�j�I�|�η;���T�5� L�D/Uj#��y�u���^��(t��?�q]������P�@�"CK������[�/5Sx��b	��p�ztaO$42�$�}&��hɈ9�~+ ߈����hF�M3��"�/��A��nN+4OΦ=� ��h#��#v�h������׹�z(�����;���ǹoѪl�͠uW^�����/��?�U�<S�R������$? g�-�H��ʝE�GY�Ra�����o"�MZ�Bq�?�͘��	~D�P.xah�fΞs�ղ�B�)V9T��l� LK�~���t�J.��:QN��q����K0ch����>�0��{�;��o7��7�Z��Ք}�x�~�Q�&U�'��2MB~T�c' s��o(1�5N����Z� ��I����<b=�㬛b�뎈$�u�c��Sj�����Cb��*�Zт��l��9� ���9Y��8�l���� Do�8�Q{h0B	/O�o�l?��y�!Ү
eߔ3 ��J�j6�&�n��^A����.�j e�~�}�lq9�P�xn󃩞?.�<��"�*�zw���)o!�v7rM�9gRY� *s�Cj�7�`F�/#��9�&�:=��Z��F�E?Zy���(�Ѿ!�Q{�k��V &�Hn�,�c��;s`o���VA�YD����>>(,Ȫ�������o�e�F�u��}��73-!��:��=m���vg�z1�v�º$�]��GH�C��S+�F���|��!#�����I�hx�m��������F���!��v�5I�7�%��L����h�'�CW�ve,�X_
L�j�bN�� A���*��m��.���x�E|���z"����Ws�������� ��]ߨ��F5��?K:���JH,�Flp��u�+fj�\K��k��8���ڽƎ���I;%�n�i̴-2��-��,�=�О>O���FU�y0��c'����d��´)���|���e�{B�h�9����c/�v�/k��uq��"�Vl&�{�5�Rc�~q��<�7@B'��*K4�S�����Ll��o2�z�=Z�M�$b��S���|EjA�p�V}~���j���_϶ D�����Hc�n�m��Z8c%�������d1����x�Ȇ �]�,R�УN�̪���4�DG�u��}IߓO�Hg����b��C�Oߓ:J7�PT�@�����6ꂳ�Iq�q�ʆ��
d̳��z[ѯ$&Qճ��P~�5>fgY��<#�2���a:�kR5�����ȸ�_����M��@(U3R�OU1��d� �f9C�HB�H��u�*3��=K\���sc���i	���Ҙ�����&)�(�֡by3<'����T�o�¨�������6%(Z����@w �M³ŉY��+�R�������9`�4JL4ILdIA�TO���JR�ڛn��أ�#�_%�hU��9����.�zj�A�����f���_����~�zC�X���c��q�W�vM�f�?�I�RWr����V��9����ڏ�_����tcI���񤕓����Ieu�n|��ϧ�OFczh�E΁��ʗ�k��<^f�Rڢz�_��Aø���0��@ך�u��G��X<���鴗�)��5�c?Z�hQ�)��BEl��S
�2������l��߱d�+cJ��*[KeXȖ�&>-S|���[�bc�R�X:��}	~Mk�މ��	]���TgzJk3�.�X�=��NU���1��:K��LEQ�`��?�Ŗmo�F�^MX���|���Y��S}�~nw�Q[\:`k�Պ�;�ܱ&���t�Ґ��Ʋ)-�(�\��b6>�vq�Wd��
��O���]Ui�����������<���@2�1��ע��@�1\�K�tӄT,}��}���C�Zhxx[K��/�zB�9Q/fx�ˑ�u$� ?ꍢ ��yO�W`+5�}����V�O� �O�/����*��}K���p	oZ9H�omԠt�X�C)�`a�L�مC�%9�q���0b���-%z
� �BJ�����wi��snЮ;�<�x2����
q��c'`L]�^[��A-�ϣ������m�O��̀����FW&�?�h�֨�{d���Bkx��6�eoI��*�<R�5 ���Ô0Ȝ9�I5�W�+q���G���m����n6`��릀tiw;2��(�4V6��� q1j�<Cw�4G�����,*�Ad`;S� �w{WBJ'sX��v/s���t��H
�0��&X���"�Ҫ���q���d�XWd���_h�q���s ��.�b���m� g�)[�����E���Ѫ����};����w9��e�U;-9#�e��]{�\�{�==Y�������k�93ݖ���
��8���7�r�?;���qp!����@皒��H|�~�Y�X��Q^��:�E)����O�R�0���4PB����V*�؀�嚽��2
'T��-���*�=웭��Xn}�M]�oD.J�4��ĲR�lys�ZZ�Yͬrz�C�=CB�Eں�@�rr6�:KOi��xx[��@o�8A\�w}P@�&��	19�S8�r{�����o*�y��{M5�OӴ�$�۹֎��k��M�#����l��v�ź$��R�Nx$���O�	�%XE�a��В�8��Zn���[���s���82�Z�~��BP�2�bg@��r�tL��5`���Ɯ���y-��	��Œ�����_��T��b�u�
S��8Z��x�U!np�@��a�w�O9���G�q�#�"h$�@��Ϩ�I�Y�>��접�`@,�a�c�, .��9؟l��e�v,� ��|�.��C
Fd%H��,Y=R�)a�S��?���2�ׄ�����Q��|.�m;.�x��g�濱��鿎TMJ��t/���J�K�Q�%U��$g��F|.�fɩ�|�&[b�S:++�h6�*^FL]/�KP�	�t]����JE�f�o�7��IK��&ݚ�5��%=&�nf	,�q���֌�7����I|$���C��Z���N����H�=cPo���I��9:�X�eU�~޵����\>FV<�-\�ٌ!�G����^R(�� �} ������>Qp8)AeHu���RDgщ�	��ΨVhD���R~7�3�U���Uq�PQ��yo���)S�ݳ�>�u�ږ��Ǎd���_h�q���s ��.�b���m� g�)[�����E���Ѫ����};����w9��e�U;-9#�e��]{�\�{�==Y�������k�93ݖ���
��8���7�r�?;���qp!����@皒��H|�~�Y�X��Q^��:�E)����O�R�0���4PB����V*�؀�嚽��2
'T��-���*�=웭���y���B8U#V�;�;��+����I��'�}�]�a��C���m����;������)�$��-�n1�L��u��P1�	b]��u����4S�y���j`������R>e��w�ezl�����4h�a�� 80�qHx�Q��'���U��2y
�`͞�'N�������{{zQ��$y��X3:�n��8,����!	+��V�y3�B>.?N������O6��&�\��l��b��u\��q��NQ�49Ҹ9��̅>�Vr��'S`�{�Eꑈ8��-�rAQs3�	�����.�@�����B�� �+�ȋ�jޱ	�K�7}q@�s��	mZ��h�o8^I�n�Vp���rQ"��/���-̄�?n���?sb{�9�p��Se��9�<����7m��� �^�G"q�ɫ٫� ����Ʃ"�:���;�W���u���v�jJ^z�k���n�����c�1N�WA�@K-/���*���������S\A2K�U9����6Z6�s�O��"y=�C== omK�s4S-�Խ���˶��OP�b�VYΒW�>Q!�k@��o�\{IiR�0k|~�J(�)� M*P��Im`N���}��M�u��cB��ޭg��s�! �*��@��I�17��n5}��Wa�'1%��O�\+��!�؝ϻ��ƞg\8��|�xa�ZV���iþ[�^L@�x�X��M�Q�Zf�Q?-��ʑ�v>��š<�/���P/�!X��2�IAJh�d~>k�u�ć���O��lz�'7.�� ���o����Q�w��.N�k���^���r(��m+�a�����wޤq��bQ��b����B���5�!��E�?[���G��[��A�P��O;C��H�B�.�hp1+h��%n����{3���~�l�����v�"I%�.y���`�K�@��Р�qB�G���$��/c�6"����������2��u-me��2��w;;P;z�AP�r�M�ud=��蔌��>�;A���c	�}��s+���1la��'��:�}��,��� ���j���ժ�`m�OMZ�i�ko���>;�f�#h�R�ơ��}
 �]	z�Me9���E_)�9��q���q�-��N�Fv�hqq�m�ǒDE��j���<�B*ڛo<�x�Y=�^A���P|�=-�qyJ�Tw������H�Z��,�a4!�����p(�k/Io���&��L�Q�EH�4i�}l��PJ���0�H��,���[��>d7�<@d�� +�!���^<WHϏ1�93c/b̝�y��/��w�#��﮶�&��J�dR�	�o�`��G�����`�j �>�&i�O����]#/p���&�;� %�)q )$�1�S�� ���0�QܩЂ��KG�h�d�_�"����Ƚ��-��ւf3�v��et'�k=���S%�ϛ�T,���S������a�(O�F��K�v��c�,���:���Ӯä���B�R��㟥��e�,MW;�؎�8Đ�{4W��e�H��]�V �UʌX۠Q]����(�V3Ϭ�O�&����F����b!aYʮ��&�#ad������g6�gKZ�(Ȥ�L��Ge�N�i��^��C
�X(+��ly��r*m�jwH&H�v8��e{�a%����o�n�90�w�O���4���V㦢�5���{	Db@�~T���K��>=� B�}C��)H<F��#E��ީ�w�G�+�%���n��|�&Կr�Y42���ո���m0$%12�v *��W�c���ݶ�ҜS�\� b�rry_]T�{��g�9���5����q)~$�3�_�U�&�;��������ld����n$���*Ez�QWhsV��.\roQ�$.sHϼ���C�qD}AG��U���)1yc/�(�����'�/�����a��-qI�ky`(G�)�#�J�,3�{8�����i��Og��~�o��}��<�J��i��N�Ei�rXhv�� ���Tr\�H��!W6ɪ*V�vad{��(f�:�VY�WDbG%Ώ�~]^�NYJ��1o�:�D�W�縟/3�j�>���r�Ñ���_Mt���Ny�O�8�B;]�����K*j�G	sAC��o	Z�5��,��+ L��nJ#ڱ��2�y��ah��)_[�_"ECiQ��\{�C6(`s�Bi)yK�تC��h֨�}#�6#����}��O�^1C{h��ۖ��X��GنM���]�LE��_w��(������h�w(΄;w�R}��Ks��)?g-V�]���̄����}�B�?�����%�,LE��}��Z����0|]������L����m�z�6�����am����H���
��G�}Y+^щ�1*���Km�����f	���+܆�Y�|Z�yXw��<��bR(���i��?��b�_����cx��o��Z����;��,dSv�M#|0x��I��o��-����[���s���82�Z�&<<Ibz�^����n����w�r�N�O�)oo�n�90�w�O���4��i�gr^�V#&ߙ����1a�P���k���Fo�n�90�w�O���4��IZ�[�y�D���̄a�]�>�S�Xg�?ψR���뗈�k�V"-M4xV���$�g�?�5�l�	��&��4.�?c�����ъ�w�I�K��O��L�$��-fpsⷨ^t���Al���9���;���.����j�Ӵ#!p�ϫ=FR8]6�r�}\�_[ZM�ڱ�B�Q�P��7+HRL�ђ�2����eq���D'��1�]5�KW�S�y���U��^g�e��z���X��U��� ������+LE�4��u*���r��.v`?�[������Nt��lg^IL`�T��~�+D��CKp���R�̐X�p������<��hk#ԃ����&V����i1��[#Y3!,�Q���C`�uv�+\RN�d��9h����9��}�`��{��Ի�/�z�i��"�ݳr'�C��^�Y�����_��Xx-�ި'g-�|��f9[ƣrF�5��X�рC�>Hf���&��f�oQ��ENpq�E��I�C�b���y]����ٮ�0�9yb��-�-%z
� �BJ�����d�dMQ�px���4�;��|"�ԝ9��!J_��f����.�:b��!�*Rg�J�=&��l�tZ��z�7�1��j�VCBi.����^���W�[�F��@YOR��).M���oN��N�B��X�W����?p8�O�G.H`/��/�Ӡ;��1��ßW7�˶�,�@#g�a����_bzy�#�7"�0C��C���C;E���5輧x ���%��"�ƥg�H�� �Kd�ثA4[�
�����8��Q��`��2�n�s�Q�4��- 3K��m�a�J&xҢt29f�m(\,[ۯ@;��W:��
�I�L�&YO�:�Ωڭ��� <�U��ll�����L�=W����-�Ir�o�f�8a�c��;3�KS�>�QcY��i��1����/��������m����F$�=�V���B/�e7 @DSk�7����&^�L�o�H?�:�_/\<��ew���X.v�l�&�"��	1(j9�
����}����;#�\[D~)^À���|���-�8睏|8�;S/	Gl		sT��3H=�;���(1"緪�
i�����U�8�t�8�~%}��������k�B����;CʬJ�2�L�5y�m��b	��p�ztaO$42�$�}&��hɈ9�~+ ߈����hF�M3��"�/��A��nN+4OΦ=� ��h#��#v�h������׹�z(�����;���ǹoѪl�͠uW^�����/��?�U�<S�R������$? g�-�H��ʝE�GY�Ra�����o"�MZ�Bq�?�͘��	~D�P.xah�fΞs�ղ�B�)V9T��l� LK�~�K������:2WA��m��DWw����S����7A*���'��d��QuXC]�e-�6HSܰM�:����D�T�Mצ��!ڊ��z˛�;ڟ<A�V�
ɷ��CUU*T���y�^�\ƣV O�a�hFd���B�C�Ƥz�4����wL.��܋@��g�B�5.B$)�v`�˒��������N�N�-27��7�Z��Ք}�xv��_�����S��6�Y0X�폰kGH��_e�5��B��~��(ɏ)��U]:��JA.Ga�"]l����9i�gszE=��V��Z���T"�|��KŠ��n2��I�ؒ1)��u��D���f�T�6�+G(&���A �;T�	��F�:*�Gb��-�� 
��c�\���v��~�M�ZV�[IV���=v���k�t��Sĵ�s��J��mTHR����n��ܪ�ts�$�l��J�E�M���Aw˛���>EV/R�|�0t�2����oR'�eٚ��f2rSz�C��R�=Vח���K���u�XߕȻ���k�v��.�X�r��)��'�S~t���
~g<�D w��1����vRWG�C[�e���Ds$Q��Q�M��▖d#FullMData
� Sld_FullData �� first_member_ptr �� alumni_members �� ref_part_table �����
name START_ASM �type �revnum �memb_id_tab �����dep_id ��attributes � simpl_rep ��feat_id �� next ����STARTASM D��������STARTASM t��������STARTASM z��������STARTASM ����������STARTASM ����������STARTASM ����������ASM_VIB_MACHINE ��	 ���� first_xsec_ptr ��� lang_ptr ��interact_on �� first_csys_ptr ��� dim_array �����type �dim_type ���feat_id �attributes �� dim_dat_ptr ��value �bck_value � tol_ptr ��type 	�tol_type �dual_digits_diff ���value ���digits �nominal_value ��bound �bck_bound �� bck_tol_ptr ��value ���datum_def_id ���places_denom �bck_places_denom �dim_data_attrib  � dim_cosm_ptr ��� p_sld_info �� attach_ptr ��parent_dim_id �� symbol ��type �text(i_val)  ��������	������� ��������
���	������� �����������������������U��/V�/V��	��
�/V���
�� ������	U��-Q=p��
-Q=p��
�	��(�bM����-Q=p��
��(�bM����� ������U��/v�/v��	��
�/v���
�� ������|U�@ �/ / �� / ���%�   ����� line_array ��0��� text_array ��1���text_type =�
text_value  �txt_attrib ��owner_id �height �width_factor � format_info ��thickness ���1�q COMPONENTS � ���-��|U�@ �����   ������0���1��1���2= ����1�q COMPONENTS � ���-����N����	��(�bM�������$(�bM����� �����-����\���	��(�bM������(�bM�������������\���	��(�bM������(�bM����� ������ipar_num � � smt_info_ptr �� cable_info_ptr ��id ��type ^�ref_harness_id ��cable_assem_id ��cable_assem_simp_id �� logi_info ��work_subharn_id ����parent_ext_ref_id �� p_frbn_info ��
log_info_src_name ��log_info_src_type  � cable_report_info ��
cable_jacket_report_name DEFAULT �
cable_shield_report_name SHIELD �
cable_node_report_name - � comp_info_ptr ��� prt_misc_ptr �� shrink_data_ptr �� expld_mv_info ��� state_info_ptr �� alumni_feats �� p_mdl_grid �� mdl_status_ptr �� appl_info_arr ��T���id ��type &��T��� ribbon_info_ptr ��� p_xref_data �� first_analysis_feat ��� da_info �� setups ��first_free �� id_map_info_ptr ���faceted_rep  � sw_info ��offset �-V���lF�-|S� �0@�-~z�8��#NeuPrtSld
� Sld_NeutPart �� neut_part ��#NeuAsmSld
� Sld_NeutAssem �� neut_assem �� assem_top ��revnum  �accuracy �outline ���accuracy_is_relative  � mass_props �� time_stamp �� colors �� attributes �����
name VOLUME � value ��type 2�value(d_val) /Y �ref_id ���ref_type ���SURF_AREA �2/n ��MP_X �2/ ��MP_Y �2��MP_Z �2H ��WEIGHT �2/Y ��PTC_COMMON_NAME �3�
value(s_val) asm_vib_machine.asm ���� layers ������id <�
name 00_LAYER_AXIS �user_id  �ids �$�operation ���B01_LAYER_CSYS  ��H02_LAYER_CURVE  ��?03_LAYER_DATUM  �K05_LAYER_POINT  �507_LAYER_ASSEM_MEMBER  �L13_DEFAULT_DATUM  ��	� item_names ������
name ASSY_SIDE_YZ �obj_id �obj_type ���ASSY_TOP_ZX 	�ASSY_FRONT_XY �ASSY_AXIS_X �ASSY_AXIS_Y �ASSY_AXIS_Z '�ASSY_CSYS �DEFAULT �@  �5�DEFAULT �@  ���DEFAULT �@  ���DEFAULT �@   ��� members ������id M�type �
name BODY �e1 ���e2 ��e3 ���origin �����NFOOT ��HN /r �VFOOT 4��4��Hr HN �s�*0�WFOOT 4����:#�HN Hr �XFOOT �y�y/r HN 7-��+j��[PLATE ��/~ �\MAIN_JIG ���;ǜ��B#/���;ǜ��B#�#ModelView#0
� View ��id  � viewattr ��attr_arr � �� mpoint ���id  �coord �JH��i�1i�&�A�J9�L��D� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@-z^    (F�z(2{���� spoint �� ��؟�-  ���orient �k��-E1ƴҰ_�o��_�ێA���MC?�qǎ@J�q9�
2��(W�CZ>�a����Z^�"#ڮ��local_sys �a��(c���4gŃ}t�A��X�� j��y��i̻pl��a����A��Θ��(˔��e.g-�-he�q-rLCNKb0F��ت��� world_matrix_ptr ��world_matrix �a��(c���4gŃ}t�A��X�� j��y��i̻pl��a����A��Θ��(˔��e.g-1��� �F`#yci;�F_���O�model2world �a��(c���4gŃ}t�A��X�� j��y��i̻pl��a����A��Θ��(˔��e.g-1��� �F`#yci;�F_���O� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� q,[)��}�� ��matrix ��������Y�� ��/@-z^    (F�z(2{�JH��i�1i�&�A�J9�L��D�}�� �q��py�����>^�1�'c�n2����Zs�]p`�SUc-NP��r���(����>�x�@���-���outer_xsec_index ��next_view_id �simp_rep_id  �
name no name �fit_outline �/Y /UF��>��/� /��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix ����F�d{yk��position ��-���|!��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#30
� View ��id � viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr ��world_matrix �y9�|��k�y9�|��k��9�|��k��model2world �y9�|��k�y9�|��k��9�|��k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���	� ���� ���outer_xsec_index ��next_view_id (�simp_rep_id  �
name TOP �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#40
� View ��id (� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys ��9�|��k�y9�|��k��9�|��k/@/z^F�J��7�� world_matrix_ptr ��world_matrix ��9�|��k�y9�|��k��9�|��k��model2world ��9�|��k�y9�|��k��9�|��k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��s��2đh{�MR�x������A��0g��,r�� PGpxS��щomË8�)A�i���qm� Z��������� �����	� ���outer_xsec_index ��next_view_id )�simp_rep_id  �
name 5_BACK �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#41
� View ��id )� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k��9�|��k�y9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ���#9��i�����A��h�+ i����-w��<)�(�v����p(�t�AT�(��q�GM`��#�>�����	� ������ ���outer_xsec_index ��next_view_id +�simp_rep_id  �
name 6_BOTTOM �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#43
� View ��id +� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ���9�|��k�y9�|��k�y9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��A�+��+yUVB���p��Ol$*Xݕksuzyآ��P��L�ym7TjcA�Ԋ�8w�AϮ-ԗ7P������ �����	� ���outer_xsec_index ��next_view_id ,�simp_rep_id  �
name 4_LEFT �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#44
� View ��id ,� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ��y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��x�zGϩ2F9f�[��*�<YZ�ɤ�<xh��a1���z��D���o�;�\\J\w˼�TKP������ �����	� ���outer_xsec_index ��next_view_id -�simp_rep_id  �
name 2_RIGHT_YZ �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#45
� View ��id -� viewattr ��attr_arr � �� mpoint ���id  �coord �/m`� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �q�oL�|�q�oL�|�q�oL�|/@-o�fƔ|F��t���� world_matrix_ptr ��world_matrix �q�oL�|�q�oL�|�q�oL�|�Fd���9k��model2world �q�oL�|�q�oL�|�q�oL�|�Fd���9k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� q�oL�|��}�� ��matrix ��z���A���}/!(��Q�� Y���(�x�}LC;UA�Ag�V�(�d�<|�\�Qy���X������� �����	� ���outer_xsec_index ��next_view_id .�simp_rep_id  �
name 1_FRONT_XY �fit_outline �/Y /UF�a�i�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��t����view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#46
� View ��id .� viewattr ��attr_arr � �� mpoint ���id  �coord �/m`� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �q�oL�|�q�oL�|乜oL�|�/@/z^F�M�>0��� world_matrix_ptr ��world_matrix �q�oL�|�q�oL�|乜oL�|�Fd���9k��model2world �q�oL�|�q�oL�|乜oL�|�Fd���9k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� q�oL�|���	� ���� ���outer_xsec_index ��next_view_id 3�simp_rep_id  �
name 3_TOP_ZX �fit_outline �/Y /UFz��qK-\/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��t����view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#51
� View ��id 3� viewattr ��attr_arr � �� mpoint ���id  �coord �-J��K�-A�{�{FD�f�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����Y�%�c0o�׏�)A�Q.�v��jL"�w����<7���n��9��m�5/6���u��U7���local_sys �ӟ��!W�̨��7Bi�1��ZW�AĹ���\� ��*Kհ���x��?��x|kA��:��Ŵ���]�-�Ț^��-w�2y� LF�Ҽx)��� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���}�� ��matrix ���=���l�Z�C	�(����S*M��7[��cuW	���A���o��(�8����tAϭ���� ��+�����-�� ��B���������outer_xsec_index ��next_view_id �@  �simp_rep_id  �
name 0_ISO_BACK �fit_outline �/Y /UF�i�5P�/� /��� det_info_ptr �� named_view_pzmatr ������page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��B�F��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#1073741828
� View ��id �@  � viewattr ��attr_arr � �� mpoint ���id  �coord �/o@� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �xyT�ۢ�c�:�+gk���RFW n�:���rv�P�A��?6���]�AmR@�8Q���local_sys �olE�T�r\D�ȑћA����f��D�j���s�A�x8�Ӟ�^q�o�Jdc�u�r-�F�o�-s��e�F�z�/x�F� world_matrix_ptr ��world_matrix �olE�T�r\D�ȑћA����f��D�j���s�A�x8�Ӟ�^q�o�Jdc�u�r-Jk��!��FZ�~k�F_>�5|+1�model2world �olE�T�r\D�ȑћA����f��D�j���s�A�x8�Ӟ�^q�o�Jdc�u�r-Jk��!��FZ�~k�F_>�5|+1� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���outer_xsec_index ��next_view_id ��simp_rep_id  �
name DEFAULT �fit_outline �/Y /UF�T�����/� /��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-���|!��view_angle � explode_state ���model_cosm_rev_num  �zone_id ��#DispDataColor
� ref_color_table �� ref_color_arr �����maximum �)�fcQ��R)�f�ambient )���diffuse )���specular )��spec_coef �transmission �threshold Z�gl_ind �highlight ���reflection �bump_height �bump ��decal_intensity �decal ��texture ��
name �� surfprop_ptr ����cQ��R�)�f)��)��)�Z�������r\(�\)ٙ)��)��)�Z�)��������#DispDataTable
��r�o��x��|E4�J�zՕ?�;��f��~yl�d�6ms=�Ƴ�?�BWOeH(z�xJ��	����c���J��긆���ˡ�6l�Wy���H���^�X�[-b�ҍ�'��6�w?y�g��7)�J��ʊSJ-&�a�0Ta�fSA����e�d�u�QY4�\��L��k瓄���`��6E?�v����\�OI	���(�Go�U!Q.i�rh>6��>nKD�
��s�_̖����<p���EPFų|�����AI�ó?~�4�S�a���z����Y��Z=@�T6:�;�e�I��9U����_�\�Z�5+Z&��{��!��1,�����"1\�݃�5�]�K�Wdz9��R'��iS�$���a_�����v)�ղj�}LmGz��>8E�-��#L��vE�%� 3%5���v�@�9R�8�tV.0[@�<|�$��v|�p��'}һ�p�[�̷c�^����Þ�X8y�� q���Փ� ���y�ŋ�GH3�b&�x��ط�,WX�������\�k��~�J^-��4��*���,�,�
_���-�.�JY�K�W�Q�p�ȯ�jG�5��'ϥ؍J�\ܔ0��t3��8p�Y7f�d�&���1˿܊��So=�}��b��:�c#�(?R�0r�{�?dG�e�f�6�2�}�n��l�?���dC�X�t~UYP�"eP�ɥvuo�����5�i�"�D�1�g�0L�|��U}Xd�M�d �`��$Ƥ�$(j�Gx��p'Rv����)3q(����/�s�b����^�d��	��H�EI^��JOn]���:F�ǭ��G�*�JSt�	��L{-b c��I��vUG6 �X���������FZЄ����#��6	��/ʺ�h�|�DG�MO#AssMrgPrt
� assem_merge_parts ��#IndentTag
� FiG}�W]r��s␓�|�-O�/����I�������Ar��e������"�4�-�y-V^��F*�ȶ�<�8)�J�oU����C^��t*���?d��'�D����)B[WTޢ����\����_D��O��51:o�H\�g|I,d�k�O�ϊR,����Y�rzN"��&��'�8 ���zPhT��(iZ�2��4���
Q_�8!���d�\:�(��f��E-O���Юi�hV���	�P�M�1K}k��.��1$ �+��If��YC�O��.ݖd���������l�/�Y޵�u��Q;�Yϙ�3� K��rjh�﫚���.����n�nV��v�I��}{�Zƴ���!Rr�)��#׮:}8|l��P�Z�xz�ɿ��x�s��&���IA��2�A�������/UD�[l��37}y-���.���J���*�.F>��I�~L��HL��ߔҖ�>I��'�=/$��q���Ɵ⟾����_����ʩ2�n�J_�������.����#iV�{�.4���B�����Y����A����H�ň��D��&R~���ҬYɕT1$J����UEhQ/F���[/��]�.��l!�!�$N�I�.���Xs��{
.��L�~�����t��YV�(R�)���3qd��� z�J��m��5H�_+��А���F0�~BI�u��l��3@��ǥ=E�Ok��~��ٟ���nlz�W�}�kC+_)*�1���r1�����O�%�����y�ڲ1���k�d��(xݸ(Y��*��,�\�{������=wq�^~���I %J>�}����G�3t7�&��~>���#FeatRefData
� Fe��j����]m�&�@��y�Q6.�3���F��y�]�~o:���P$ʙ�M�F'�.��\�9kh ���mŪD�)�d�k�(��۳��5�!:�����;*��FDNO!T�ꛌ9��[Ǎ���n���zc�3����RB4wu+)�2s�n�T~�3,�Ĕe�S+^�eq��u�3�i�h����
�N�dD�f2����B��X�=�=߃�ߒ�i,��a�c}��T�c}�\4e#END_OF_UGC
