#UGC:2 ASSEMBLY 1315 620 8 1 1 15 2500 2004320 00000ae2 \
#- VERS 0 0                                                          \
#- HOST                                                              \
#- LINK                                                              \
#- DBID                                                              \
#- REVS 0,                                                           \
#- RELL 0,                                                           \
#- UOBJ_ID 1111145974 344355723 1987995311                           \
#- MACH _Windows                                                     \
#-END_OF_UGC_HEADER
#P_OBJECT 6
@P_object 1 0
0 1 ->
@depend 2 0
1 2 [3]
2 2 
@model_name 3 10
3 3 ASM_VIB_MACHINE
@model_type 4 1
3 4 1
@revnum 5 1
3 5 302
@bom_count 6 1
3 6 1
@dep_type 7 1
3 7 2
@stamp 8 0
3 8 
@major_vers 9 0
4 9 
@vers 10 1
5 10 0
@alias_name 11 10
5 11 
@minor_vers 12 0
4 12 
@vers 13 1
5 13 0
@reason 14 10
3 14 
@comp_ids 15 1
3 15 [1]
4 15 77
@comp_dep_types 16 1
3 16 [1]
4 16 2
2 2 
3 3 DAU_JIG_SHOCK
3 4 2
3 5 809
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 86
3 16 [1]
4 16 2
2 2 
3 3 ASM0001_CIR_OUTER
3 4 1
3 5 2247
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 78
3 16 [1]
4 16 2
@dep_db 17 0
1 17 ->
@dep_db 18 0
2 18 [4]
3 18 ->
@to_model_id 19 1
4 19 0
@dep_type 20 1
4 20 2
@obj_id 21 1
4 21 77
@obj_type 22 1
4 22 3
3 18 ->
4 19 1
4 20 2
4 21 78
4 22 3
3 18 ->
4 19 2
4 20 2
4 21 86
4 22 3
3 18 ->
4 19 -3
4 20 4
4 21 7
4 22 242
@model_db 23 0
2 23 [3]
3 23 ->
@model_t_type 24 1
4 24 1
@model_name 25 10
4 25 ASM_VIB_MACHINE
@generic_name 26 10
4 26 
@stamp 27 0
4 27 
3 23 ->
4 24 1
4 25 ASM0001_CIR_OUTER
4 26 
4 27 
3 23 ->
4 24 2
4 25 DAU_JIG_SHOCK
4 26 
4 27 
@usr_rev 28 0
1 28 [1]
2 28 
@name 29 10
3 29 SMLEE
@mod_time 30 0
3 30 
@tm_sec 31 1
4 31 53
@tm_min 32 1
4 32 32
@tm_hour 33 1
4 33 12
@tm_mday 34 1
4 34 18
@tm_mon 35 1
4 35 2
@tm_year 36 1
4 36 105
@action 37 1
3 37 1000
@comment 38 10
3 38 Hostname: 'SORBON'
@rel_level 39 10
3 39 
@rev_string 40 10
3 40 
@revnum_std 41 1
1 41 318
@flag 42 1
1 42 0
@name 43 10
1 43 NULL
@inst_hidden 44 1
1 44 0
@old_name 45 10
1 45 NULL
@user_symbols 46 0
1 46 [1]
2 46 ->
@class 47 1
3 47 1
@access 48 1
3 48 0
@changed 49 1
3 49 0
@copy_of_generic 50 1
3 50 0
@name 51 10
3 51 PTC_COMMON_NAME
@value 52 0
3 52 
@type 53 1
4 53 51
@value(s_val) 54 10
4 54 dau_shock.asm
@id 55 1
3 55 -1
@type 56 1
3 56 -1
@appl_data 57 0
1 57 ->
@bounding_box 58 2
2 58 [2][3]
$C077453B696EB4DA,C050D4EDA5BAD368,C077453B696EB4DA
$4077453B696EB4DA,408EDA9DB4B75A6D,4077453B696EB4DA
@ver_key 59 0
1 59 ->
@time 60 1
2 60 1111149148
@random_num 61 1
2 61 1021460835
@hostid 62 1
2 62 1987995311
@last_read 63 1
1 63 -1
#END_OF_P_OBJECT
#Pro/ENGINEER  TM  Wildfire 2.0  (c) 1988-2002 by Wisdom Systems  All Rights Reserved. M050
#UGC_TOC 1 32 81 17#############################################################
SolidPersistTable acd 304 1 1315 10 2 4558 349f#################################
SolidPrimdata dd1 fb 1 1315 10 2 6697 e4ed######################################
BasicData ecc 31e 1 1315 9 -1 4333 827b#########################################
BasicText 11ea 33c 1 1315 9 -1 540b 7901########################################
GeomDepen 1526 16c 1 1315 9 -1 92b8 88fc########################################
DispCntrl 1692 2c9 1 1315 9 -1 3e25 c169########################################
DisplData 195b 29 1 1315 9 -1 0cac b9ca#########################################
LargeText 1984 1519 1 1315 9 -1 7369 fb3a#######################################
FamilyInf 2e9d 2d 1 1315 9 -1 0e87 f0a9#########################################
VisibGeom 2eca 2fc 1 1315 11 -1 6bb3 8237#######################################
NovisGeom 31c6 172 1 1315 11 -1 bb60 ffa3#######################################
ActEntity 3338 6a6 1 1315 9 -1 db21 5a37########################################
AllFeatur 39de 2278 1 1315 11 -1 fd8b 777d######################################
FullMData 5c56 7d0 1 1315 9 -1 5e5a 14bf########################################
NeuPrtSld 6426 29 1 1315 9 -1 0cfc be04#########################################
NeuAsmSld 644f 3e8 1 1315 9 -1 60bb 137f########################################
ModelView#0 6837 400 1 1315 9 -1 ac70 f5c6######################################
ModelView#30 6c37 350 1 1315 9 -1 57f9 9b46#####################################
ModelView#40 6f87 3c3 1 1315 9 -1 9957 ed58#####################################
ModelView#41 734a 36f 1 1315 9 -1 6f62 2bde#####################################
ModelView#43 76b9 36e 1 1315 9 -1 6cf0 10d2#####################################
ModelView#44 7a27 36f 1 1315 9 -1 6f42 d074#####################################
ModelView#45 7d96 3e3 1 1315 9 -1 a0bb 4586#####################################
ModelView#46 8179 2f5 1 1315 9 -1 304d 1184#####################################
ModelView#51 846e 3c4 1 1315 9 -1 9d36 b6e6#####################################
ModelView#1073741828 8832 3fe 1 1315 9 -1 a46b c8b4#############################
DispDataColor 8c30 1d3 1 1315 9 -1 ca10 8ab7####################################
DispDataTable 8e03 363 1 1315 12 2 ad8c 40c4####################################
AssMrgPrt 9166 21 1 1315 9 -1 09c5 6a48#########################################
IndentTag 9187 36f 1 1315 11 -1 b707 def3#######################################
FeatRefData 94f6 101 1 1315 11 -1 7aba cfcc#####################################
################################################################################
#SolidPersistTable
�� �9���2r椙C��0bؔ0\��|IC  �a��!��2y� �4o~y#F�p��)cF&M:yx��M�6=��4��c��3��L��)R�S�y�N�y��gC%U6iܔY�'�԰�X�8��7r��|C.�4mQ�W,��t��SG�[�*���3M2��(�*H�a�q3f0X�)��al��7p2�I������a�.s�M����r��9�{s�Z x�ܛ���@��y��3'Ԇ@%����:\��1}�1��.���v��{��È*       ,�CP�1�v�P��Nn\&�rt�!Gb��}��qVZ �Gz|�Z"�� �0  � ���GZg|ڌu0�]��Qst��}��Vr�@<7���� ���>�4�]v�ӒM�E��Ie ��@݋b� ��@QG��W�)��<҉3��g �X@G�@��X��="$���`��fG��G� �c�i� \u�iZ�rxQ�	k�G�����OA0�N���^�y��{��gz.��\He���E�S�.��a��х8فY[ŉ�o����}��6��Y���1�:`�wA�㑬c��F�y�l�AZr�y��oB(!�)�k$�Jj����P�JV�ƙ�Q���0K-a���!�C�1w�==�j]A�ƤT���PE�f�OJ�i �gQ�q�+y�zz�	~ �^�`�e�-b*>h�����u �, #SolidPrimdata
�� �9��9iڐ	C'̗3r�ԁ�6e �(��A�
:�(qΗ0r� ����},x0�#�0� 6o�|��M�/`  8���2fL��G��{^n��ԩ���p-XFΜ4s��y�\�7b�P�#p	��-��T�fȖq3ほ/~IC��3V�&Ȇc޸a��Y �x��X��W�p�����8
� v�Cg&�4t�������č�s @�q�X��w#BasicData
� Sld_BasicData ��tab_key  � tab_key_struct ��type H� tab_key_ptr �h� cmprs_tabkey ���type �� revnum_ptr ��geom_revnum �*�cosm_revnum � tol_default_ptr ���type 
�value �(�bM���(6��C-#�����h�#�������ang_value ���lin_digits ����ang_digits � ��standard  � iso_tol_tbls_ptr ��datum_count � �axis_count �insert_layer_id ��layer_count �rel_part_eps (S�]� ��model_eps 
�fixed_model_eps �assembled_count  �part_type �first_ent_id �9��first_view_id  �outline �Hv�HN Hv�/v�/��/v��dim_num � geom_lib_ptr ��attrib_array ��   �    �� A  ��max_part_size �
model_name DAU_SHOCK_X � disp_outl_info ��disp_outline �FwE;in��FP�����hFwE;in��-wE;in��-�ڝ��Zm-wE;in��� sel_outl_info ���outline �FwE;in��FP�����hFwE;in��-wE;in��-�ڝ��Zm-wE;in���#BasicText
� Sld_BasicText �� assem_mass_prop_ptr ��type  �density �init_dens �rel_density �init_rel_dens  �aver_density z�G���;�vrevnum �!�volume �A��+���cg ��d�c�	-r�Q��sF{C��<��inertia ��B��)���
�c����A�d!�����
�c����B�������i�j�Z���A�d!�����i�j�Z���B��ɽ,��eps #�����h��surfarea �ARʏ��.��csys_id �� mass_prop_ptr ���   ����� assign_mp_ptr ��
mat_name_ptr �� unit_arr �����type �factor (�(P�B�
�
name MM �unit_type  ���-�9D�݁TONNE �(24Vx���SECS � p_unit_info ��� custom_unit_arr ����������� xp_custom_sys_unit ������_id ��_obj_type �
name Custom �type �
unit_names �mm kg  sec C rad �_unit_ids ��!�
principal_sys_units �millimeter Newton Second (mmNs) �_principal_sys_units_id 3�history_scale ��_version �_revnum � dsgnate_data ���#GeomDepen
� Sld_GeomDepend ��frst_geom_ptr  � fem_data ��first_quilt_ptr  � topol_state ��last_regen_feat_id N� incr_time_stamp ��time �B:ɿ�random_num �p�M"�hostid �v~f�� asm_comp_map ���comp_id ��attrib  �
uniq_name �� asm_map_sys �� time_stamp ��vis_revnum ��invis_revnum �� geom_own_tbl ���num_mech_connection  �num_feat_regens q�first_lo_ptr � �#DispCntrl
� Sld_DispCntrl �� color_prop_ptr �� layer_array �����id 5�type u�user_id  � weird_wline_ptr ��
weird_wline 07_LAYER_ASSEM_MEMBER � layer_contents ��� layer_compiled �� layer_rbd �� cond_block ��saved_operation ��deftype_num ��attributes � ���<u �00_LAYER_AXIS �� item_data ����� item_data �����type � id_data �����id ����$�revnum ���
���������������������$������ �?u �03_LAYER_DATUM ��
����� �Bu �01_LAYER_CSYS ������������������
��������������������� �Hu �02_LAYER_CURVE ��
����� �Ku �05_LAYER_POINT ����� �Lu �13_DEFAULT_DATUM ����������������������	����
����������������������	������� �#DisplData
� Sld_DispData �� disp_data ��#LargeText
� Sld_LargeText �� names_table �����id �type �
name ASSY_SIDE_YZ �	attr  ���	ASSY_TOP_ZX  �ASSY_FRONT_XY  �ASSY_AXIS_X  �ASSY_AXIS_Y  �'ASSY_AXIS_Z  �ASSY_CSYS  ��@  �5DEFAULT  ��@  ��DEFAULT  ��@  ��DEFAULT  ��@   ��DEFAULT  � relsymdata ��� relsym_set_arr �����id ��type �	ui_order �	ui_order_fixed  � symtbl �����
name  �sym_class '�attr � � value ��type 3�
value(s_val)  � bak_val ���3 �access_fn_idx ��dim_id ��appl_int  �
description �� sym_rstr �� sym_bak_rstr ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �2�value(d_val) /Y ���2/Y ��� ����VOLUME '� �2/Y ��2/Y ��� ����SURF_AREA '� �2/n ��2/n ��� ����MP_X '� �2/ ��2/ ��� ����MP_Y '� �2��2��� ����MP_Z '� �2H ��2H ��� ����WEIGHT '� �2/Y ��2/Y ��� ����PTC_COMMON_NAME '� �3�dau_shock.asm ��3dau_shock.asm ��� ���� rel_data ���attr  � relset_code_tree �� relcode_tree_child ������node_type � node_data(simple_data) ��type � node_val(loc_handler) ��own_id ��own_type �relset_idx  �sym_id �
src �?0 � relcode_tree_next ������� �� node_val(const_val) ��3 �(("" �� �� node_val(func_call) ��code -�ext_func_id ��postfix_id ���(MP_MASS �� �node_val(op_code) ���(= �� node_data(line_data) ��code ���line_no �
src ) ��)������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�.��MP_VOLUME �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�/��MP_SURF_AREA �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�*��MP_CG_X �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�+��MP_CG_Y �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�,��MP_CG_Z �� �/��= ��0��) � �  ��� relset_bak_code_tree �������� �!��#�� ?0 ������ ��*�3 ("" �� ��+�-��MP_MASS �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�.��MP_VOLUME �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�/��MP_SURF_AREA �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�*��MP_CG_X �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�+��MP_CG_Y �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�,��MP_CG_Z �� �/��= ��0��) � �  ������ �����PRO_MP_SOURCE <��~"x �3GEOMETRY ��3GEOMETRY �� ������MP_DENSITY -��~"x �:� value(NO_val) ����:��� ���	attr �id ��set_id ��unit_id G�origin �����6��G���PRO_MP_ALT_MASS J��~"x �:���:��%� ������������PRO_MP_ALT_VOLUME J��~"x �:���:��%� ����@�����@���PRO_MP_ALT_AREA J��~"x �:���:��%� ����;�����;���PRO_MP_ALT_COGX K��~"x �:���:��&� ������������PRO_MP_ALT_COGY K��~"x �:���:��&� ������������PRO_MP_ALT_COGZ K��~"x �:���:��&� ������������PRO_MP_ALT_IXX K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IYY K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IZZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IXZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IYZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IXY K��~"x �:���:��&� ����N�����N���PRO_MP_MASS =��~�( �2��A���5;����2�A���5;���� ������������PRO_MP_VOLUME >��~�( �2�A��+����2�A��+���� ����@�����@���PRO_MP_DENSITY ?��~�( �2z�G���;��2z�G���;��� ����G�����G���PRO_MP_AREA @��~�( �2�ARʏ��.���2�ARʏ��.��� ����;�����;���PRO_MP_COGX A��~�( �2�d�c�	��2�d�c�	�� ������������PRO_MP_COGY B��~�( �2-r�Q��s��2-r�Q��s�� ������������PRO_MP_COGZ C��~�( �2F{C��<���2F{C��<��� ������������PRO_MP_IXX D��~�( �2�B��/�����2�B��/����� ����N�����N���PRO_MP_IYY E��~�( �2�B�"�`~��2�B�"�`~� � ����N�����N���PRO_MP_IZZ F��~�( �2�B���{�@���2�B���{�@��!� ����N�����N���PRO_MP_IXZ G��~�( �2���d!�����2���d!����"� ����N�����N���PRO_MP_IYZ H��~�( �2�Bi�j�Z����2�Bi�j�Z���#� ����N�����N���PRO_MP_IXY I��~�( �2�B
�c�����2�B
�c����$� ����N�����N���� restrict_table ���idx_array ����	attr �� p_alt_sets_tbl �� cfg_states ��� disp_mgr_data �� disp_app_arr ��A���appl_type �5� rep_arr ��C���gen_db_id �@  �gen_db_type �5�gen_db_base_id ��gen_db_gen_attrs  �gen_db_app_attrs  � gen_db_refs �� cra_cis �� item_data ��K��� rep_comp_data ��status � simpl_rep ��set_by  � lw_data ��attrs  � cra_rbd ��� cond_block �� gen_db_runtime ��� gen_db_data ��� rep_data ���spec_rep_id ��attributes � � app_ids ��act_id ��cur_id ��def_id �@  ��A��� disp_style_arr ��]���D�@  ���  ����K���L��� � �����X ����@  ��� expl_arr ��^���D�@   ���  ��id ��type ���state_type  �cur_stage_ind �� stage_info ��c��� unexpl_subass_info ��� expld_comp_ref ���comp_id �� override_sys �� expld_state �� gen_info ���font �color 
��T�� ���@   �@   ��� presentation_arr ��l���D�@  ���  ��T��� disp_ref_arr ���m���id �@  �type �5�aux_id ���m��@  �����@   �� ��@  \���X ���@  �@  �#FamilyInf
� Sld_FamilyInfo �� drv_tbl_ptr ��#VisibGeom
� Sl��($��a�����T�!St����Z���|6V��G�&��5J��J� ୁ8'���G�]�0x$���҄l1����L�[�	$;��.����A�� �&/޹��|���,��W�֖�X�I,L�v)���QC�5)�{�c(j:�/��Nxt�A9<\��))aj"�:V�P��@8cBŞ��\���Mǰ(?����e]���C��g����n��NO}V3u�\��h� ��3̈)�m%����� ��eQ���|da�W�G��Rk*���ϯ��)�����G�'��6�^��J�V���э�:ZX_��1=�.�O;R���*�������*]��;9���
�!@�g��� ��3�<CT:�_B�W��w�L-�0p$1�h�-Max[��]Xs O��j×m���&�ւX��O&z]�f#���O�:�Ք
�����t[h�p�� �,�dYê0n��P��{yr�93;�Ir@����������`�N.O\�"�Q�[���/_��k���ְ1�/cn9'�������0�#3�NS^w�Dx�͠�w�)�������k�25�OC��#���]��-x
���Q�C�C�9M�N�;��6�C'T�/�&�k5t�(UB}I�~RlX<��0J�g��E�x�5�w���%��5l$0�~�'�x-�Q<�m�'e��}����3f��a=�!��R�䓹�^�01�ջc:��0&���`=���)t}�ջr��Fu`b�#NovisGeom
� Slke:����g1����m��_�)��V��>]�{�\�7�'\%��g#�^<:2�!吊T[>�J^��\�]!q;�j�[-�LC���
�����˶!��L�PZ�sC���]�K��:©���8��w��%ʰs����(��=q��*׏������}=d�����ަ.�`���J��qF�˜!!��߱}T�nTmLH=SR@"( �K���2~i�]/�z[����"�|s`���ܼ}�*�	U�;WT�ls�xx1W�('����z�H�[��˩?Λ��T��Ҡ���e�.�F�nМ�r�P�����p����
0���H?.�{�bR��{+��F�Jk��ӕ��U�td#ActEntity
� Sld_Entities �� act_feat_ents �����feat_id �datum_id �� entity_ptr(coord_sys) ��type �id � gen_info ��color �sys_type ���ref_csys_type ��radius �local_sys �����feat_id � entity(text) ���text_type "�
text_value ASSY_CSYS �txt_attrib �coord �/�/z�F�J��7��owner_id �height �width_factor � format_info ��thickness ����� entity_ptr(cosmetic) ��type 2�id �� gen_info ��owner_type ��owner_id � text_ptr ��text_type �
text_value ASSY_SIDE_YZ �txt_attrib �owner_id �height �width_factor � format_info ��thickness �� cosm_disp_ents ��� first_disp_ent(line) ��id �� gen_info ��flip ��end1 �FP�����hFwE;in���end2 �FP�����h-wE;in��� entity(line) ���*��FP�����h-wE;in��-�ڝ��Zm-wE;in�����-�ڝ��Zm-wE;in��-�ڝ��ZmFwE;in�����-�ڝ��ZmFwE;in��FP�����hFwE;in��� plane_ptr ���attr  ���	�2��	�ASSY_TOP_ZX �����	FwE;in���FwE;in��-wE;in����/���	FwE;in��-wE;in��-wE;in������	-wE;in���-wE;in��FwE;in�����	-wE;in��FwE;in��FwE;in�����0� �
�2���ASSY_FRONT_XY 
�����FwE;in��FP�����hFwE;in��-�ڝ��Zm��/���FwE;in��-�ڝ��Zm-wE;in��-�ڝ��Zm���-wE;in��-�ڝ��Zm-wE;in��FP�����h���-wE;in��FP�����hFwE;in��FP�����h��0� ��� entity_ptr(line3d) ��type �id � gen_info ��geom_id ��end1 �/v��;��J�L��end2 �/v��;��J�L�A6��C-�orig_len � entity(line3d) ���3���	�6/v��;��J�L�Hv��;��J�L���ASSY_AXIS_X -�: >��-j�d��R�F㺆*W���������6/���;��J�L�A6��C-/���;��J�L��:����	/���;��J�L�HN �;��J�L���ASSY_AXIS_Y -N�>�-zݮ��<F��N����$��&���6%�;��J�L�Hv�A6��C-�;��J�L�Hv��:�'���	%�;��J�L�Hv��;��J�L�/v���ASSY_AXIS_Z -��)s���-���o�
F�͒{_�-��#AllFeatur
� Sl�x$ܢ�wlq�����$�*�z��|G���yz�gf+f���*}m���J�F�-�{�~�#	���G
A�~����5ͮ���ܲZ�]u���)����x��p�je�f��h�N}H��������,BVg�#@=Nt���K(��*�'H=)G��R�kH�5���^�L9��/��<r��D�$�
桷�E�2ޡ'|{(PT~-
��V �'k�p(^��4�g��wu��Z����݆STx��Hma0�!�`�u�'C����,��fc�D�\�!��O���%��b>G�J�ng5��L�r����܄�c�+���4��U�Bȗ��T.���,��ܺT�p��ن����?j�Ϯ/�-A�<�\��R6j��FoK������ �
�h�^XP�I	@=�F�F�Q�60Fs/�b�|��7{��&��/ih�~��i����%�?p�I��-�洄�u]�r��\R�^�3�a��8�"���Q����n��^;^����(e��XC������?F��&Ƀ=�#M��*��0�,.M[B�ĺ�j�����*eO]���4ӏ���[E5Y�Kڜ���bx]8kyﻴ'|R1�g�W�$�`Yp߅�#�N�	)��`�G�&��0nM6��1�z2�]�c5��I�U���4Q�>�	&�:�����bH)ٛ�#��.���Cə�0�С�DfYJH��#~��+�ae��,�UH��u����oC�G4�m��K��*GoX�B������pc�,A��yO{��f�M�%\ă>"s,To�$3E��Z�3�F��̞�O��U�r�/�=zY0.A�~��Xo��p ���5FzѨT|�i�w)+O��9aX�2�~��Ka�~t[��u���Q�^.okx�L�%�b���O�Ò�}���<Y�k�{���;�'���t�e�
����1v��yp(�?V���g:�S�~�F�#^.�fxW��ι��2Itش�^���:L#+��u�(W53�tܑ�UL��H����d�Sp>a�ȡ�Ǣ�,�l�'��Y����y~����<�y��YUh%p�F�s���fb�� ݊5y1�ͩM"�_��4��`�
N`���b�;0�3łSwG3�z�,e����.3��ٚ#�W�1�(t�+Zt�3
��Ja�Z'�����:��a�ʊ-���S�������������dM�Q��3'ƅVx�b�T^L���N�ʖ�B�:`R�"ÿ���۩FV�u7�dY�U���йV�)?ku=�O��!?/ܷ�+�$��*S��e)�z�ܔ��O��B�mz�C����18n�b���y�� �I�(�|N銺m�3�w\þ��iS���T'�GX��V̚�,52�+�;?�P�䡗\��O����m�;��L��'�}~�V�>P��5��s�́_��E�������B�~u�Q����QU����gJ1��������Z���:\�.��<����BC!��h��S�@�4�n	a�KT���r������ٙ��jFvq"җ'���y�5(	5N	H0�I ��;>w��-µ��k}<5c&�:]��Vj$����"Q�BZ�]Nõ<՜Q��S������b�{\�
24��O��%&�fQ;�b-ʰ�v�3 ��3N��T*���I%q8��A���}ŭ �22@�;�57T��&�qCN9��|�jӟ��)Su&�|���9�fo���C�=�O���ث��]i�lވ৴.����W�:&^�V����:��7K� )��LO�mVc����)q��JU���'���S��ʃ��)p7@YΖ̽����u���gc2�L�2u-��Q���H�t#���%�T�9 �����|�����7ӿ�;A�@W]G��d��o���/�"YUF�B��;�-_�5���B�}-���4-��8�����D�!Ƿ_�`���7�B����y1����&{���\'���y��K8����WÎ���Mp�Pj��prw�XZJ�	F���S_�Y�x����kaJ�aV�G�;��E�!y{��,�(S~���/�܄+��yZ(��ʙ��a���J�-��t�� &�]?�b`?��DЪ1FVXox����n�8=�c]'��BS�P8~��H�%�t��N!m���D��i�-H�N����$
�P� }����v ����@� ��7�} b��c�hw�1���]/�Qk%�y9�b�T�f�i���J��u����h+�L ���D���=k�4�;�(�v�MzZ�,���D
 �'��G%���4pu�z���L,V���q+�m�V`���" O�B��'*>���k|]��`�!Sy�u'"�D�\����;sLA��zi{�r� \ b�@�5$��*(�-�!-���U�Ń�n�u�� �Q,`1-�$f^b�
O(�4o�wSi�s��=TP�9G�V����Nw�	\>�2��j�ȭ�	�"�d��K^����p�Z<1��D?���L�F ѴN[�W�9�t��=�`4&I����o-A"���57��-�x������%[ax�W���ў%��P�&�G�����:�e=d<#b���?dA�p�6�F�s�a�]FP�UB6��8��=UZ��cU��Q-t�~��U���x3+�+R3������oc�0:���º��&�ۿ��eu���I6'$M��,b9J�(a�߳͹A�ۄb�u�UT��m*��j*�:��-�㕉�&%��_W�X؅Fe���|L��� ���J��29l>�i��"I�C4�;��w�|6���}@���\^,�/A��y���H�f^=��>�<�t�C[HN"�ҿ�(u7/+qZ7~�i�$G��x���VU/OWN7O��: �u
���)��o���l�%.�Z9�|�����Bn�t���m�K���?7�<w�kY m��4p�	�+i$B�U�~��� 'rʲ�dP���s��x'��g�:���K.�L�� B8*B)�X˰�����T8���=]:u-Ud �;��Q���͑7����.������]�vp(jh1����_�s�oIZ��t�*̍�M�wPa߭�E�q�^{w���~[f�5u�`-�koy�H��ő�P�D�M70i�Y'qٓ��(~��������ʽk\��~�jO�>!�v���U��w���]�bE�^�����)/��7���S�صF�RV� �L��sY2���2�=��ر��ɩ�4�+A&MUy��-E<vccӣo?}
mP�����y뽮�*f���A$�`�>f�d~U�<Q��S������b�{\�
24��O��%&�fQ;�b-ʰ�v�3 ��3N��T*���I%q8��A���}ŭ �22@�;�57T��&�qCN9��|�jӟ��)Su&�|���9�fo���C�=�O���ث��]i�lވ৴.����W�:&^�V����:��7K� )��LO�mVc����)q��JU���'���S��ʃ��)p7@YΖ̽����u���gc2�L�2u�M�\�p4��x}B����rv���b8��8I�_�N"W�UV�ae^���̺#�7��&Hf 4p��g`�#h{���ⓒ����0t	RQ�q�[�k�&�9ߖaH�'���D�%�'�~!\�an��(��*d�4Y�UPv�c��H�$��+0?+������KPU��|Vu�ڷ����$����n���1��5Ӵn"�IUٍP���
���W|͒w��x��-�,��W�u@].�j�]�TO{�6�v�eM�N9	̼�3U#,��)�>zU�	hċY���3�̄��ǡ�ܷ\�A�xMl�A�>R�O��|za�W��v���&Ƀ=�#M��*��0�,.M[B�ĺ�j�����*eO]���4ӏ���[E5Y�Kڜ���bx]8kyﻴ'|R1�g�W�$�`Yp߅�#�N�	)��`�G�&��0nM6��1�z2�]�c5��I�U���4Q�>�	&�:�����bH)ٛ�#��.���Cə�0�С�DfYJH��#~��+�ae��,�UH��u����oC�G4�m��KE�? be��U��XA���y\��ㄊ�܍uq�c��
��V;�k&7��{�H�S�ɧ��߂-���WG�.��-�]|�c)T����/��n�U��Q-t�~��U��W��Q���G�z<�g�u��3���x.5�����{S��Z�yo\R��ۆ�FDU�j�]!����bD����L.ǃ���֔��jw'�Q�U���|)�!K�T%;'
͍������{�Q��\��b����*D%3���n�����a]���%ٖK��pSǤ�x:���+���S\Uw4a��9�!���jc*������봢|���e�V����}�I�h��5�hLSS#�I��w�����6"Y0�gK�L��͝�=�}���~�l:���N�ben���_��czgc�ȍnU=#�c;�p���I6	O<��nq3���끻೛����هE
"�Փ4�v&�`v���c�Eӵ�H�tq�|+��Rl	~�m�;k$m����)r����[��=�Jr��]�))����!~e�ԿSC(�+���L��}]^��HJp�=^Ѭ+�����i��Y�"��R���t��^{�p���e���Ly�}��;���Bc��Zb��&�ؖ��E�C�1�&¶Nl�%��U��Q-t�~��U�����\5������F*(��T#3��yt�6��b��F��uP��n@�pm���t�;�z	��N+�D(9;�^�/>���n����MWF�k�7�1��a�gw�j@�d1��~��C_�b<�N�6��o��̙��	��*��
G\��c�I�F>	��������%=���#���mG��92��0?�m��W�A���L:K�p�GVsA2!��d�q,P��T�Z�EF�~�d;�0�QG��RyzƓ8�ΦUrr���U|��{L�m� +7k�p]��GKv~L�}�W�ܠD5��-��r ��sN^�")د�-,9x�ߥ���em��cEX�;�kH����h렿*~|��6\^>L�`|��s6��>l��r/t��H,(��
"�1�옎9�2Y�[���l��j��4C�.j�7�y>f%@+P�W,F 	��^G�1�B ���2Yo�|�ft�'l��\�(� �ͫ[��b�7o��S%jC㵒��מ`�Ĭ]S\��_Y�?jN|b8߃��tA��R���"��X��9��%!��Q���um��܆nl�G��'�$\��w�w�\�,��X:�||BuFMҀ�1ߤ|8M"K3��/s���%�	λ��}%��R�N��w�v�mi�葷�rda������e�PY?��Q#,�s	�y����
���l�[Y���gxL�����
r�޹�b��r�$ۼ?�� E���D�������E�IE�ᶥ��2P �na�}0����`�׏*5�M���@����|�<���w��9/���� /�A�Xce��V~� �y(J;�ّB�QE�ƹ���B~��,#���7�R����e���cf�:�t��f����F�ګ����������WXX�����	ւ�`:1Np���}������\�%��>�Խ̭�o�r���'�M�<�(Q��L	*�&C@�ςE=QlM�p$I����	9�_�U-6���M0��"���p�贉d�7�*���YPYN��hZ��&Ƀ=�#M��*��0�,.M[B�ĺ�j�����*eO]���4ӏ���[E5Y�Kڜ���bx]8kyﻴ'|R1�g�W�$�`Yp߅�#��UA���V�ܫ9�x^@8tB�@��5���O��ϑ@]l܉=ǘ��VKG��(�x�\�c_�<��I�/����6����A_H6p�<|�T ݥb��(�M��ZdXM��n�i�F�i�2�l����N����c�CzP!���5:K*ى�bD�����C�x�U߮j�}��j�X��(D����BHV��Q8���T���8z�p�T�N	־���z9��j>O���y���S��
��l�+x��F�!&(hK|�[� ���}خ�G�"�>�p��+H�,��9���}4@+v�	�9��	��0s������ɰ��4�+A&MUy��-�N�E��p>Tg��O������N��TL24��N��VŬ��Q+�@��V_lP�?�:%�3~L���O�� u��4s�+�F^1�hG����M b���W�%כ6�-��5�"x��ǐ�:e�|��{\'��+�1zȤ���@�% ��$�TLc���9G��!-������(�X��,N��˖�n
��nsU�˄���m������b����`�)�EvF���7ImH��*
���^ҿ�X>����k���`P�����?l焬"L��`� ��,�@z�c�g��&�l6#��kd�F؀��\T����) ����ǁ$z�j�珸zY��1�o�J��?⴮�	�D��ƿ�,Bu�
OvPZ8�����a���]�@#�s�����'Rt�S�ί���\/ t7�D@�	�5B�ڪo�-\.ߧ�%�O�"�^�	^�o؊�eA{^�EZ�h�;�����-�< <-�q����8�?8��E��m����/3�қ)]����]�s�V.v��M<`�]��1mЕ4�������D�;�0������2�M-�����*�M�^�ӣ���mR�����l�N�2���fF��fi
�ʞ!E3��O%�Ka��0
�6��NK�F:������[��57�L����sk�9Q��6��"�w� [�ٙ�,"X�����IY�NǦ+gX�?���!��A�[g�j#��'�o|��kp�^o������x�����ZT��wX~�6eI�V��ʋ`�	0�eNN�1��4��h�re�Ãd������M\ȌpB�$�+过?m�ZE^�\uTt���[�_ ��x<5��{��Se�����N�O廼/�*z��[n�h'���y�WK).̧
��j"9ŵ~wm�xD�k]��*<v��k�LeiՃn.����Sv��8�kӐ����7�t0�Mm&��ȷ�82���'�߿Kf��0�ES�'߁�Eko�#�[I@eW=���N���M��B��s�������=ivqƑEf"�I�]�s8�Fk���,38��\�g��s��٩\�b����ԝd贵O��\L-?�����(��4�S������[��� Jv��^:�v�
�ܨ<qM_��5��� 8KbKp���z� �9W���pҡ�z�]���3�u<�eEE��Ǟ���;���h��C�.���9��������B�W�(��c�-FF5K"�㕯�`�!Sy�u'"�D�\����;sLA��zi{�r� \ b�@�5$��*(�-�!-���U�Ń�n�u�� �Q,`1-�$f^b�
O(�4o�wSi�3Fu��ЉsǨ��^��~Hߔ�Ǯ%�h	�P���EӶ��۲�{�i��k2�с˷c �kC8��#���h�XU$�5�t�;��6�3��CP�MUUU�G<��(�YvQ�^L�g�
�1�V�t{���p8�f/sL`��nc�Ԙ��}˴i�������.N�5����z�$�Ku����w�,��`��F@9��#�l���w��nLn��� ��!aDC)���5�e
�4���<Z�*=ցLJP�MZ/�Xx�A_'�Z*�˪��f.oc$^F(|��@�m9�y���s���,=a��$�8�l94������7bA�$�
#�a���D�k��VRn�2A�P�ZR	픛.�L����~!,ߺ�B��;��F�;���pY.P�6�Տ�J��5���:)�a1��H:_���L��1}�Z�4�蚕��c��u�U�y�-�d��t<�	�Zg�P�(��m�d��b~F_&�ŌC�`�4��{�p���Q�����^Q���ޗ��.��q3�SI��������O���i��5�U[�q������0�� a
��)@^q��~��;caeV$�KK�d牂K���+AA�B-%�:�-[��!"T���N���e#n"�%s���Q�"�U��)W�j{���I�z�ӹo-�aȽ�F�. �B� �,E͌��/ː���x�V?�Jq�yٓKJkcSa{��Y����fMql��4̬3;�Hzu5�?>E�,��~F��F�4�;c0L�n��s��?�đV����rD�d�h��9Y(
������B5��s��q�����;e�,
��i�����kK Ll�ͬ�i�w}�Ӄ��IR�THsP%\.Y-��_�)���r���<}fՈ�í�K_��35�E�<�y#Y�I�|�<��GY��<4�Ls=l-
�/ڂ��TP�A��[<��|��'5�� �����oUo�J�<�@�dZ��L� }����ip�����'ܡ�M�����3�l���L��ћ ��b��.撇5�m�Y�J4-�a���^��X(N��YiA&�"���w�و����o��6҇�7�&��5�腈�L�A�e��[z���w��mYi�T�ey�����i��Gt;�ō�ʉ5�\z�7�b�)����%6������▖d#FullMData
� Sld_FullData �� first_member_ptr �� alumni_members �� ref_part_table �����
name START_ASM �type �revnum �memb_id_tab �����dep_id ��attributes � simpl_rep ��feat_id �� next ����STARTASM D��������STARTASM t��������STARTASM z��������STARTASM ����������STARTASM ����������STARTASM ����������DAU_SHOCK_X ��	 ���� first_xsec_ptr ��� lang_ptr ��interact_on �� first_csys_ptr ��� dim_array �����type �dim_type ���feat_id �attributes �� dim_dat_ptr ��value �bck_value � tol_ptr ��type 	�tol_type �dual_digits_diff ���value ���digits �nominal_value ��bound �bck_bound �� bck_tol_ptr ��value ���datum_def_id ���places_denom �bck_places_denom �dim_data_attrib  � dim_cosm_ptr ��� p_sld_info �� attach_ptr ��parent_dim_id �� symbol ��type �text(i_val)  ��������	������� ��������
���	������� ��������������������������V���	��(�bM������(�bM�������������V���	��(�bM������(�bM�������������������N��/i /i �	��(�bM����/i ��(�bM����� ������ipar_num � � smt_info_ptr �� cable_info_ptr ��id ��type ^�ref_harness_id ��cable_assem_id ��cable_assem_simp_id �� logi_info ��work_subharn_id ����parent_ext_ref_id �� p_frbn_info ��
log_info_src_name ��log_info_src_type  � cable_report_info ��
cable_jacket_report_name DEFAULT �
cable_shield_report_name SHIELD �
cable_node_report_name - � comp_info_ptr ��� prt_misc_ptr �� shrink_data_ptr �� expld_mv_info ��� state_info_ptr �� alumni_feats �� p_mdl_grid �� mdl_status_ptr �� appl_info_arr ��J���id ��type &��J� 8��� ����7� post_reg_outline ���outline �FwE;in��FP�����hFwE;in��-wE;in��-�ڝ��Zm-wE;in��� ribbon_info_ptr ��� p_xref_data �� first_analysis_feat ��� da_info �� setups �� setup_xar ��T���setup_type �setup_id  �attr  �appl_id  � setup_data(da_setup_icon) ��shape  �color �	�first_free ��� id_map_info_ptr ���faceted_rep  � sw_info ��offset �-cZb����-wu�"Q��-}��#҅�-j��ދ�K��#NeuPrtSld
� Sld_NeutPart �� neut_part ��#NeuAsmSld
� Sld_NeutAssem �� neut_assem �� assem_top ��revnum  �accuracy �outline ���accuracy_is_relative  � mass_props �� time_stamp �� colors �� attributes �����
name VOLUME � value ��type 2�value(d_val) /Y �ref_id ���ref_type ���SURF_AREA �2/n ��MP_X �2/ ��MP_Y �2��MP_Z �2H ��WEIGHT �2/Y ��PTC_COMMON_NAME �3�
value(s_val) dau_shock.asm ���� layers ������id <�
name 00_LAYER_AXIS �user_id  �ids �$�operation ���B01_LAYER_CSYS  ��H02_LAYER_CURVE  ��?03_LAYER_DATUM  �K05_LAYER_POINT  �507_LAYER_ASSEM_MEMBER  �L13_DEFAULT_DATUM  ��	� item_names ������
name ASSY_SIDE_YZ �obj_id �obj_type ���ASSY_TOP_ZX 	�ASSY_FRONT_XY �ASSY_AXIS_X �ASSY_AXIS_Y �ASSY_AXIS_Z '�ASSY_CSYS �DEFAULT �@  �5�DEFAULT �@  ���DEFAULT �@  ���DEFAULT �@   ��� members ������id M�type �
name ASM_VIB_MACHINE �e1 ���e2 ��e3 ���origin �����VDAU_JIG_SHOCK ���;ǜ��B#/���;ǜ��B#�NASM0001_CIR_OUTER ���;ǜ��B#/�`HW@�#ModelView#0
� View ��id  � viewattr ��attr_arr � �� mpoint ���id  �coord �/|�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �xyT�ۢ�c�:�+gk���RFW n�:���rv�P�A��?6���]�AmR@�8Q���local_sys �k�D�m(�(o�Z��A�Fϲ0�`���>b+d(�gr1x��)���P���6L^��3��s-��`�B�-p�R���F�;	_�v� world_matrix_ptr ��world_matrix �k�D�m(�(o�Z��A�Fϲ0�`���>b+d(�gr1x��)���P���6L^��3��s-S���Fcy[��2Ff�kr�r�model2world �k�D�m(�(o�Z��A�Fϲ0�`���>b+d(�gr1x��)���P���6L^��3��s-S���Fcy[��2Ff�kr�r� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� loXŁ�g���outer_xsec_index ��next_view_id �simp_rep_id  �
name no name �fit_outline �/Y /UF�&���v�/� /��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�`KG�c��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#30
� View ��id � viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr ��world_matrix �y9�|��k�y9�|��k��9�|��k��model2world �y9�|��k�y9�|��k��9�|��k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���	� ���� ���outer_xsec_index ��next_view_id (�simp_rep_id  �
name TOP �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#40
� View ��id (� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys ��9�|��k�y9�|��k��9�|��k/@/z^F�J��7�� world_matrix_ptr ��world_matrix ��9�|��k�y9�|��k��9�|��k��model2world ��9�|��k�y9�|��k��9�|��k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��s��2đh{�MR�x������A��0g��,r�� PGpxS��щomË8�)A�i���qm� Z��������� �����	� ���outer_xsec_index ��next_view_id )�simp_rep_id  �
name 5_BACK �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#41
� View ��id )� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k��9�|��k�y9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ���#9��i�����A��h�+ i����-w��<)�(�v����p(�t�AT�(��q�GM`��#�>�����	� ������ ���outer_xsec_index ��next_view_id +�simp_rep_id  �
name 6_BOTTOM �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#43
� View ��id +� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ���9�|��k�y9�|��k�y9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��A�+��+yUVB���p��Ol$*Xݕksuzyآ��P��L�ym7TjcA�Ԋ�8w�AϮ-ԗ7P������ �����	� ���outer_xsec_index ��next_view_id ,�simp_rep_id  �
name 4_LEFT �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#44
� View ��id ,� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ��y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��x�zGϩ2F9f�[��*�<YZ�ɤ�<xh��a1���z��D���o�;�\\J\w˼�TKP������ �����	� ���outer_xsec_index ��next_view_id -�simp_rep_id  �
name 2_RIGHT_YZ �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#45
� View ��id -� viewattr ��attr_arr � �� mpoint ���id  �coord �/|�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �loXŁ�g�loXŁ�g�loXŁ�g/@-eg�}��F�`KG�c�� world_matrix_ptr ��world_matrix �loXŁ�g�loXŁ�g�loXŁ�g�FoT��Ku�model2world �loXŁ�g�loXŁ�g�loXŁ�g�FoT��Ku� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� loXŁ�g��}�� ��matrix ��z���A���}/!(��Q�� Y���(�x�}LC;UA�Ag�V�(�d�<|�\�Qy���X������� �����	� ���outer_xsec_index ��next_view_id .�simp_rep_id  �
name 1_FRONT_XY �fit_outline �/Y /UFy[��9 /� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�`KG�c��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#46
� View ��id .� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���	� ���� ���outer_xsec_index ��next_view_id 3�simp_rep_id  �
name 3_TOP_ZX �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#51
� View ��id 3� viewattr ��attr_arr � �� mpoint ���id  �coord �-J��K�-A�{�{FD�f�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����Y�%�c0o�׏�)A�Q.�v��jL"�w����<7���n��9��m�5/6���u��U7���local_sys �ӟ��!W�̨��7Bi�1��ZW�AĹ���\� ��*Kհ���x��?��x|kA��:��Ŵ���]�-�Ț^��-w�2y� LF�Ҽx)��� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���}�� ��matrix ���=���l�Z�C	�(����S*M��7[��cuW	���A���o��(�8����tAϭ���� ��+�����-�� ��B���������outer_xsec_index ��next_view_id �@  �simp_rep_id  �
name 0_ISO_BACK �fit_outline �/Y /UF�i�5P�/� /��� det_info_ptr �� named_view_pzmatr ������page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��B�F��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#1073741828
� View ��id �@  � viewattr ��attr_arr � �� mpoint ���id  �coord �/|�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �xyT�ۢ�c�:�+gk���RFW n�:���rv�P�A��?6���]�AmR@�8Q���local_sys �k�D�m(�(o�Z��A�Fϲ0�`���>b+d(�gr1x��)���P���6L^��3��s-��`�B�-p�R���F�3��v�� world_matrix_ptr ��world_matrix �k�D�m(�(o�Z��A�Fϲ0�`���>b+d(�gr1x��)���P���6L^��3��s-S���Fcy[��2Ff�kr�r�model2world �k�D�m(�(o�Z��A�Fϲ0�`���>b+d(�gr1x��)���P���6L^��3��s-S���Fcy[��2Ff�kr�r� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���outer_xsec_index ��next_view_id ��simp_rep_id  �
name DEFAULT �fit_outline �/Y /UF�&���v�/� /��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�`KG�c��view_angle � explode_state ���model_cosm_rev_num  �zone_id ��#DispDataColor
� ref_color_table �� ref_color_arr �����maximum �)�fcQ��R)�f�ambient )���diffuse )���specular )��spec_coef �transmission �threshold Z�gl_ind �highlight ���reflection �bump_height �bump ��decal_intensity �decal ��texture ��
name �� surfprop_ptr ����cQ��R�)�f)��)��)�Z�������r\(�\)ٙ)��)��)�Z�)��������*� )�N��
=p�N��
=p�Z	�������pp��
=q)��)��)�Z
�)��������x\(���)��)��Z�������#DispDataTable
��hxO焟I��Д�Gg��pCx~�#��ͣ�	R:ɏ`���r�4�4�ΒR���R%����r���=֓�R�U �j7}x`�3��h�WG����5��@�U ��Q��LE}ߓ4��\R*�n���Y��V���$�`;���}VRoC��W���r�"�zZ��1��._��o���9����������a�4�\)�)�_-RQ�],sj<��T��.���:fm|�*��{�ejݷ��k%hѪ�8䍰�*����Ц�+�ѥQh 8Q`�X��y!��l�b�דq�_3.ߚ�e���Px�>��O�!�v�@=u,�O]�I\*$q?�<����0��ͭ������='�Oњd,��EJ
^���n�HN)��Y�c:�b�)!�Z�5Zd���������|=�ف������Os�:�K���`��$i��-M�Ti���D|�~���ȶΥ��OJ��q3._rA��*�Δq��%����SQ(�j�N�M�(!�Ҧ��:��J�0o�l��	x�&Í����J{���ƥsX?=M��Ƚ,o+�G���}P�}�0��P#y�@��N���|ӑu�����G�6���O�DK}|j��g�XRH����jgH�.䩭��T�"� �~:���V�D�4�g�%?����S"y�m˽����9�וn��櫝�5�g�!BN])5�{7��H�H��[���-����'��!��k����$�\+$}��.|ꮾ�\��=o�{:�n�5+xc�ހ������f\�	��f﷔� p������fuCq���:��e�Bw������f�f�	:�ܷ�T�*b��n#AssMrgPrt
� assem_merge_parts ��#IndentTag
� Fi'�Pյ?˲J`!�H�����C�?S�q+(�;ڝUI����}V��#��i��\�2�:[^Ѱ�Ԙ&y�
���!:�Ҵ0-��k����Q�+kB [���ȝ�&|Ү�d� /��n�Ӟ�b���/�μ��E�N��2�e=���ȸ��ױW-8;T2�_M�ӥ�ϼ��	l�/<dZX����7�c�s
0��D 9������cH>��G��ޝ�	W9�+Q��3q:�h�W��aka�Cg���p�:�x|��1��h�P��=�Lz]2Z`�L.�RvE��w�gk�)a%>:�P;`��J:t����=�k[7?C�qB�k�y���h�΢4[v�g2�+��5��c�T ���@Bږi����#� �$b��Pb��t�qs4�3�	�g^��	Q���z�ތ@����yY�q�0�~vI�%���%�٥J��o�������&"����ZLT�_������뼱9{t8
��$�vŐ}{*N��8P������,Lۛgz&������v���t���X��Y�w��hV������� �	͝����s���ҳf���m���0�1�vR���D��x��Dʫ���s �`�H^E-�T1kF>7�pM��=O�.J؅�͂�7J�������<�)�=�bO��و�0��נ爅u�
O�̫k8SFE_͞�������$M�n�c�|aB�8I���:z���@2Vs�ڃ�#�#Ʉ���y���1׹�|���v1��L��r-������2_�pa৤X����y~L�W�n��� :�M�M��o���D�~lL�{�X��S6�F�4@����I�Zpj�7�_�|��G�#FeatRefData
� Fe��׭j1p)�[��3�Xt��r(p�.�����9����I��cl�0,����a�~���x�1~�K[��yt�Ie�֛h���f�FԩU6[���>�J?�˽�{�ܶTN�#�Ic1IY����|�LJ�AF�w�z�?,�s��z�[�j2��$
����V����\����W�)�\�z���Y�_�Q^��5�#�W�'�vѷ{4�����T��t�b����4��#END_OF_UGC
