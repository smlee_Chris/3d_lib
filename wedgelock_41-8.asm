#UGC:2 ASSEMBLY 1315 620 8 1 1 15 2500 2004320 00000cd5 \
#- VERS 0 0                                                          \
#- HOST                                                              \
#- LINK                                                              \
#- DBID                                                              \
#- REVS 0,                                                           \
#- RELL 0,                                                           \
#- UOBJ_ID 1101985977 2029335734 529864431                           \
#- MACH _Windows                                                     \
#-END_OF_UGC_HEADER
#P_OBJECT 6
@P_object 1 0
0 1 ->
@depend 2 0
1 2 [4]
2 2 
@model_name 3 10
3 3 WEDGELOCK_41-8-8_MID
@model_type 4 1
3 4 2
@revnum 5 1
3 5 1534
@bom_count 6 1
3 6 1
@dep_type 7 1
3 7 2
@stamp 8 0
3 8 
@major_vers 9 0
4 9 
@vers 10 1
5 10 0
@alias_name 11 10
5 11 
@minor_vers 12 0
4 12 
@vers 13 1
5 13 0
@reason 14 10
3 14 
@comp_ids 15 1
3 15 [1]
4 15 103
@comp_dep_types 16 1
3 16 [1]
4 16 2
2 2 
3 3 WEDGELOCK_41-8-8_BOTT
3 4 2
3 5 884
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 104
3 16 [1]
4 16 2
2 2 
3 3 WEDGELOCK_41-8_TOP
3 4 2
3 5 881
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 105
3 16 [1]
4 16 2
2 2 
3 3 WEDGELOCK_41-8-8_PIN
3 4 2
3 5 856
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 106
3 16 [1]
4 16 2
@dep_db 17 0
1 17 ->
@dep_db 18 0
2 18 [5]
3 18 ->
@to_model_id 19 1
4 19 0
@dep_type 20 1
4 20 2
@obj_id 21 1
4 21 103
@obj_type 22 1
4 22 3
3 18 ->
4 19 1
4 20 2
4 21 104
4 22 3
3 18 ->
4 19 2
4 20 2
4 21 105
4 22 3
3 18 ->
4 19 3
4 20 2
4 21 106
4 22 3
3 18 ->
4 19 -3
4 20 4
4 21 7
4 22 242
@model_db 23 0
2 23 [4]
3 23 ->
@model_t_type 24 1
4 24 2
@model_name 25 10
4 25 WEDGELOCK_41-8-8_MID
@generic_name 26 10
4 26 
@stamp 27 0
4 27 
3 23 ->
4 24 2
4 25 WEDGELOCK_41-8-8_BOTT
4 26 
4 27 
3 23 ->
4 24 2
4 25 WEDGELOCK_41-8_TOP
4 26 
4 27 
3 23 ->
4 24 2
4 25 WEDGELOCK_41-8-8_PIN
4 26 
4 27 
@usr_rev 28 0
1 28 [3]
2 28 
@name 29 10
3 29 ADMINISTRATOR
@mod_time 30 0
3 30 
@tm_sec 31 1
4 31 40
@tm_min 32 1
4 32 26
@tm_hour 33 1
4 33 5
@tm_mday 34 1
4 34 3
@tm_mon 35 1
4 35 11
@tm_year 36 1
4 36 104
@action 37 1
3 37 1000
@comment 38 10
3 38 Hostname: 'NC8000'
@rel_level 39 10
3 39 
@rev_string 40 10
3 40 
2 28 
3 29 LGIBMUSER
3 30 
4 31 17
4 32 32
4 33 1
4 34 20
4 35 0
4 36 105
3 37 1000
3 38 Hostname: 'SORBON'
3 39 
3 40 
2 28 
3 29 SMLEE
3 30 
4 31 50
4 32 23
4 33 10
4 34 1
4 35 10
4 36 105
3 37 1000
3 38 Hostname: 'SORBON'
3 39 
3 40 
@revnum_std 41 1
1 41 397
@flag 42 1
1 42 0
@name 43 10
1 43 NULL
@inst_hidden 44 1
1 44 0
@old_name 45 10
1 45 NULL
@user_symbols 46 0
1 46 [1]
2 46 ->
@class 47 1
3 47 1
@access 48 1
3 48 0
@changed 49 1
3 49 0
@copy_of_generic 50 1
3 50 0
@name 51 10
3 51 PTC_COMMON_NAME
@value 52 0
3 52 
@type 53 1
4 53 51
@value(s_val) 54 10
4 54 wed_lock.asm
@id 55 1
3 55 -1
@type 56 1
3 56 -1
@appl_data 57 0
1 57 ->
@bounding_box 58 2
2 58 [2][3]
$C00CAA0DCA18D4D9,C0287DF82EF0B42F,C006917A555C6A5A
$400CAA0DCA18D4D9,4057E189C66128F5,400CA1DCA32F5C0C
@ver_key 59 0
1 59 ->
@time 60 1
2 60 1130840583
@random_num 61 1
2 61 388388085
@hostid 62 1
2 62 1987995311
@last_read 63 1
1 63 -1
#END_OF_P_OBJECT
#Pro/ENGINEER  TM  Wildfire 2.0  (c) 1988-2002 by Wisdom Systems  All Rights Reserved. M050
#UGC_TOC 1 32 81 17#############################################################
SolidPersistTable acd 304 1 1315 10 2 4558 349f#################################
SolidPrimdata dd1 fa 1 1315 10 2 6652 6a58######################################
BasicData ecb 32e 1 1315 9 -1 4e0e 829a#########################################
BasicText 11f9 32f 1 1315 9 -1 4041 4dd8########################################
GeomDepen 1528 16c 1 1315 9 -1 9192 8853########################################
DispCntrl 1694 330 1 1315 9 -1 7290 bb07########################################
DisplData 19c4 29 1 1315 9 -1 0cac b9ca#########################################
LargeText 19ed 1517 1 1315 9 -1 65b0 ec0c#######################################
FamilyInf 2f04 2d 1 1315 9 -1 0e87 f0a9#########################################
VisibGeom 2f31 32e 1 1315 11 -1 889b 34f6#######################################
NovisGeom 325f 160 1 1315 11 -1 a85c 5b89#######################################
ActEntity 33bf 6db 1 1315 9 -1 fa5d 30a4########################################
AllFeatur 3a9a 2517 1 1315 11 -1 8446 d675######################################
FullMData 5fb1 86e 1 1315 9 -1 9920 ccd9########################################
NeuPrtSld 681f 29 1 1315 9 -1 0cfc be04#########################################
NeuAsmSld 6848 46c 1 1315 9 -1 8cf9 ea59########################################
ModelView#0 6cb4 40e 1 1315 9 -1 a91d 5022######################################
ModelView#30 70c2 350 1 1315 9 -1 57f9 9b46#####################################
ModelView#40 7412 3c3 1 1315 9 -1 9957 ed58#####################################
ModelView#41 77d5 408 1 1315 9 -1 9d10 b9f2#####################################
ModelView#43 7bdd 409 1 1315 9 -1 a292 1c61#####################################
ModelView#44 7fe6 36f 1 1315 9 -1 6f42 d074#####################################
ModelView#45 8355 414 1 1315 9 -1 c242 cb3a#####################################
ModelView#46 8769 38e 1 1315 9 -1 6b58 c7e8#####################################
ModelView#51 8af7 3c4 1 1315 9 -1 9d36 b6e6#####################################
ModelView#1073741828 8ebb 40c 1 1315 9 -1 a0b9 43fd#############################
DispDataColor 92c7 120 1 1315 9 -1 701a 93df####################################
DispDataTable 93e7 360 1 1315 12 2 a97f c509####################################
AssMrgPrt 9747 21 1 1315 9 -1 09c5 6a48#########################################
IndentTag 9768 36f 1 1315 11 -1 ab89 d160#######################################
FeatRefData 9ad7 102 1 1315 11 -1 7553 3d4e#####################################
################################################################################
#SolidPersistTable
�� �9���2r椙C��0bؔ0\��|IC  �a��!��2y� �4o~y#F�p��)cF&M:yx��M�6=��4��c��3��L��)R�S�y�N�y��gC%U6iܔY�'�԰�X�8��7r��|C.�4mQ�W,��t��SG�[�*���3M2��(�*H�a�q3f0X�)��al��7p2�I������a�.s�M����r��9�{s�Z x�ܛ���@��y��3'Ԇ@%����:\��1}�1��.���v��{��È*       ,�CP�1�v�P��Nn\&�rt�!Gb��}��qVZ �Gz|�Z"�� �0  � ���GZg|ڌu0�]��Qst��}��Vr�@<7���� ���>�4�]v�ӒM�E��Ie ��@݋b� ��@QG��W�)��<҉3��g �X@G�@��X��="$���`��fG��G� �c�i� \u�iZ�rxQ�	k�G�����OA0�N���^�y��{��gz.��\He���E�S�.��a��х8فY[ŉ�o����}��6��Y���1�:`�wA�㑬c��F�y�l�AZr�y��oB(!�)�k$�Jj����P�JV�ƙ�Q���0K-a���!�C�1w�==�j]A�ƤT���PE�f�OJ�i �gQ�q�+y�zz�	~ �^�`�e�-b*>h�����u �, #SolidPrimdata
�� �9��9iڐ	C'̗3r�ԁ�6e �(��A�
:�(qΗ0r� ����},x0�#�0� 6o�|��M�/`  8���2fL��G��{^n��ԩ���p-XFΜ4s��y�\�7b�P�#p	��-��T�fȖq3ほ/~IC��3V�&Ȇc޸a��Y �x��X��W�p����B�% v�Cg&�4t�������č�s @�q�X�F�;#BasicData
� Sld_BasicData ��tab_key  � tab_key_struct ��type H� tab_key_ptr �o� cmprs_tabkey ���type �� revnum_ptr ��geom_revnum �[�cosm_revnum <� tol_default_ptr ���type 
�value �(�bM���(6��C-#�����h�#�������ang_value ���lin_digits ����ang_digits � ��standard  � iso_tol_tbls_ptr ��datum_count � �axis_count �insert_layer_id ��layer_count �rel_part_eps (S�]� ��model_eps �fixed_model_eps �assembled_count  �part_type �first_ent_id �$]�first_view_id  �outline ��bM���F'l1&�x�I�^5?}�bM���-W�KƧZ�1/�dim_num � geom_lib_ptr ��attrib_array ��    �� A  ��max_part_size �
model_name WEDGELOCK_41-8 � disp_outl_info ��disp_outline �ݪ���F(}�.�/ؑzU\jZ�����-W��a(���ܣ/\� sel_outl_info ���outline �ݪ���F(}�.�/ؑzU\jZ�����-W��a(��O�;dZ�#BasicText
� Sld_BasicText �� assem_mass_prop_ptr ��type  �density �init_dens �rel_density �init_rel_dens  �aver_density  �vrevnum �[�volume -�8䝲a�cg �#.�l��r-E 0L�Q[��V��inertia �-��)�d�(�u�rz�(�q(�"��(�u�rz��AW,d/|�-إUI>D�(�q(�"��-إUI>D�-�.�[�)��eps #�����h��surfarea -�6�I���csys_id �� mass_prop_ptr ���   ����� assign_mp_ptr ��
mat_name_ptr �� unit_arr �����type �factor (�(P�B�
�
name MM �unit_type  ���-�9D�݁TONNE �(24Vx���SECS � p_unit_info ��� custom_unit_arr ����������� xp_custom_sys_unit ������_id ��_obj_type �
name Custom �type �
unit_names �mm kg  sec C rad �_unit_ids ��!�
principal_sys_units �millimeter Newton Second (mmNs) �_principal_sys_units_id 3�history_scale ��_version �_revnum � dsgnate_data ���#GeomDepen
� Sld_GeomDepend ��frst_geom_ptr  � fem_data ��first_quilt_ptr  � topol_state ��last_regen_feat_id j� incr_time_stamp ��time �CgB�random_num �Tf��hostid �v~f�� asm_comp_map ���comp_id ��attrib  �
uniq_name �� asm_map_sys �� time_stamp ��vis_revnum ��invis_revnum �� geom_own_tbl ���num_mech_connection  �num_feat_regens }�first_lo_ptr � �#DispCntrl
� Sld_DispCntrl �� color_prop_ptr �� layer_array �	����id 5�type u�user_id  � weird_wline_ptr ��
weird_wline 07_LAYER_ASSEM_MEMBER � layer_contents ��� layer_compiled �� layer_rbd �� cond_block ��saved_operation ��deftype_num ��attributes � ���<u �00_LAYER_AXIS �� item_data ����� item_data �����type � id_data �����id ����$�revnum ���
���������������������$������ �?u �03_LAYER_DATUM ��
����� �Bu �01_LAYER_CSYS ������������������
��������������������� �Hu �02_LAYER_CURVE ��
����� �Ku �05_LAYER_POINT ����� �Lu �13_DEFAULT_DATUM ����������������������	����
����������������������	������� �Uu �7_ALL_FEATURES ��������
������������ �Vu �1_ALL_PLANES ���������
������������ �#DisplData
� Sld_DispData �� disp_data ��#LargeText
� Sld_LargeText �� names_table �����id �type �
name ASSY_SIDE_YZ �	attr  ���	ASSY_TOP_ZX  �ASSY_FRONT_XY  �ASSY_AXIS_X  �ASSY_AXIS_Y  �'ASSY_AXIS_Z  �ASSY_CSYS  ��@  �5DEFAULT  ��@  ��DEFAULT  ��@  ��DEFAULT  ��@   ��DEFAULT  � relsymdata ��� relsym_set_arr �����id ��type �	ui_order �	ui_order_fixed  � symtbl �����
name  �sym_class '�attr � � value ��type 3�
value(s_val)  � bak_val ���3 �access_fn_idx ��dim_id ��appl_int  �
description �� sym_rstr �� sym_bak_rstr ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �2�value(d_val) /Y ���2/Y ��� ����VOLUME '� �2-�8䝲a��2-�8䝲a��� ����SURF_AREA '� �2-�6�I����2-�6�I����� ����MP_X '� �2��2��� ����MP_Y '� �2-E 0L�Q��2-E 0L�Q��� ����MP_Z '� �2[��V���2[��V���� ����WEIGHT '� �2-�8䝲b��2-�8䝲b��� ����PTC_COMMON_NAME '� �3�wed_lock.asm ��3wed_lock.asm ��� ���� rel_data ���attr  � relset_code_tree �� relcode_tree_child ������node_type � node_data(simple_data) ��type � node_val(loc_handler) ��own_id ��own_type �relset_idx  �sym_id �
src �?0 � relcode_tree_next ������� �� node_val(const_val) ��3 �(("" �� �� node_val(func_call) ��code -�ext_func_id ��postfix_id ���(MP_MASS �� �node_val(op_code) ���(= �� node_data(line_data) ��code ���line_no �
src ) ��)������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�.��MP_VOLUME �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�/��MP_SURF_AREA �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�*��MP_CG_X �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�+��MP_CG_Y �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�,��MP_CG_Z �� �/��= ��0��) � �  ��� relset_bak_code_tree �������� �!��#�� ?0 ������ ��*�3 ("" �� ��+�-��MP_MASS �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�.��MP_VOLUME �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�/��MP_SURF_AREA �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�*��MP_CG_X �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�+��MP_CG_Y �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�,��MP_CG_Z �� �/��= ��0��) � �  ������ �����PRO_MP_SOURCE <��~"x �3GEOMETRY ��3GEOMETRY �� ������MP_DENSITY -��~"x �:� value(NO_val) ����:��� ���	attr �id ��set_id ��unit_id G�origin �����6��G���PRO_MP_ALT_MASS J��~"x �:���:��%� ������������PRO_MP_ALT_VOLUME J��~"x �:���:��%� ����@�����@���PRO_MP_ALT_AREA J��~"x �:���:��%� ����;�����;���PRO_MP_ALT_COGX K��~"x �:���:��&� ������������PRO_MP_ALT_COGY K��~"x �:���:��&� ������������PRO_MP_ALT_COGZ K��~"x �:���:��&� ������������PRO_MP_ALT_IXX K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IYY K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IZZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IXZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IYZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IXY K��~"x �:���:��&� ����N�����N���PRO_MP_MASS =��~�( �2�-�8䝲b��2-�8䝲b�� ������������PRO_MP_VOLUME >��~�( �2-�8䝲a��2-�8䝲a�� ����@�����@���PRO_MP_DENSITY ?��~�( �2��2��� ����G�����G���PRO_MP_AREA @��~�( �2-�6�I����2-�6�I���� ����;�����;���PRO_MP_COGX A��~�( �2��2�� ������������PRO_MP_COGY B��~�( �2-E 0L�Q��2-E 0L�Q�� ������������PRO_MP_COGZ C��~�( �2[��V���2[��V��� ������������PRO_MP_IXX D��~�( �2�AW0�哛���2�AW0�哛��� ����N�����N���PRO_MP_IYY E��~�( �2-��mB����2-��mB��� � ����N�����N���PRO_MP_IZZ F��~�( �2�AW3�0���2�AW3�0��!� ����N�����N���PRO_MP_IXZ G��~�( �2A�q(�"����2A�q(�"���"� ����N�����N���PRO_MP_IYZ H��~�( �2FإUI>D���2FإUI>D��#� ����N�����N���PRO_MP_IXY I��~�( �2��2�$� ����N�����N���� restrict_table ���idx_array ����	attr �� p_alt_sets_tbl �� cfg_states ��� disp_mgr_data �� disp_app_arr ��A���appl_type �5� rep_arr ��C���gen_db_id �@  �gen_db_type �5�gen_db_base_id ��gen_db_gen_attrs  �gen_db_app_attrs  � gen_db_refs �� cra_cis �� item_data ��K��� rep_comp_data ��status � simpl_rep ��set_by  � lw_data ��attrs  � cra_rbd ��� cond_block �� gen_db_runtime ��� gen_db_data ��� rep_data ���spec_rep_id ��attributes � � app_ids ��act_id ��cur_id ��def_id �@  ��A��� disp_style_arr ��]���D�@  ���  ����K���L��� � �����X ����@  ��� expl_arr ��^���D�@   ���  ��id ��type ���state_type  �cur_stage_ind �� stage_info ��c��� unexpl_subass_info ��� expld_comp_ref ���comp_id �� override_sys �� expld_state �� gen_info ���font �color 
��T�� ��@   �@   �@   ��� presentation_arr ��l���D�@  ���  ��T��� disp_ref_arr ���m���id �@  �type �5�aux_id ���m��@  �����@   �� ��@  \���X ���@  �@  �#FamilyInf
� Sld_FamilyInfo �� drv_tbl_ptr ��#VisibGeom
� Sl�o\�3fw�`yrcV�bQBm�G�u0p���!*-֊Ɂ�1�\�sM���jM����V��V�"k�!��08�hBĴ� �}������w�R��� =�M�ȿ$��w�e��&{�R��T��28���{+�ߣ�sq~���Ewm���Z̤(����J>SH�䙍z!�G:�c^��$�������*�['����="�*0k�(߬m��t��{1��*z�E�;�C;S����vӔ���*E�B�P���=��)4N�G�q�X�e��v���d��y@�Og�h3J��Ţ�O�U���"~6�����ĝ�>��M/�晌i�������?�_�$K�j}o|�E���z��h�D�f��v�w!���>n��[��;�_�(���b����&��9�сeD5>>G����ཏ7^ԙ��vi��?�e�p��l1���>�b��=��m8�(f�zl��}v�Cw�4QLߠb>��P�"
�}:�����������M�bO�
�������*�P�����xK o6+����>sD40
���An"�{�zX�6c�{�7!w1Hf���l�'��/c)���x��l��X����hD��/.��@/m��s��'��E�WfD�,\���y��TK�'�%v�D�6-?��K��`�Y��K/�y>R��ڛ�ְ�\2ɉ02/�@�x�Ek���
�*�8���o:*>��^Q�xL��2j�xC��^q�N%O�%-&� �u��A��C{=�	k6.E�2���V{�ջr̊:���u`b�#NovisGeom
� Sl�;���]1��¯�V���-)�H��R�rt�L�p�k{�J�7�4�E�9������<0�K2�F/�O��"R\ʴ�I&��ɇ���_�
��9�V�~)!�)i�|�t��/ޮ�Gk)�u!�
~��0&����[�\�w0dQO#�`�8a\	�_��x�@+>+cL���?���9�k�;=�����6we�+?]�^���Y�K(Ԣ��T�
>2��+R9�a�D�a�G\9��T���ߞص={�a�V��Ne#sK�����9�=�{��[m��c@��w����N���K,����@*^Ěn,�������l"�u��0PO��3y5�zd#ActEntity
� Sld_Entities �� act_feat_ents �����feat_id �datum_id �� entity_ptr(coord_sys) ��type �id � gen_info ��color �sys_type ���ref_csys_type ��radius �local_sys �����feat_id � entity(text) ���text_type "�
text_value ASSY_CSYS �txt_attrib �coord �-`=��t|/z�F���#B�owner_id �height �width_factor � format_info ��thickness ����� entity_ptr(cosmetic) ��type 2�id �� gen_info ��owner_type ��owner_id � text_ptr ��text_type �
text_value ASSY_SIDE_YZ �txt_attrib �owner_id �height �width_factor � format_info ��thickness �� cosm_disp_ents ��� first_disp_ent(line) ��id �� gen_info ��flip ��end1 �F(}�.�/ؑzU\jZ�end2 �F(}�.�/��ܣ/\� entity(line) ���*��F(}�.�/��ܣ/\-W��a(���ܣ/\���-W��a(���ܣ/\-W��a(�ؑzU\jZ���-W��a(�ؑzU\jZF(}�.�/ؑzU\jZ� plane_ptr ���attr  ���	�2��	�ASSY_TOP_ZX �����	ؑzU\jZݪ���ؑzU\jZ�������/���	ؑzU\jZ�������ܣ/\��������	��ܣ/\�������ܣ/\ݪ������	��ܣ/\ݪ���ؑzU\jZݪ�����0� �
�2���ASSY_FRONT_XY 
�����ݪ���F(}�.�/ݪ���-W��a(���/���ݪ���-W��a(������-W��a(���������-W��a(������F(}�.�/��������F(}�.�/ݪ���F(}�.�/��0� ��� entity_ptr(line3d) ��type �id � gen_info ��geom_id ��end1 ��bM����;��J�L��end2 ��bM����;��J�L�A6��C-�orig_len � entity(line3d) ���3���	�6�bM����;��J�L��bM����;��J�L���ASSY_AXIS_X -�,^T���-j&U�ܠF�-�EZ&��������6-W�KƧ��;��J�L�A6��C--W�KƧ��;��J�L��:����	-W�KƧ��;��J�L�F'l1&�x�;��J�L���ASSY_AXIS_Y -t���1!-��@�F�ט��S��$��&���6%�;��J�L��I�^5?}A6��C-�;��J�L��I�^5?}�:�'���	%�;��J�L��I�^5?}�;��J�L��Z�1/��ASSY_AXIS_Z -�JK!�V-n<I�ͷ�F�/��*92��#AllFeatur
� Sl�yb�t������Qgl��N�^#F[���q��"<�`;�ʀl���h]��}�D��}����4'���:�!/,_�?D"
��V5'g̦_�2]l#�^�ıT���U���-�KH�.~A��GO��O �Wt�!�N�O�1�PNc"?C�NHsn�эFsps�8E��گ���q��R��.d���!����tq��B�&�q����b]7��6�Xr�D�&�n���nL�5�k�3�&q|��L����|��S����阫KYdfx�d*'a�l-� e��{8�j��*X��Mr���	�!7�w�	��+���cU_$�~T������P���3]������Y=50&�)����|Y}��O��8�ć��w3Y6�I�ޒ,o&_�P�`$�o�B��_yQ���锉�đ2_8\���m�����2>��0����k�<���J���9U�_�a��4�2�����s�(�s����1~[7���0���B�sϧ� �M�� �E<�T4�$�������΀g�G9Az��40�C�JH�GH��&U�m����f�It���8I�;��"�sQE�B�k2|�ȸ�jv9�a��� &Bw0Kvc��.�
�c2n�����`�g��Fg�6t[)a��f������Ҫ�L�!�-7�D^>�,��A5���ݏ�� �7����hgk�`~�$t�,�����-,.�����U����k'»�'���.4Ч�~,��CX��K��ooAk��w�I+�ge kj�2����'��t6��<[���K�&~�>�Ql>����q�F)a�ԔWCmmA\�@��N���5Aq�1�;tu����^RW��"���b�Խg�Qk�z�*R __�=]V�X#��k�>���Y�HƩ���� $��Q�pOLp��47=��%+_01�=�C�=��f�c2��.C� �����on>;�ذ���m���C<(Q�dHWɺQ���yB�W�l2����q��HsPO�Z�y���!9+�X1�2�*i.�G��k�tW��`B����sF�N2��_�&�
~��{�Ҳ: Š'�L�+��.b=�xC��C�"�+���nT}Bxm�L��;�k��e�U���%d�֝/J1�99`�E�4.�r�=ɼ}���I,-���j� ����.��J*-��9�L���a%���(�^?-I��b@�����|��@yX���q0�Y�$}i����a�(�����wRm��e���|�e��*~c�^"2S��c���<fu�
�+�
�	2k|�V���ދ����8��*W�cul�7����o�X/ͺZl7�#�x�\��nb'���!Qj�y����C:A�[!��i%��
�w����:n�R�7��K&�C�,K�m�@�#�!8�AK�S�:����	�9�R����)��:a��}��I	������!q�|���Q3�I@*.r�eW��O�><4*��S	�U�����b)�oX)�]���x�U���˻��&�>t�"zB�����d.���u�+�B��]-��bV���l��^
�,,BR�.u����L�>���{)fSƩ�=�7�	?��W{�n�d��Fwsa�ᢐ�W�)Tx}��%��ϥҗ�"/	Oo���C5���Y#��pӚ�:��{�羁c�rcR	�"����R��>�:��+���hd6���i��G�����"�'<J�H0#�~E� +S�u�>�0��##�{��Y�FNT�L�����Й���R��55Eऺ��F�ue�v�CyaBb�2�l ���"%ږWC�g��җ�ֺU�)CئPѽ��CB���.w�����b�+//�8�=?�23��aK�M�4��# JJB`+�J/&,c��2X\��i�_U���1�@ʊ>M�D
��-��|�H~~_�<��Y�Ȍ��R��P:�o���7������-��R�9IC+�B����@��%bx�2�F�:���Pc5Ԭ���0�ndq�,���:�zC7:J������^.�0G���K���KH>x�*�W�em�nI�W��f��Ȁ�%�}�g���=�����d/")��'��8NF�@��hm�����+�Tp9$�hD�Iz��5��551��R3��[�V��k�c��وe�~,0�9��#�B�3_�`3���\`d����,�uI��k&��7��Ԓ#��nG#�
5?s61�X��4��31��*�o"M��JQ��hl.e`W�U�*PD�ૺ�~hd�lg���C������sR���r��0/����H+r�c:������lU"5o������c&Lt�Zj�ۑ��f��YE(�
x(}�#>�e<�!��܉�q@B�h��K`�c���v����0�C�M���O)Ǝ���#��%�c�ߒ�<�����5m�90>N{�y�����?�q�q7I/�`�����j��%	�Ĺԙ1�-� d+��@uyO��؜H����汰C7&Sg���6�p4���G�rNQh��9�{�-ـ���r����e��ۧ+��B\!>����;̕�mrj�o�^Q��eᢡHɘ�Y�6P�
��:��	�.�4�1��X7���e�%����2�4�"?���m��hD�o���p(x����,2�X�aNݖ��-��ÊzZ �5*g��$NE��	9�;!�Ĳq�#0�z��}��G�o{z6�x��Ѓ V�q��:-�we�w]]޺i�J���Z�s���F�i�'뙂���m�m�^Y'��̲�0�TQtN/e���o	i���5b����MLP�Յ=T� h��Z�)U���J��$b���Eu���Gh�Q���q@B�h��K`�c������'��������a�\/Q{�Z�S;mj����Ⱥ��W�S&��'UV�}�����^���k��Z\[�]���ԋS�rϣ�"��<��
B�T�l�>3�f�b+Qφ�a��ǟx~�r����ɛ��	�:�?��y����ߺed�c�a:7��94���n.���H�̐��yG@*vN��qW�u]����_α�6�I����#����XH$��f^���{6�X��Ev��X����z�#�H
���BX��b��b�* �U~�����<��#��_]a�2��Tqu�.I�U��	�F5DO�@3�F�<C��#K���Ǧ
�٘4���S�&�ٰ~ln,�����LWu���4�n�D#��_�B38��U�s@��as�k���O�ss	0��[ˤ�̠o�^ry�~��B�r�����|GZ�C3�����_�������$����P3k�P=!͝a�a�˪�����3��o4LI��Ơ$Qs+[�:c�P6E��8 �����������Ro�{��a�ᢐ�W�)Tx}��%��ϥҗ�"/	Oo���C5���Y#��pӚ�:��{�羁c�rcR	�"����R��>�:��+���hd6���i��G�����"�'<J�H0#�~E� +S�u�>�0��##�{��Y�FNT�L�����Й���R��55Eऺ��F�ue�v�CyaBb�2�l ���"%ږWC�g��җ�ֺU�)CئPѽ��CB���.w�����b�+//�8�=?�23��aK�M�4��ˮ�[HK�ϝ��nÇ.����ւ��8̚��p�q�'gR��ٚXY��%��Ʌ*^��n��?Wg�A�3��B��pbF��;�z!�i��}&%������(�����Vc�j���-���ݓ�n��S��ަ�v�X���f3�z�F:5n�nd2��4B����ˬ��  9s�v��#%��?%��� r(LQ(a�0��j3�~,� "��l8���52m�6:�������Y�P'�h�k!�_������x�.�z}�sI)�8��
}G�ް��X&!Y��4�vN3T�m�KxM7�g��8����|Z�a������s;fr�}8����c���}���V܂���Z�L���LVKi˭*�t��;��֕W�s�5k{�h�N�5�הઑ+e(��A�C5�`��U]Yz�Jk�bo�4��P�Ӹ���>i��� 	�����c�lY,�)�6&�3ߣ���g�o@�O�ǃo8�L�φ{^����PJ͝�ӕ(�3��c�r�<��˂g����ŭ����6��чF+�����zҙ�r�Q��e�1�pUJ�jd�m[%I�x"-To���#C�mS�|��P�B�o���㉁��ޡ�������BI�͂�^�b����� S���$Ē�[p>g�N�7���^���HQ�������3E[��}�����7�I�C�8/+����3Yʀ9y��j�L��7N2��}n����N��ٸ�Y130�8Ov��O���D����ě Q8�a!S��|��k|�w�:�X_f�lM�4{��Dp�(��&���ݬyaB~Q����d(��bIDʼ~��x�H&Hzr�*�qs�)j�B�6���S�km�X8�=?Ճ��,�\�0,��%��QǦ6�ejNJM�x뜊�& ������X�i?�D�P~M��=,��sb� evC�T*5 G')��j���V�P���5��},FM㜳�W���ݢ̔<;�C������+�z@	����6V��=�P+Ý���R.|�g%w�vJ�ΐ�H�3�!p�F��Ѫ+
�j�'�]�4�mV�,�:������(���|�A�A)�
��Q7��L{��<��Y׹��u��s&��Ô_7Z�d����rM��!��"zР�_�Wi<c�'�i��w�X���&�Kؔ�T*�y+xu�`N{d,g�Cg�Oa��[;ބY�����x�uO�<���S�)���!�C�e���s�z�]ect��W�$�z�lō�A>0�!R��&��X���j���L�+T����d�����/IuS��p�]+�H��@��J{ ���l�W&����ޕVxPf)�i���h�/��5M��j���b�&Ef	���7�A��Dq{��&\�k��U�vV��e�\�*�7�܄��*����bF��X(Q�Q"˸�:�Nݷ~COY�%��á��.�)|	��BṤ�ɝ;�&��y*�T~6T�~Mꊟ.(M_u��$wȍvd�uw�ӯ���Af�W��De�lpa����	҅�kO�/�{���xP��&;�ߴH֨Ӥ�C$���7��|}������с�9���
ow��Q�}�l�5v��n�%p|�g��>+lwz32��q�#}u{S��7ս�Z�F��d��1m������b�Ep�P���f�Z�
E���?{�`�7(�����3@r����v�	��&S�p��u�dW��,�#�>�9b�TS�Uz�s"ĳ��E���./���e�`8����$�������M��n��5��)J�
�C��Q1�İPB��y�E��O���O�#r�e:T7�F/2��r������Q~����ۃ��h�J�G�̄"{�&�����}�P�p��s3Q�Ɵ�x*$�	!��q|�3��� Vg�@T<e�-�w�N�A�l�Q��m���p(����Ӽd�����6C>Y��{��v�jH�G�j��_�����iR� ��#gt)��������Y(�T1�4����lJ7��a �������#��g�\RA�txn�H���<�1�,�zij|c�����X��V��RS�c��kT׳e��'�f$�ġ��Q���Gt�"?-2�T��p�f��aG�y��|��y�Q_8�mJ~��������yHY_�a���$�+�F˺�ų���Ò;9fvU*+������˷э6ȴ���3�� �{=��yG�P�W�1��"�j���o��/��`���C���	~:���ȰhN��ATu���;y��YO²��8�^̀�E��.Ҩ�g�SDP�M�=�n�
�n����ˑȖOY��PT��:���s��Z�[_/Y(p)ɑ�?�ĐE�|�W���>Tg���/��<�8y~7r�wn��4Ų�O����ր��B���fZ��&��2�$Q ��m��J�L�}��c�L���2�J�^�Na�\�O�z�Iu�ճ�D\8A��|t'U���&g]���G����4�i��Y]��";��6��5�}�2�k��h�S�*)��W���"�bA�ċ��Nl�a�t�$�B ����	O�-�^��D(@õ5�lɃ��M������zɦW���H�v�"f?�� �9Zh��&�=���������G��)3U��_0��B��d�Є���;z�Yx�\��hح�\��K��P��ZE�_s�Ȝ)�4��G���0�Р{�eU���X��|��_�3��)��Aڐz}&N�6!b8�8e���8�忎�Wb�o�%:�P�~���}J��5�n{�c\���G�Fg����zY��XC_a4��Z�������(���Bw�W�j0i��g1��l�:��z@�vQ����2��E-F$Mkv�?����E���t0JXm�ˎ>[1��*Yf�A���Mö�:^��VL,a�]�=/�Ga�h�B-{�j��zM�� q)�i������y��@Z�sQy��V��������	�6��_?߯����[�e "���{�,w�Ȍe[CW����c�m�z~��y�&�j:�#����7�Y�7� ��Z/eZ^�6��8jL��X��(�h�%˴���-x�E�CoK{��?��k�ެ2���V���:Fl�#��n��0|�������� K����� F��	7�r������i�4�(����}r�J�<�B��1B���K�φ�����[�ގON�x���~pg�na]FOi_�.60�� /��G/��?М}e�t;��h(��=g=�h���zsQ|��ҍ*v������Td���1r��l�сf7�u���C���g8W��f��T�|��y��#�G�e��rL�m����dn�F��8�2V�M�a���"�V�06D�!C�R�sǾn>U�S_�t�G��CO��ā�f��h�t�;��_a�w)G[ 3�Akj\�2�ךP=<o,�DC��(����h�{H�owD:���F3�wK5}��o��)� ���,n����jZ�^�9�t�+��Α�Ǵ�S�J��������S�G�o�@�a*�Ew�z\�tD�!����F�{Qg`��q�q!G$��1�G_�w�o���_P����!Ӭl�)�N���q�`҉Ӛ՞�;E ���Ц��p>�6��Ou�$}'�n.QǦ6�ejNJM�x뜊�& ������X�i?�D�P~M��=,��sb� evC�T*5 G')��j���V�P���5��},FM㜳�W���ݢU|��n���{/�Ȕ��t��ez�I�?TB��i�q��~�Z�;��6k3��\��udÐ3��Egg-z;x�8Ct'�VL�˸^�ܣ�̫��� �odQ��j����l	6��::��r�i1�[V��18��].�ƊxA;T1����m���Df౧�wq�܍b�وr��+�w-[�����������x����n�d\��َ8�\j0��n\�!��~!�����~��!���|Чah߷4��ե�ϴ�ә��m�������M6�T*8��C�������)]+^�3����ޤ����`f��)��w�xR�l&����W̮M�� ���,n����jZ�ّ�E�C��k���岱:��7/�.h*���\Y�܄��*����bF���񆳒��G+��H�;o/^���d;���+��mW0�:�Aj̒��xiY��]00F����^���1����pɔܼ�ʁVZ����x�#3�b��l^ј�+���L}y6N Dl���7+�ztH�����9�W�Bv��-�����;���4%i�c�<J�%��
	y<� y��5o%/3��q���*V�h:�cd�A��+9^�QQ5Hv,����0-.�2�L�g���Q���>ĝ�l��c�p){��!8�?6T�)�Ж
��F�ǵvr%B��ΏdN.2pL�Gm,��KA��D��Bڒ:wr^�3�H���<tCY7�u���C���g8Ȧᒱz��48l��MH��e&�A��8�6m[��_�֢�	�$��KD4wrpJG,�ZZ����\Jy��V�f���t�b�W5�����x����r�r�üe�� !�iY��o)c���Ы�p��|x�U��I���IZ�,br��R���X����
�:�#�֎�'���[�xlR�Z-层�5='I	h�o���1,���ʚ�{ʊ� �@�q�Z����B z7'��GQZ$4
�r�r�_������2֎��� ��
��V\���$�������`���P�{������'T�>1д��������r�b|�C��|�e�~������;
�LO��7�^ ��>�_�����Py�2�}U{����ʉ�M}bQ܋���+A&.�P�I�H�U��h%ư	R#�7�u�h��a������N}ˋ��o)����v�ab�-�am3\ί��YӊȺme�z�����S�v^���)���1�y�R;��kD�L0�g���*���E��1�DfY���J�;՚�H��ۯw�VDL�pO"�Ѽ��nN�] +;,��έ����c�9�:26чT\��\0�<�B�w6�a�aR�����&E�g�	x�����!"b�<&q��(��9��ܫ�l��=rÈףq˼:ay��\�я�j�����z6!��6�A��!����pt��5!��.�oD?���y�����o�c.6;��"A��?I���d԰k��y�Xs��Ȭ�y)F�7�u���C���g8��E�˧�TaL��o��:� �YO�:l�����"l�D\��dm�	�|8a?��܄�<A�1��4�����:�������SL�'�y{�)��ਖ਼\�d�������y�q�r�����;}Dg��x�Z��r�dt����-m?�G?oW"�L�O�d�F�,�X��H����
2�uxT�E �[�8�|��EG+e��A����`e	��0O����I���6g��'�VW�/��
�?�f��9_�k��> ��誠��f��"dN���O��@�1���J0�u�0���$�u�5�����)��hz��!{Z[�:���l�!m����{:���lԟ���Z�D�l,�'
�3vgQ��c�W�@�Č�oc_L�ܹ�5���t�u��W�s�1��Ch1Ai��6�=����c�*d��?]�>b!�����|]�[�)�WD͉O��y�����▖d#FullMData
� Sld_FullData �� first_member_ptr �� alumni_members �� ref_part_table �����
name START_ASM �type �revnum �memb_id_tab �����dep_id ��attributes � simpl_rep ��feat_id �� next ����STARTASM D��������STARTASM t��������STARTASM z��������STARTASM ����������STARTASM ����������STARTASM ����������WEDGELOCK_41-8 ��	 ���� first_xsec_ptr ��� lang_ptr ��interact_on �� first_csys_ptr ��� dim_array �����type �dim_type ���feat_id �attributes �� dim_dat_ptr ��value �bck_value � tol_ptr ��type 	�tol_type �dual_digits_diff ���value ���digits �nominal_value ��bound �bck_bound �� bck_tol_ptr ��value ���datum_def_id ���places_denom �bck_places_denom �dim_data_attrib  � dim_cosm_ptr ��� p_sld_info �� attach_ptr ��parent_dim_id �� symbol ��type �text(i_val)  ��������	������� ��������
���	������� �������������������� ��	�������� ����������������i��FQ��RFQ��R�	��(�bM����FQ��R��(�bM�������������h��FQ��RFQ��R�	��(�bM����FQ��R��(�bM�����������ipar_num � � smt_info_ptr �� cable_info_ptr ��id ��type ^�ref_harness_id ��cable_assem_id ��cable_assem_simp_id �� logi_info ��work_subharn_id ����parent_ext_ref_id �� p_frbn_info ��
log_info_src_name ��log_info_src_type  � cable_report_info ��
cable_jacket_report_name DEFAULT �
cable_shield_report_name SHIELD �
cable_node_report_name - � comp_info_ptr ��� prt_misc_ptr �� shrink_data_ptr �� expld_mv_info ��� state_info_ptr �� alumni_feats �� p_mdl_grid �� mdl_status_ptr �� appl_info_arr ��J���id ��type 7� post_reg_outline ��outline �ݪ���F+}�.�/ؑzU\jZ�����-W���a(���ܣ/\��J��&��J� 8��� ��� ribbon_info_ptr ��� p_xref_data �� first_analysis_feat ��� da_info �� setups �� setup_xar ��T���setup_type �setup_id  �attr  �appl_id  � setup_data(da_setup_point) ��shape  �color ���scale ��T�  � setup_data(da_setup_icon) ��shape  �color �	��T�  � 	�first_free ��� id_map_info_ptr ���faceted_rep  � sw_info ��offset �-��l��-D�F�$Sd�-����-��l��-J`�o��-jk�5��-n�y��-J`�o��-n�y�-jk�5�-J�P�L��#NeuPrtSld
� Sld_NeutPart �� neut_part ��#NeuAsmSld
� Sld_NeutAssem �� neut_assem �� assem_top ��revnum  �accuracy �outline ���accuracy_is_relative  � mass_props �� time_stamp �� colors �� attributes �����
name VOLUME � value ��type 2�value(d_val) -�8䝲a�ref_id ���ref_type ���SURF_AREA �2-�6�I����MP_X �2��MP_Y �2-E 0L�Q��MP_Z �2[��V���WEIGHT �2-�8䝲b��PTC_COMMON_NAME �3�
value(s_val) wed_lock.asm ���� layers ��	����id <�
name 00_LAYER_AXIS �user_id  �ids �$�operation ���B01_LAYER_CSYS  ��H02_LAYER_CURVE  ��?03_LAYER_DATUM  �K05_LAYER_POINT  �507_LAYER_ASSEM_MEMBER  �L13_DEFAULT_DATUM  ��	�V1_ALL_PLANES  ��U7_ALL_FEATURES  � item_names ������
name ASSY_SIDE_YZ �obj_id �obj_type ���ASSY_TOP_ZX 	�ASSY_FRONT_XY �ASSY_AXIS_X �ASSY_AXIS_Y �ASSY_AXIS_Z '�ASSY_CSYS �DEFAULT �@  �5�DEFAULT �@  ���DEFAULT �@  ���DEFAULT �@   ��� members ������id g�type �
name WEDGELOCK_41-8-8_MID �e1 ���e2 ��e3 ���origin �����hWEDGELOCK_41-8-8_BOTT �� � F ɺ^5?}sA�7Kƨ�iWEDGELOCK_41-8_TOP 4� � �-V��t�ksA�7Kƨ�jWEDGELOCK_41-8-8_PIN � �� -W�KƧ�sA�7KƟ�#ModelView#0
� View ��id  � viewattr ��attr_arr � �� mpoint ���id  �coord �-D����ocA�7K��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �xyT�ۢ�c�:�+gk���RFW n�:���rv�P�A��?6���]�AmR@�8Q���local_sys �-!D�?�m��4b�fe;��t\�R�O�"��-�J���'en��F_}f9W~�x�#-�_rF -f�����F�L4�>o+� world_matrix_ptr ��world_matrix �-!D�?�m��4b�fe;��t\�R�O�"��-�J���'en��F_}f9W~�x�#-]��0y �Fm�qt�Fq����b~�model2world �-!D�?�m��4b�fe;��t\�R�O�"��-�J���'en��F_}f9W~�x�#-]��0y �Fm�qt�Fq����b~� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� -"��������outer_xsec_index ��next_view_id �simp_rep_id  �
name no name �fit_outline �/Y /UJ��a�H�/� /��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��TH!O�view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#30
� View ��id � viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr ��world_matrix �y9�|��k�y9�|��k��9�|��k��model2world �y9�|��k�y9�|��k��9�|��k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���	� ���� ���outer_xsec_index ��next_view_id (�simp_rep_id  �
name TOP �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#40
� View ��id (� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys ��9�|��k�y9�|��k��9�|��k/@/z^F�J��7�� world_matrix_ptr ��world_matrix ��9�|��k�y9�|��k��9�|��k��model2world ��9�|��k�y9�|��k��9�|��k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��s��2đh{�MR�x������A��0g��,r�� PGpxS��щomË8�)A�i���qm� Z��������� �����	� ���outer_xsec_index ��next_view_id )�simp_rep_id  �
name 5_BACK �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#41
� View ��id )� viewattr ��attr_arr � �� mpoint ���id  �coord �-JL�����4� � first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �-�C"��F�C"��-�C"��-UmswS2�/z^F�B>t�8� world_matrix_ptr ��world_matrix �-�C"��F�C"��-�C"��Fy�"+3M��Oٍ<�model2world �-�C"��F�C"��-�C"��Fy�"+3M��Oٍ<� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� -�C"���}�� ��matrix ���#9��i�����A��h�+ i����-w��<)�(�v����p(�t�AT�(��q�GM`��#�>�����	� ������ ���outer_xsec_index ��next_view_id +�simp_rep_id  �
name 6_BOTTOM �fit_outline �/Y /UFJ�T4��@/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�B>t�8�view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#43
� View ��id +� viewattr ��attr_arr � �� mpoint ���id  �coord �-D���`A�� � first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ��F#)%�H6;�-#)%�H6;�-#)%�H6;�/@-6�A���F��z���� world_matrix_ptr ��world_matrix ��F#)%�H6;�-#)%�H6;�-#)%�H6;�7)%�H6;Fx��;����model2world ��F#)%�H6;�-#)%�H6;�-#)%�H6;�7)%�H6;Fx��;���� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� -#)%�H6;��}�� ��matrix ��A�+��+yUVB���p��Ol$*Xݕksuzyآ��P��L�ym7TjcA�Ԋ�8w�AϮ-ԗ7P������ �����	� ���outer_xsec_index ��next_view_id ,�simp_rep_id  �
name 4_LEFT �fit_outline �/Y /UFQ pL���/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��z����view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#44
� View ��id ,� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ��y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��x�zGϩ2F9f�[��*�<YZ�ɤ�<xh��a1���z��D���o�;�\\J\w˼�TKP������ �����	� ���outer_xsec_index ��next_view_id -�simp_rep_id  �
name 2_RIGHT_YZ �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#45
� View ��id -� viewattr ��attr_arr � �� mpoint ���id  �coord �-Js�|���-Dz�G�{�I�^5� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys ��Q{{�ͣ�Q{{�ͣ�Q{{�ͣ-g��֐'�-f�d�F���~���� world_matrix_ptr ��world_matrix ��Q{{�ͣ�Q{{�ͣ�Q{{�ͣFsF���Fm������M�{?�
�model2world ��Q{{�ͣ�Q{{�ͣ�Q{{�ͣFsF���Fm������M�{?�
� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� �Q{{�ͣ��}�� ��matrix ��z���A���}/!(��Q�� Y���(�x�}LC;UA�Ag�V�(�d�<|�\�Qy���X������� �����	� ���outer_xsec_index ��next_view_id .�simp_rep_id  �
name 1_FRONT_XY �fit_outline �/Y /UFD��`/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-���H}G��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#46
� View ��id .� viewattr ��attr_arr � �� mpoint ���id  �coord �-D���opE����|� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �-"�x1��-"�x1��F"�x1��/@-z����F��N��	�� world_matrix_ptr ��world_matrix �-"�x1��-"�x1��F"�x1�������Fw_A��Q��model2world �-"�x1��-"�x1��F"�x1�������Fw_A��Q�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� -"�x1����	� ���� ���outer_xsec_index ��next_view_id 3�simp_rep_id  �
name 3_TOP_ZX �fit_outline �/Y /UF�`O3�(�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-���>����view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#51
� View ��id 3� viewattr ��attr_arr � �� mpoint ���id  �coord �-J��K�-A�{�{FD�f�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����Y�%�c0o�׏�)A�Q.�v��jL"�w����<7���n��9��m�5/6���u��U7���local_sys �ӟ��!W�̨��7Bi�1��ZW�AĹ���\� ��*Kհ���x��?��x|kA��:��Ŵ���]�-�Ț^��-w�2y� LF�Ҽx)��� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���}�� ��matrix ���=���l�Z�C	�(����S*M��7[��cuW	���A���o��(�8����tAϭ���� ��+�����-�� ��B���������outer_xsec_index ��next_view_id �@  �simp_rep_id  �
name 0_ISO_BACK �fit_outline �/Y /UF�i�5P�/� /��� det_info_ptr �� named_view_pzmatr ������page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��B�F��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#1073741828
� View ��id �@  � viewattr ��attr_arr � �� mpoint ���id  �coord �-D����ocA�7K��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �xyT�ۢ�c�:�+gk���RFW n�:���rv�P�A��?6���]�AmR@�8Q���local_sys �-!D�?�m��4b�fe;��t\�R�O�"��-�J���'en��F_}f9W~�x�#-�_rF -f�����F�}�P��� world_matrix_ptr ��world_matrix �-!D�?�m��4b�fe;��t\�R�O�"��-�J���'en��F_}f9W~�x�#-]��0y �Fm�qt�Fq����b~�model2world �-!D�?�m��4b�fe;��t\�R�O�"��-�J���'en��F_}f9W~�x�#-]��0y �Fm�qt�Fq����b~� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���outer_xsec_index ��next_view_id ��simp_rep_id  �
name DEFAULT �fit_outline �/Y /UF��a�H��/� /��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��TH!O�view_angle � explode_state ���model_cosm_rev_num  �zone_id ��#DispDataColor
� ref_color_table �� ref_color_arr �����maximum �)�f�m=p��
=�ambient )���diffuse )���specular )��spec_coef �transmission �threshold Z�gl_ind �highlight ���reflection �bump_height �bump ��decal_intensity �decal ��texture ��
name �� surfprop_ptr ��#DispDataTable
���C�T��%2	�ecO+�5�=�yW��~�{���ʄu�mՀ��H
��gr8O���A��8��Ǫ��4� �$�GyE��f�*ޥ>$5t?�g:���Ftk���4  йOc2w���RB�t@����Ey�� S�	#8j_
L������oy�3O=n��l�V�t����p���霈��,�>ٱ����N%�`Ѳ�Z\ӹ��@㈖�prZ��i#��l��LJE��jd/��H@A��]N�;�~��wY�]�0�]��}b,��b����Pg�l�K+���3hZ<a��,m����*��<C	B���s&&�Z�	=d����29���ph��C	m�B�����i���JM-XV���� ��>�MV�fx&Sp�"]�z_3��C��g��S�Z��'�4F(�s�-����"�=t��Q4����)�n|kIGۤ9�����X��Y��S��,d;ī�ލry&�`��v=����iK]��|?t 2��coL�����W�OF��<���Y���#����a��'�����m��\.�d��#3����v�š�>�5 �"�N�o�`�� W [,�I���/���ʠ��������B5���Sxq0���!�K�̱�n3��x���U�?���G��0s�:���12|O�)e$��m89�����M����R� �pN���j����Oמ�	B�O�W��H�B�r�V�����yM7�k�D��l��X@�Q��y�NwfX����~�$��KK���.ܺ��=S����/_g�ОZH*X���[�l�U�����(�Ҕ�}:Q{z��?6���`��K�s���ُC��mӓ #AssMrgPrt
� assem_merge_parts ��#IndentTag
� FiJ�z�n����_��45^��Ǡ���ʚ׿N~L�`-K(�uu�Y�؛��rN��|��G�������{�92${�Oܙ�E�k��E���~Bl���
�~������B��G��G娘�"�چ~hw E�y+&lB8F��{�p�+��oɆu���'ʝ� ��[�F��k�(k;dxC]l0���4:ޟƞ�5�[u�
�hU�N]���(��f�:g�}�`�s?��t��c�ʢ|O�Ly��;�x�<��1XGZ�d�J�[��R��]Č=��9=�}��#����� �Q�z܍�Uw�o����/w��[�>CA�4�(�t��Y��Dl�d���r�V<m[L�rsu�p:����Y���[U���ؔ[N,P�Du
l{w	-���a�qx7���)��.��+���]�is��Q$=��l�*�]�c*<'J*
� M̈���~m���B筎&F��B�?��_:�)�7̂��8��{��W�(6��Z������Z�9w%��6��"��z����}������@��?�o	a�!X�I��om7 ��{�	�\�k\��Yh��-_(��k=�D,x�n�Zu��{��l�B�e#�����I�"O��p;��`���2�9iA2�k�~'��bS3���f|^��þ*�c1<q��d�5"�	D/֋�ɤZ�P'���:���S��F�A�C{__ʄT�:>���!��$�����L��t�e[��2�5�0����Y٫|�<����d'�ݭy��GS.@~���l�c��!�m �)��)7��^}�@��Șw���uk��&���Ý�K;�ԎLu�{p��I���m�#FeatRefData
� Fe2�KRԊ��mZ�tf9�<�
o}A�B�Op+��r��w�˻�J�P_�M�>���y�?��=ü���Љ����r^�OOv�$�u��~p�(�W��� ��i�e��Kр�Su����,>�m��d�cn�Z-B#��T��?�W��i�W��ĝ�&�>[�	m:*f��"J{��=)�}~H`V{zX	N>�gF��=���Ufq��ukl���!=z���?CW?�\�He#END_OF_UGC
