#UGC:2 ASSEMBLY 1315 620 8 1 1 15 2500 2004320 00000a0d \
#- VERS 0 0                                                          \
#- HOST                                                              \
#- LINK                                                              \
#- DBID                                                              \
#- REVS 0,                                                           \
#- RELL 0,                                                           \
#- UOBJ_ID 1126246667 1920756888 1987995311                          \
#- MACH _Windows                                                     \
#-END_OF_UGC_HEADER
#P_OBJECT 6
@P_object 1 0
0 1 ->
@depend 2 0
1 2 [2]
2 2 
@model_name 3 10
3 3 NOTE_BOOK_BODY
@model_type 4 1
3 4 2
@revnum 5 1
3 5 1020
@bom_count 6 1
3 6 1
@dep_type 7 1
3 7 2
@stamp 8 0
3 8 
@major_vers 9 0
4 9 
@vers 10 1
5 10 0
@alias_name 11 10
5 11 
@minor_vers 12 0
4 12 
@vers 13 1
5 13 0
@reason 14 10
3 14 
@comp_ids 15 1
3 15 [1]
4 15 78
@comp_dep_types 16 1
3 16 [1]
4 16 2
2 2 
3 3 NOTE_BOOK_COVER
3 4 2
3 5 1302
3 6 1
3 7 2
3 8 
4 9 
5 10 0
5 11 
4 12 
5 13 0
3 14 
3 15 [1]
4 15 79
3 16 [1]
4 16 2
@dep_db 17 0
1 17 ->
@dep_db 18 0
2 18 [3]
3 18 ->
@to_model_id 19 1
4 19 0
@dep_type 20 1
4 20 2
@obj_id 21 1
4 21 78
@obj_type 22 1
4 22 3
3 18 ->
4 19 -3
4 20 4
4 21 7
4 22 242
3 18 ->
4 19 1
4 20 2
4 21 79
4 22 3
@model_db 23 0
2 23 [2]
3 23 ->
@model_t_type 24 1
4 24 2
@model_name 25 10
4 25 NOTE_BOOK_BODY
@generic_name 26 10
4 26 
@stamp 27 0
4 27 
3 23 ->
4 24 2
4 25 NOTE_BOOK_COVER
4 26 
4 27 
@usr_rev 28 0
1 28 [1]
2 28 
@name 29 10
3 29 SMLEE
@mod_time 30 0
3 30 
@tm_sec 31 1
4 31 58
@tm_min 32 1
4 32 38
@tm_hour 33 1
4 33 14
@tm_mday 34 1
4 34 9
@tm_mon 35 1
4 35 8
@tm_year 36 1
4 36 105
@action 37 1
3 37 1000
@comment 38 10
3 38 Hostname: 'SORBON'
@rel_level 39 10
3 39 
@rev_string 40 10
3 40 
@revnum_std 41 1
1 41 274
@flag 42 1
1 42 0
@name 43 10
1 43 NULL
@inst_hidden 44 1
1 44 0
@old_name 45 10
1 45 NULL
@user_symbols 46 0
1 46 [1]
2 46 ->
@class 47 1
3 47 1
@access 48 1
3 48 0
@changed 49 1
3 49 0
@copy_of_generic 50 1
3 50 0
@name 51 10
3 51 PTC_COMMON_NAME
@value 52 0
3 52 
@type 53 1
4 53 51
@value(s_val) 54 10
4 54 note_book.asm
@id 55 1
3 55 -1
@type 56 1
3 56 -1
@appl_data 57 0
1 57 ->
@bounding_box 58 2
2 58 [2][3]
$C063B00EA8F454C8,C00403AA3D0F8AB3,C0653F07395B0DF1
$4063B00EA8F454C8,40708A5DEBC4B951,405FE01D51E87C5R
@ver_key 59 0
1 59 ->
@time 60 1
2 60 1126275506
@random_num 61 1
2 61 1881432372
@hostid 62 1
2 62 1987995311
@last_read 63 1
1 63 -1
#END_OF_P_OBJECT
#Pro/ENGINEER  TM  Wildfire 2.0  (c) 2004 by Parametric Technology Corporation  All Rights Reserved. M050
#UGC_TOC 1 32 81 17#############################################################
SolidPersistTable adb 304 1 1315 10 2 4558 349f#################################
SolidPrimdata ddf fb 1 1315 10 2 65e3 bb5d######################################
BasicData eda 331 1 1315 9 -1 46ef 0b18#########################################
BasicText 120b 330 1 1315 9 -1 45ac 78c2########################################
GeomDepen 153b 16c 1 1315 9 -1 926c 4598########################################
DispCntrl 16a7 371 1 1315 9 -1 9472 024f########################################
DisplData 1a18 29 1 1315 9 -1 0cac b9ca#########################################
LargeText 1a41 1592 1 1315 9 -1 a6a4 eb37#######################################
FamilyInf 2fd3 2d 1 1315 9 -1 0e87 f0a9#########################################
VisibGeom 3000 334 1 1315 11 -1 871a 977e#######################################
NovisGeom 3334 14c 1 1315 11 -1 a380 1a03#######################################
ActEntity 3480 6ea 1 1315 9 -1 e5a4 3588########################################
AllFeatur 3b6a 1dd2 1 1315 11 -1 d697 edf5######################################
FullMData 593c 6b7 1 1315 9 -1 d8f2 3c58########################################
NeuPrtSld 5ff3 29 1 1315 9 -1 0cfc be04#########################################
NeuAsmSld 601c 426 1 1315 9 -1 7dd7 07aa########################################
ModelView#0 6442 4ce 1 1315 9 -1 1e75 d68a######################################
ModelView#30 6910 350 1 1315 9 -1 57f9 9b46#####################################
ModelView#40 6c60 3c3 1 1315 9 -1 9957 ed58#####################################
ModelView#41 7023 36f 1 1315 9 -1 6f62 2bde#####################################
ModelView#43 7392 36e 1 1315 9 -1 6cf0 10d2#####################################
ModelView#44 7700 36f 1 1315 9 -1 6f42 d074#####################################
ModelView#45 7a6f 3c9 1 1315 9 -1 951a 7571#####################################
ModelView#46 7e38 2f5 1 1315 9 -1 304d 1184#####################################
ModelView#51 812d 3c4 1 1315 9 -1 9d36 b6e6#####################################
ModelView#1073741828 84f1 3fe 1 1315 9 -1 a49e fc37#############################
DispDataColor 88ef 11f 1 1315 9 -1 7075 4150####################################
DispDataTable 8a0e 322 1 1315 12 2 8005 6a31####################################
AssMrgPrt 8d30 21 1 1315 9 -1 09c5 6a48#########################################
IndentTag 8d51 36f 1 1315 11 -1 a50b c7c9#######################################
FeatRefData 90c0 fb 1 1315 11 -1 75d2 eec4######################################
################################################################################
#SolidPersistTable
�� �9���2r椙C��0bؔ0\��|IC  �a��!��2y� �4o~y#F�p��)cF&M:yx��M�6=��4��c��3��L��)R�S�y�N�y��gC%U6iܔY�'�԰�X�8��7r��|C.�4mQ�W,��t��SG�[�*���3M2��(�*H�a�q3f0X�)��al��7p2�I������a�.s�M����r��9�{s�Z x�ܛ���@��y��3'Ԇ@%����:\��1}�1��.���v��{��È*       ,�CP�1�v�P��Nn\&�rt�!Gb��}��qVZ �Gz|�Z"�� �0  � ���GZg|ڌu0�]��Qst��}��Vr�@<7���� ���>�4�]v�ӒM�E��Ie ��@݋b� ��@QG��W�)��<҉3��g �X@G�@��X��="$���`��fG��G� �c�i� \u�iZ�rxQ�	k�G�����OA0�N���^�y��{��gz.��\He���E�S�.��a��х8فY[ŉ�o����}��6��Y���1�:`�wA�㑬c��F�y�l�AZr�y��oB(!�)�k$�Jj����P�JV�ƙ�Q���0K-a���!�C�1w�==�j]A�ƤT���PE�f�OJ�i �gQ�q�+y�zz�	~ �^�`�e�-b*>h�����u �, #SolidPrimdata
�� �9��9iڐ	C'̗3r�ԁ�6e �(��A�
:�(qΗ0r� ����},x0�#�0� 6o�|��M�/`  8���2fL��G��{^n��ԩ���p-XFΜ4s��y�\�7b�P�#p	��-��T�fȖq3ほ/~IC��3V�&Ȇc޸a��Y �x��X��W�p���.�X v�Cg&�4t�������č�s @�q�Xp��w#BasicData
� Sld_BasicData ��tab_key  � tab_key_struct ��type H� tab_key_ptr �T� cmprs_tabkey ���type �� revnum_ptr ��geom_revnum ���cosm_revnum -� tol_default_ptr ���type 
�value �(�bM���(6��C-#�����h�#�������ang_value ���lin_digits ����ang_digits � ��standard  � iso_tol_tbls_ptr ��datum_count � �axis_count �insert_layer_id ��layer_count �rel_part_eps (S�]� ��model_eps 
�fixed_model_eps �assembled_count  �part_type �first_ent_id � y�first_view_id  �outline �Fc`   �Fd���f��-c`   �-pbV�J�<-_?������dim_num � geom_lib_ptr ��attrib_array ��   �    � A  ��max_part_size �
model_name NOTE_BOOK � disp_outl_info ��disp_outline �Fc���T���=��Fe?9[�-c���T�-p�]�ĹQ-_�Q�|U� sel_outl_info ���outline �Fc���T���=��Fe?9[�-c���T�-p�]�ĹQ-_�Q�|U�#BasicText
� Sld_BasicText �� assem_mass_prop_ptr ��type  �density �init_dens �rel_density �init_rel_dens  �aver_density �vrevnum ���volume �A::�s�g�cg �2�MCүy-IjO�y,�FH�Di��inertia ��B
z�xI��h�-�Cۤo d�#�h�-�B�B	�U�G1}����WSޛۤo d�#����WSޜ�Bo���=��eps #�����h��surfarea �A&0v�;��csys_id �� mass_prop_ptr ���   ����� assign_mp_ptr ��
mat_name_ptr �� unit_arr �����type �factor (�(P�B�
�
name MM �unit_type  ���-�9D�݁TONNE �(24Vx���SECS � p_unit_info ��� custom_unit_arr ����������� xp_custom_sys_unit ������_id ��_obj_type �
name Custom �type �
unit_names �mm kg  sec C rad �_unit_ids ��!�
principal_sys_units �millimeter Newton Second (mmNs) �_principal_sys_units_id 3�history_scale ��_version �_revnum 
� dsgnate_data ���#GeomDepen
� Sld_GeomDepend ��frst_geom_ptr  � fem_data ��first_quilt_ptr  � topol_state ��last_regen_feat_id O� incr_time_stamp ��time �C!���random_num �O�iQ�hostid �v~f�� asm_comp_map ���comp_id ��attrib  �
uniq_name �� asm_map_sys �� time_stamp ��vis_revnum ��invis_revnum �� geom_own_tbl ���num_mech_connection  �num_feat_regens r�first_lo_ptr � �#DispCntrl
� Sld_DispCntrl �� color_prop_ptr �� layer_array �	����id 5�type u�user_id  � weird_wline_ptr ��
weird_wline 07_LAYER_ASSEM_MEMBER � layer_contents ��� layer_compiled �� layer_rbd �� cond_block ��saved_operation ��deftype_num ��attributes � ���<u �00_LAYER_AXIS �� item_data ����� item_data �����type � id_data �����id ����$�revnum ���
���������������������$������ �?u �03_LAYER_DATUM ��
����� �Bu �01_LAYER_CSYS ������������������
��������������������� �Hu �02_LAYER_CURVE ��
����� �Ku �05_LAYER_POINT ����� �Lu �13_DEFAULT_DATUM ����������������������	����
����������������������	������� �Ru �7_ALL_FEATURES ����������������P�
����������������P���� �Su �1_ALL_PLANES ����������������P�
����������������P���� �#DisplData
� Sld_DispData �� disp_data ��#LargeText
� Sld_LargeText �� names_table �����id �type �
name ASSY_SIDE_YZ �	attr  ���	ASSY_TOP_ZX  �ASSY_FRONT_XY  �QADTM1  �ASSY_AXIS_X  �ASSY_AXIS_Y  �'ASSY_AXIS_Z  �ASSY_CSYS  ��@  �5DEFAULT  ��@  ��DEFAULT  ��@  ��DEFAULT  ��@   ��DEFAULT  � relsymdata ��� relsym_set_arr �����id ��type �	ui_order �	ui_order_fixed ��� symtbl �����
name  �sym_class '�attr � � value ��type 3�
value(s_val)  � bak_val ���3 �access_fn_idx ��dim_id ��appl_int  �
description �� sym_rstr �� sym_bak_rstr ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �3 ��3 ��� ���� '� �2�value(d_val) /Y ���2/Y ��� ����VOLUME '� �2�A::�s�g��2�A::�s�g��� ����SURF_AREA '� �2�A&0v�;���2�A&0v�;���� ����MP_X '� �2��2��� ����MP_Y '� �2-(��y7~��2-(��y7~��� ����MP_Z '� �2¼������2¼������� ����WEIGHT '� �2�A::�s�f��2�A::�s�f��� ����PTC_COMMON_NAME '� �3�note_book.asm ��3note_book.asm ��� ���� '� �2�/s`��2/s`��� ���� '� �2/o@��2/o@��� ���� '� �2/9 ��2/9 ��� ���� rel_data ���attr  � relset_code_tree �� relcode_tree_child ������node_type � node_data(simple_data) ��type � node_val(loc_handler) ��own_id ��own_type �relset_idx  �sym_id �
src �?0 � relcode_tree_next ������� �� node_val(const_val) ��3� �(("" �� �� node_val(func_call) ��code -�ext_func_id ��postfix_id ���(MP_MASS �� �node_val(op_code) ���(= �� node_data(line_data) ��code ���line_no �
src ) ��)������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�.��MP_VOLUME �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�/��MP_SURF_AREA �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�*��MP_CG_X �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�+��MP_CG_Y �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�,��MP_CG_Z �� �/��= ��0��) � �  ��� relset_bak_code_tree �������� �!��#�� ?0 ������ ��*�3 ("" �� ��+�-��MP_MASS �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�.��MP_VOLUME �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" �� ��+�/��MP_SURF_AREA �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�*��MP_CG_X �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�+��MP_CG_Y �� �/��= ��0��) ������� �!��#�� ?0 ����� ��*�3 ("" ����3assy_csys ,"assy_csys" ����3 ,"" �� ��+�,��MP_CG_Z �� �/��= ��0��) � �  ������ �����PRO_MP_SOURCE <��~"x �3GEOMETRY ��3GEOMETRY �� ������MP_DENSITY -��~"x �:� value(NO_val) ����:��� ���	attr �id ��set_id ��unit_id G�origin �����6��G���PRO_MP_ALT_MASS J��~"x �:���:��%� ������������PRO_MP_ALT_VOLUME J��~"x �:���:��%� ����@�����@���PRO_MP_ALT_AREA J��~"x �:���:��%� ����;�����;���PRO_MP_ALT_COGX K��~"x �:���:��&� ������������PRO_MP_ALT_COGY K��~"x �:���:��&� ������������PRO_MP_ALT_COGZ K��~"x �:���:��&� ������������PRO_MP_ALT_IXX K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IYY K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IZZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IXZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IYZ K��~"x �:���:��&� ����N�����N���PRO_MP_ALT_IXY K��~"x �:���:��&� ����N�����N���PRO_MP_MASS =��~�( �2��A::�s�f��2�A::�s�f�� ������������PRO_MP_VOLUME >��~�( �2�A::�s�g��2�A::�s�g�� ����@�����@���PRO_MP_DENSITY ?��~�( �2��2��� ����G�����G���PRO_MP_AREA @��~�( �2�A&0v�;���2�A&0v�;��� ����;�����;���PRO_MP_COGX A��~�( �2��2�� ������������PRO_MP_COGY B��~�( �2-IjO�y,���2-(��y7~�� ������������PRO_MP_COGZ C��~�( �2FH�Di���2¼������ ������������PRO_MP_IXX D��~�( �2�BlX��֔��2�B����`w�� ����N�����N���PRO_MP_IYY E��~�( �2�B{���b���2�B�����P� � ����N�����N���PRO_MP_IZZ F��~�( �2�B�L_����2�B
����!� ����N�����N���PRO_MP_IXZ G��~�( �2��2�"� ����N�����N���PRO_MP_IYZ H��~�( �2�B��WSޛ��2�Aw����{Z�#� ����N�����N���PRO_MP_IXY I��~�( �2��2�$� ����N�����N������O������4����N���� restrict_table ���idx_array ����	attr �� p_alt_sets_tbl �� cfg_states ��� disp_mgr_data �� disp_app_arr ��A���appl_type �5� rep_arr ��C���gen_db_id �@  �gen_db_type �5�gen_db_base_id ��gen_db_gen_attrs  �gen_db_app_attrs  � gen_db_refs �� cra_cis �� item_data ��K��� rep_comp_data ��status � simpl_rep ��set_by  � lw_data ��attrs  � cra_rbd ��� cond_block �� gen_db_runtime ��� gen_db_data ��� rep_data ���spec_rep_id ��attributes � � app_ids ��act_id ��cur_id ��def_id �@  ��A��� disp_style_arr ��]���D�@  ���  ����K���L��� � �����X ����@  ��� expl_arr ��^���D�@   ���  ��id ��type ���state_type  �cur_stage_ind �� stage_info ��c��� unexpl_subass_info ��� expld_comp_ref ���comp_id �� override_sys �� expld_state �� gen_info ���font �color 
��T�� ���@   �@   ��� presentation_arr ��l���D�@  ���  ��T��� disp_ref_arr ���m���id �@  �type �5�aux_id ���m��@  �����@   �� ��@  \���X ���@  �@  �#FamilyInf
� Sld_FamilyInfo �� drv_tbl_ptr ��#VisibGeom
� SlZ��>n�)"H�V'�I�L2���,��|ĺ��Om܍�(�&_��ؘ	]` �*�y�����a��r��f33����Eʲ�mWQ�`��}��m&}ߜw	;��TQ'����\�(��`���|pɽļ�e�v��xt���0���*�:������[7�=&. ���TpH�<2� �^`�C�X��p�
�{Q����͹^�Ч	�3�(�Yd�BA&0{��Yd�ű���93��D�*L��׹t݈[y��i�Lr�@ح�V��be���e�/�6l�2��u�M�S<��鴦�y�J��t����$����Dhޅ3h�k5ENE(z�}��mvjV!���>hQ,�jJ��3s���o˒���g�$
�N����ZJ����m��F��I � x�u�=@K5+�P/4� �"�O��'�X�����	�t+Jn&@�WU�8�K>�$m-dB�J�4�\���@WO@>Q$��9�&76��a���9F5�g��e`�W�|'ҵˢ����/S�W͌*�v�{*6�p���=�c0�r$D�N}γ�:�OzgCd���q�x��T�C�%5)6�N���1�@��>
cj?���?��P�$6Ǻd�2yc'׏_�5�>����
��MSF>c�D��^����w�^MZq	��ʈm����1���?=�6��ED=���|S�}_@��?y-ƀJ�nQ��eZ�=[�#g����f�S nj P�������p�����t�9��٧�.$���r>S$V�F|��>݃P\�V0.�(�q��,����6iu`b�#NovisGeom
� Sl9"�S��\��X'J��#ҽw�с%ֳZN7�$qh�����V^d�ņ ��p/t�_�d�id���`��H�����Rl���T<�u`D'�r!��)a�a�Oißq9B�%K�NmFmݦ�!�!�.�����Q()B��b�.y���h�c�<m�p�Z��f��xÎIᮬ��C�םP�K�p
�H!P��t����3�*�����s�&����y>SV�Qh2�h�f���Ԓ�cA�/1��<�#���k��R��3��9�i/���5�9���"k�/8U[�xO�ٟ���մ�U����\�NA����������b�td#ActEntity
� Sld_Entities �� act_feat_ents �����feat_id �datum_id �� entity_ptr(coord_sys) ��type �id � gen_info ��color �sys_type ���ref_csys_type ��radius �local_sys �����feat_id � entity(text) ���text_type "�
text_value ASSY_CSYS �txt_attrib �coord �/�/z�F�J��7��owner_id �height �width_factor � format_info ��thickness ����� entity_ptr(cosmetic) ��type 2�id �� gen_info ��owner_type ��owner_id � text_ptr ��text_type �
text_value ASSY_SIDE_YZ �txt_attrib �owner_id �height �width_factor � format_info ��thickness �� cosm_disp_ents ��� first_disp_ent(line) ��id �� gen_info ��flip ��end1 ���=��Fe?9[��end2 ���=��-_�Q�|U� entity(line) ���*����=��-_�Q�|U-p�]�ĹQ-_�Q�|U���-p�]�ĹQ-_�Q�|U-p�]�ĹQFe?9[����-p�]�ĹQFe?9[���=��Fe?9[�� plane_ptr ���attr  ���	�2��	�ASSY_TOP_ZX �����	Fe?9[�Fc���T�Fe?9[�-c���T���/���	Fe?9[�-c���T�-_�Q�|U-c���T����	-_�Q�|U-c���T�-_�Q�|UFc���T����	-_�Q�|UFc���T�Fe?9[�Fc���T���0� �
�2���ASSY_FRONT_XY 
�����Fc���T���=��Fc���T�-p�]�ĹQ��/���Fc���T�-p�]�ĹQ-c���T�-p�]�ĹQ���-c���T�-p�]�ĹQ-c���T���=�����-c���T���=��Fc���T���=����0� ��� entity_ptr(line3d) ��type �id � gen_info ��geom_id ��end1 �-c`   ��;��J�L��end2 �-c`   ��;��J�L�A6��C-�orig_len � entity(line3d) ���3���	�6-c`   ��;��J�L�Fc`   ��;��J�L���ASSY_AXIS_X -�k���C/z^F��x�0���������6-pbV�J�<�;��J�L�A6��C--pbV�J�<�;��J�L��:����	-pbV�J�<�;��J�L���;��J�L���ASSY_AXIS_Y -z���N�@-��D�OݳF�W���*���$��&���6%�;��J�L�Fd���f��A6��C-�;��J�L�Fd���f���:�'���	%�;��J�L�Fd���f���;��J�L�-_?�������ASSY_AXIS_Z -�iH	��-���[��F�ޔ���#AllFeatur
� Sl�bV��I|��i:��Y+��n7|{<\9�B	~5���?�@��u!O �
jJW�x����5˗�j�Wբ���+�vfx�<DF&�;Z���(b����5LǷ��U�s4�c�%0vW-#aKLU�&�.���~ōЂ>G��p�u)�^�%�)�H�l��w���?�&��Q#���R�^L�1dku
|
�,^�r/��g�q	�ڕ$5�9d��ś����5��}'����\�������o�`@A}�/��s���}�5o�������ƖL%܂I,�X#�Yp�B��l��T��5(N;mL��0N0�,�M�B-vb�j�t�����V��@��u�#Y3�]�'����_}q�0��g������]�4桢3�i�nL�Wn��la��5���}��Nn/��5t{�����*���lũ)���(O��q��ሁ�ܬ��>H��=���n�=l�4��>^QD�τ��_Au&��"�"*3��<�r8EO��]��m�c�����X�=��#6�Ћ��φd��)0i���|V88u��Y]Q�7�� 'E��aB��偰�U�֋��K��E�������P�XBA'�6i��`�Ȇ�����s���{'�x2�
�W��0�R�2�$�̔�)>K/�y���(I�:flO���x@��d��4T��ߛ���y^����z��{|��`�G�a$ع=�gmF*�Wr��e#����P>�g�AO2*�<s$���P�L+jX ����������ᙉ4o�<�!��Xe�t�LP�j���$~�礎kjp���?����4� ���"p�`� ���o�#T����TQ�L[���_��g9�fB�N�("p��,e4��~�=k9i6 G��������X���Z%����'-�����MMi�9zH��c���VL7� 4/��������S:�i�ew����c0����1���R�,o��׋ ���EyXuMKmsI^ڑ����������B���%�Z�^Ss���, �g����DP@8��v���D���7��=�>�B}T�p:�@�����o/��Bw]Y�3���ש`'�6Ds`C|t�Sʯ��v��wC��z3�����@تH�l��5��s���~�e�0`J�o&�`�����~��K
����%���{P�`fX������84z;P���B�%l��-b�}6��j�n S�Q�@�w,#�\Z�"��)S٧/��;�e��E�u�y��������p��+WSN��ݫڠp�yM��\�I�%�9x�.�l�������𻉕�g�AM7&�����a�HWrf�߆g�v[ջt��A,b�����ن��%>���vUƗ}t��j�loJ6�C���7
��N,�L�IhO��Ћu�f�i��!��ϙ�Đ�Y	�?��Paɂ���/�)(D1$�QV��hH���yn�Y�.�MN���|Y��u-�/�@���W��cܙ֯N'�-��#�%�T��n2]w6�ѭ���6)��w7Ź��N4��+�~~��i��1m�\3��ц�N��]�k�Hl��[�˰�ţ_?�RA�j��t��~-�R�O�If2~���L�,=2J~�(3ȇ�c^uC��\Y.�8�g�u�.k)������ං��8��(�Q�F�\у,Labw����Ui�P��q�P��k�np�.1����4��"si1P՟c0��+G.3	:3h
���Q�ܛ�=_�����S5����'$I><�c*:���s�Q��#al�@)������+з�I�}�yc���U��>�� ��/7��RaݿH-���ڨ@!,��Ȉ�x��MR����iDʷ�MI��r�]"e�4��r���h�TC���UvםG4}�$�!�z�K���t^�]��46E�.\�PO䜺����+�oo�X�Qj�|����F�}g�."~��/�
:L��E���6�i��B��Hϭ*�3�'TS����j�<���3U�.�%�2!�al��]����ؽ]����c���c��8h��_S�� ��/��W_U�rw����bF��\��#u4 �_�ؘ'����.b< w�Fa�P�,�H���
���z�ܓ��~׳;����'���0
�S>޴���/��vZ��s���J���ē��h�x���Cɕ���$q�L�������"$�����夓���� ZL���Fؐ��sU����tAH��v5���(Ş]���k�[D�,Q�r��=i����X�M;Q4�cw�\��ܼ̣�]��%)q���KC������6p�tĊ�����'碇�&�L��:я<y�&���,��qe��S�UFIү���x���~hɣt�Z��M. �T��zpr�5�H�,)����շ��H��:����H�7�Ϣ����oJ����b]/��S
�F�URE��ѷ8b��+<(�m��*\�����v�{p��r@��	)�N���<rţ�i�ki�%Q/&c0�ǀ2)'7�EB�bR�2�L�WL�3���`]��� NK� �o��7@��Ͽ��x�#-u4��4�@���%V�~_43n�9��/F�U}��ve�pI�n%s�u��Q��;��48<@��۽�����مP�5r���c ����ـ�=�KRx�ޅB�B����ߖ����i�mc�� ��f��ei����ږ+'V����钵���2���D1C��N�����4G�<�H�Kt��*��&�7�P,-��З�r���{����%�,�ԔH1Bo�3��gLm�φD��U%]����,{?��L �z+��N�>g��J��L���v�%��oY�m���-���zm��XD#��K=��ֈ\U�>J��3y�T(��E�j�*�^��Z��F�^1��8� �$*F��G���CKL�)Av˙�0�vԝZO�Q��A��]I���XU��wF�波�t2S����A��"=w��s�:�QZ$�����c�zC�uyge3��><��N؊Q��2Py��QD�-�΄~$M��b��vX-�G=�~I���J߭�^��Ff�2N�#U�F�g�����V����(eG�v��>9Ƕhn����x��-����$�ۯOiF��!_X\�F�=K+`�S<K���]�j�7g�Z_I��BK�"�<��5ߴ�l�����.B�a���k&�9W����|����Y��f�/q���L6���ݴ<u��L�D���YI>�|��dOA����{_�XOݛh��P&���)�!�@"s��!��%4�Z�
�v��clt�rg/%#3������V�E�h�i�W���vT��V�B�=U9c����D�:;*oF�,�&`�7O�q�n(NI�ث��@��9��v`��$]=�s�#����oo-�O����'��=Ï����?�_6`����X��܋��%.�b˂�$K�e�Xf����D�
>nnU��gb�Yl�苬!��|����	C�� ���.�!0�KE0�5������}�g|*,Z}`����T��c��fn��������,��t���T�?H�{�,��G/�������^xڴ��br�Wof0*r u�p��DLBЯ�{'CX��_ڎ:�q�ϋP�/�h�Lfh��qE�Ԋ�l!͸���Eѫ��F�y�5���K���E!o ϣl�HQ�,��@��SYz&#b�dE��V��N��,
2��V-O�m��~��� R~�5B���z:t��������a��r��h��T���{��"����ݡ��~��j��2��a�WMӪT��"k�S=���pE��G�,GW0�`#j I���[�3�*-�#��M�Ќl��o.0�{6H\���[���wv�l1!�ZQ�lkG7���3����B��y��o�~����nqO �i��U)��^z*�uB���v��ur��r{��`��&W`�^(v'eA�J����	<�ts�֍N!����{���O��?W_+~|�G��X Gf��I����^	��X�%F	��Z��ddi�C��쑒�6��a������e^�$]�#F@Q�����N}G�XQ�6St�_}��D0��y�͎k�X�+vnc��v<�k�gX54���"%k�5�M�̟��M��wԮ)b���l�ĠcL�.ܰ��|U��(j���ߖ����i�mc�� �P���gγ�tne���u�p��DLBЯ�{'CX*���1m���E���@?�*]8�r�D�y�d��1�N0���ny�7��K_��/p��r,۵�J�c�}�=3��5�EcT	[�}0�-���%2,�v3���q�8�:,^,+j��TY�:����w&���V�z�>�[��/ �c$klP���o��<����L���ѧp���2�]^ ^�����k��V6�(���u��Z�`��Lл�����QS��B��XD#��K=��ֈ\U�>J��3y�T(��E�j�*�^��Z��F�^1��8� �$*F��G���CKL�)Av˙�0�vԝZO�Q��A��]I��� \#�zP�m�����S����A��"=w����J�K�l�M��.'Q`+��w�w_P͘Mw��;<㕏$��d�� �2\�\xӭ�]�iF�7�^=|0z5%2�[����+��	Mm%����F�2(��`���%~��mx���he�]K_��<5QQ�r>x�����{'�s����tq}���d�����F�� P������e��̦��֜;����}$c��\8	pꩀ��gtH���fNe(ne�:J(/w�Z������y^
@�$��C��G��6��G�H4�V�]�	T�RG�P�W0i���,�EQ�\��y���<ǥ�~��� R~�5B�Ç��qM�����q��U1����a�w<�Fe���(�6�Z��	]U�&�����k�W�2��%�*�-Ȫ���\>��wu�Nl��A�zςV��o�|��Q=U��v}"�9c����D�:;*oF�,�&`�7O�q�n(NI�ث��@��9��v`��$]=�s�#����oo-�O����'��=Ï����?�_6`��n/��Aá	2�Q%�'C��X_���_�m�ϣ��H&\Qe��Qr���E��j��(F��6��Z���g�O,��m��w{h`m3��D�p"�ebW�f��4�ȱ�5�0,7E��h�4U���{O�akh�B�i��Oag�$6c�v�/��:��0wMP*Y�y
Y��
)��g�ǅ5H�����$p=�10_C�J���;5�%gBb��� ��x_}�O`W+*�1�'�x�J
��;@c/�ԈГ�������TG�������N�K��Cg>e�.�<B��{F�Ӎ^�a@�*�+���vge6�/�G=�#�wQ|.��_��GsE�p�Z��,3׮��:�}�g\.S�K�� ��ژ;�����,pO�Is~����<qح>6����e��z��kU}�����"�<��o?{�n_��+<sĝ=�Y���{��Hc�5�3kzm�`U%�s|���&Z��vQ�<Ą��=W�� S�e-1�c�G���C���U��s�{>k�[AO��wΜk�D.�
l6�R|�Pɼ������V,3�x$b����)�di<�R�Bډ_s/�ո�x
8�b�c�FO���J��Ie��F�9h���|mT8XZhڞ�}�6r.%{rM�+=i��B��L��#}��[#�*�'���1�k�7�y��
O;�k$-v�U�J�L���mqG�F�D詸 r��T�}Bm���Ÿ�^P����-J��IX"���6�� ��~�>W�=6C��3�2m�*���d�1�/����2	���Y��n��e�S�q��ܗ <�k���]�:]sUȾ�RfB`�*�B���-�\�{��n9�|�>�ds��D{a��q���zr:���t�OkR��ő6h(�4���aǛ<�ӀLbXPZ ���fۯo�ԓ߼�����nER�,N9\�8���܁���%;�}���*qp�b�&w�
�;l�]��1��kq�=& �<V���O^StG��ڙr��/���=�3l��^�A.���hӁt��*��X�6�L��N�/陀�ߕ���gg�����M~5�W�x��azU/3�����l��� ��&{���&T�ŮnH0�������,��o;DON��ZNW�S��>������p�Qr��cG�H�i
�O3 .%t0%D�ZmگD�WSЙ��\#s>���P�"��'뺺��@�ڂ�w6��?�n;eW'Fs��J��$6�������AO�M	3[���<�π��$V�����@u�_s�d�*r�/�δsI2s����0cP��s��۳������tKѹ�V:� �dݎ<����j&وv���HF�$ɼ<��&E}͜��]<ο�@��'՝��F�n��Us����5�bJ��<�*�U�qx}���ZV_�Ň�[���A�����Z�}Ʌ�%2������+ ��w��W�K.�tsqj<�`u�Y�7%���V����q%�Ǩn����I�o+{q�k�X����8����n ��(����B%}��Y-�XD#��K=��ֈ\U�>J��3y�T(��E�j�*�^��Z��F�^1��8� �$*F��G���CKL�)Av˙�0�vԝZO�Q��A��]I�ﯪ.�YF?('
�-�"G�"+�T=�w)�q�2�r�斑�#^��9N�,�p�FUM�<��k��@�B6D[��}l<��f�ri-@+��ܫ�^$+ �υb���7 ������y.1���M*}8Z�J�pҸ)/����&4�p:|"UzQ��*��ICl�v����8y��%��C��A�b��s�W��
 L�����n����6�n4�������#����w8�6i��?6�.$����4��ʬ�j�|���}O����7�#0?x���[��s�����R�[AG(I��yf*��<����
�������%���9��!%	p�ң�\Ui�_z^�X�,<Z���"9%|²�8^ҭK��rJ牐��^�3F������B�Ґ;t������G�ʇ�eܴ�k݅�Z+@� 2�� �[_��y�_"X�0y=�cY툼���c?nY��x�T�{?9�������lk�E=+x~���,ߞƖ��@�ֲ� �R�������U�0t[��x�RRo�/t���> #Ō[qt	D������G[�3��J�]}J���H�l��.�Z�'ad�f\S�>g���f��>�w֜'�e�`��zYFh��;�r͐jw�s��)
��p����K���p2����u=�UA�������@�φ�O~�b��w�G�p�K�0v������X�d�w���Yn�wq�FtB.1��g��5����]lg��q�I�򑇆����n�@��jϲ�h�'�P8�N��.�wA�H�'\b�P�P�j=�������7��}r�����b�'����߻1Gǯ�Z�U&F���d#FullMData
� Sld_FullData �� first_member_ptr �� alumni_members �� ref_part_table �����
name START_ASM �type �revnum �memb_id_tab �����dep_id ��attributes � simpl_rep ��feat_id �� next ����STARTASM D��������STARTASM t��������STARTASM z��������STARTASM ����������STARTASM ����������STARTASM ����������NOTE_BOOK ��	 ���� first_xsec_ptr ��� lang_ptr ��interact_on �� first_csys_ptr ��� dim_array �����type �dim_type ���feat_id �attributes �� dim_dat_ptr ��value �bck_value � tol_ptr ��type 	�tol_type �dual_digits_diff ���value ���digits �nominal_value ��bound �bck_bound �� bck_tol_ptr ��value ���datum_def_id ���places_denom �bck_places_denom �dim_data_attrib  � dim_cosm_ptr ��� p_sld_info �� attach_ptr ��parent_dim_id �� symbol ��type �text(i_val)  ��������	������� ��������
���	������� �����������������O��HY HY �	����HY ���������
�ipar_num � � smt_info_ptr �� cable_info_ptr ��id ��type ^�ref_harness_id ��cable_assem_id ��cable_assem_simp_id �� logi_info ��work_subharn_id ����parent_ext_ref_id �� p_frbn_info ��
log_info_src_name ��log_info_src_type  � cable_report_info ��
cable_jacket_report_name DEFAULT �
cable_shield_report_name SHIELD �
cable_node_report_name - � comp_info_ptr ��� prt_misc_ptr �� shrink_data_ptr �� expld_mv_info ��� state_info_ptr �� alumni_feats �� p_mdl_grid �� mdl_status_ptr �� appl_info_arr ��J���id ��type 7� post_reg_outline ��outline �Fc��E����"�q7aF_���=�-c��E��-:��L��r-_���=���J��&��J��� ribbon_info_ptr ��� p_xref_data �� first_analysis_feat ��� da_info �� setups ��first_free �� id_map_info_ptr ���faceted_rep  � sw_info ��offset �-h��l�t���#NeuPrtSld
� Sld_NeutPart �� neut_part ��#NeuAsmSld
� Sld_NeutAssem �� neut_assem �� assem_top ��revnum  �accuracy �outline ���accuracy_is_relative  � mass_props �� time_stamp �� colors �� attributes �����
name VOLUME � value ��type 2�value(d_val) �A::�s�g�ref_id ���ref_type ���SURF_AREA �2�A&0v�;���MP_X �2��MP_Y �2-(��y7~��MP_Z �2¼������WEIGHT �2�A::�s�f��PTC_COMMON_NAME �3�
value(s_val) note_book.asm ���� layers ��	����id <�
name 00_LAYER_AXIS �user_id  �ids �$�operation ���B01_LAYER_CSYS  ��H02_LAYER_CURVE  ��?03_LAYER_DATUM  �K05_LAYER_POINT  �507_LAYER_ASSEM_MEMBER  �L13_DEFAULT_DATUM  ��	�S1_ALL_PLANES  �P�R7_ALL_FEATURES  P� item_names ������
name ASSY_SIDE_YZ �obj_id �obj_type ���ASSY_TOP_ZX 	�ASSY_FRONT_XY �ADTM1 Q�ASSY_AXIS_X �ASSY_AXIS_Y �ASSY_AXIS_Z '�ASSY_CSYS �DEFAULT �@  �5�DEFAULT �@  ���DEFAULT �@  ���DEFAULT �@   ��� members ������id N�type �
name NOTE_BOOK_BODY �e1 ���e2 ��e3 ���origin �����ONOTE_BOOK_COVER �A�:~s����z����A�:~s��;ǜ��B#-aar
�|F`�=a���#ModelView#0
� View ��id  � viewattr ��attr_arr � �� mpoint ���id  �coord � � 1) �� � � first_instr_ptr �� attach_ptr_r22 �� vpoint ��� -?�����-z]�����F��������� spoint �� � P ���orient �xvU%1S�A×g�Z��a�'��(A����Ls�d1�Mo	E�OF�ٍq`�_�ǖ��3q&t�B��local_sys ��q���<,�>��K�r�&-�Aέ�\o�~�X��-ʈ�C���A�?��pN�p~5���~��-o�}i�-yN.�}dF�Ì�?px� world_matrix_ptr ��world_matrix ��q���<,�>��K�r�&-�Aέ�\o�~�X��-ʈ�C���A�?��pN�p~5���~�����>��pF3��)�F0,�˶ΐ�model2world ��q���<,�>��K�r�&-�Aέ�\o�~�X��-ʈ�C���A�?��pN�p~5���~�����>��pF3��)�F0,�˶ΐ� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���}�� ��matrix ��z'�%���A�����A��Kv���X"�f��cy���z�AÔd����(�}L{PRPG]޶:z�_��M����-�� ��z�&�����Y�� ����-?�����-z]�����F������ � 1) �� � ��outer_xsec_index ��next_view_id �simp_rep_id  �
name no name �fit_outline �/Y /UJ��X֌�/� /��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix ����F�NYO��9�position ��-�&5~t��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#30
� View ��id � viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr ��world_matrix �y9�|��k�y9�|��k��9�|��k��model2world �y9�|��k�y9�|��k��9�|��k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���	� ���� ���outer_xsec_index ��next_view_id (�simp_rep_id  �
name TOP �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#40
� View ��id (� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys ��9�|��k�y9�|��k��9�|��k/@/z^F�J��7�� world_matrix_ptr ��world_matrix ��9�|��k�y9�|��k��9�|��k��model2world ��9�|��k�y9�|��k��9�|��k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��s��2đh{�MR�x������A��0g��,r�� PGpxS��щomË8�)A�i���qm� Z��������� �����	� ���outer_xsec_index ��next_view_id )�simp_rep_id  �
name 5_BACK �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#41
� View ��id )� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k��9�|��k�y9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ���#9��i�����A��h�+ i����-w��<)�(�v����p(�t�AT�(��q�GM`��#�>�����	� ������ ���outer_xsec_index ��next_view_id +�simp_rep_id  �
name 6_BOTTOM �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#43
� View ��id +� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ���9�|��k�y9�|��k�y9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��A�+��+yUVB���p��Ol$*Xݕksuzyآ��P��L�ym7TjcA�Ԋ�8w�AϮ-ԗ7P������ �����	� ���outer_xsec_index ��next_view_id ,�simp_rep_id  �
name 4_LEFT �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#44
� View ��id ,� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient ������local_sys ��y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��x�zGϩ2F9f�[��*�<YZ�ɤ�<xh��a1���z��D���o�;�\\J\w˼�TKP������ �����	� ���outer_xsec_index ��next_view_id -�simp_rep_id  �
name 2_RIGHT_YZ �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#45
� View ��id -� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k�y9�|��k�y9�|��k/@/z^F�J��7�� world_matrix_ptr ��world_matrix �y9�|��k�y9�|��k�y9�|��k��model2world �y9�|��k�y9�|��k�y9�|��k�� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k��}�� ��matrix ��z���A���}/!(��Q�� Y���(�x�}LC;UA�Ag�V�(�d�<|�\�Qy���X������� �����	� ���outer_xsec_index ��next_view_id .�simp_rep_id  �
name 1_FRONT_XY �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��z�ɇ}M�z�ɇ}M�z�ɇ}MF@��=�)�-N�a�d�h�page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#46
� View ��id .� viewattr ��attr_arr � �� mpoint ���id  �coord ��� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����local_sys �y9�|��k�y9�|��k��9�|��k�/@/z^F�J��7�� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ����-�� y9�|��k���	� ���� ���outer_xsec_index ��next_view_id 3�simp_rep_id  �
name 3_TOP_ZX �fit_outline �/Y /UF~-��7�/� /��� det_info_ptr �� named_view_pzmatr ��matrix �����page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�J��7��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#51
� View ��id 3� viewattr ��attr_arr � �� mpoint ���id  �coord �-J��K�-A�{�{FD�f�� first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �����Y�%�c0o�׏�)A�Q.�v��jL"�w����<7���n��9��m�5/6���u��U7���local_sys �ӟ��!W�̨��7Bi�1��ZW�AĹ���\� ��*Kհ���x��?��x|kA��:��Ŵ���]�-�Ț^��-w�2y� LF�Ҽx)��� world_matrix_ptr �� first_instr_ptr ��type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���}�� ��matrix ���=���l�Z�C	�(����S*M��7[��cuW	���A���o��(�8����tAϭ���� ��+�����-�� ��B���������outer_xsec_index ��next_view_id �@  �simp_rep_id  �
name 0_ISO_BACK �fit_outline �/Y /UF�i�5P�/� /��� det_info_ptr �� named_view_pzmatr ������page_id � �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-��B�F��view_angle � explode_state ���model_cosm_rev_num ��zone_id ��#ModelView#1073741828
� View ��id �@  � viewattr ��attr_arr � �� mpoint ���id  �coord �-) ���  � first_instr_ptr �� attach_ptr_r22 �� vpoint ��� /@/z^��� spoint �� ����orient �xyT�ۢ�c�:�+gk���RFW n�:���rv�P�A��?6���]�AmR@�8Q���local_sys ��eT��Փv�i���g��`���D�!m�<�K��÷��\�<q����V/�|+b4���-��l���-yI^?�F�wإ�Y� world_matrix_ptr ��world_matrix ��eT��Փv�i���g��`���D�!m�<�K��÷��\�<q����V/�|+b4���- �����F1J���F4Si���model2world ��eT��Փv�i���g��`���D�!m�<�K��÷��\�<q����V/�|+b4���- �����F1J���F4Si��� first_instr_ptr ���type d�geom_id ��view_id ��memb_num  �offset � matrix �� next_instr_ptr ���outer_xsec_index ��next_view_id ��simp_rep_id  �
name DEFAULT �fit_outline �/Y /UF���"/� /��� det_info_ptr �� named_view_pzmatr ��page_id  �orig_page_id  �
model_name ��
unrep_model_name �� saved_pro_dls �� eye ��matrix �����position ��-�'�X3N�view_angle � explode_state ���model_cosm_rev_num  �zone_id ��#DispDataColor
� ref_color_table �� ref_color_arr �����maximum �iz�G���ambient )���diffuse )���specular )��spec_coef �transmission �threshold Z�gl_ind �highlight ���reflection )���bump_height �bump ��decal_intensity �decal ��texture ��
name �� surfprop_ptr ��#DispDataTable
��J�û6i��"�Qf}Le1D|f@��7D��Fu�H*�0��"�j��l^��ӳ��5zﴔ���s��2�Y�ew���d,`�+��	l�^'����!�B�Wa��u�/d��@��qm1��X�@�M=�PN���q��P���('	�f	�y��%/�2N��,�3~;��r�PPtIZ��O�ln��d�*�wu������3��`t���Q�$�J}�e�y�/Fr3��ߠ�|�8,ln��~"�{]=@w�o�ξ��W��X�4��0�+3��b��2A��s
��"#m;#� ���y�P	wu����RY�=ǏjTBP2q�>�8 �&,ZK��I��ucy�*�p���,�q�80a�	zvو���gz��z��ԨM%�XfY0~g�H�㢷	����4'��X�߅Sk��m%f�r�Y��M�������z[���7҂���N����˜��]����D��z�����g���i���RS�>��zV��U����?t3�}��0>�d$D�S����Z�\���e᪙>t���kh�tx�s�~[���-7���7���œ�{�[2Si�@��΢��J8KP<�s?�.ærF�����RE���	f%�B\_�-D7C���_�Ahz�Yv^5�ey�����Ba+�6�#7a8{�T(�u�x31mc�-޽K���[(7�I��Z���} ��-q7L>*(�$����\��uc�(�W��գ�ɥ.H��]#W��2��
#���%��fr��(aӞ�l�C<aER�=	 #AssMrgPrt
� assem_merge_parts ��#IndentTag
� Fi0�%��Mȵv1�c�Nv�0^d0b��#��	{X����I���d��6Do%{\��-�IY�<",�+��ҥ�ı^
ߖy��۱/�c¢-Y.h`x�k��gϙ}��f>�9�z��nU�V-�贝E�E�G
vժ��+	�]d<���������
]��T��@���T��3u"��bW��d����6�C�D�J�>N���`j>���8f�)�̔���Kg��9��5$�SHkg���M#|��o����&e3Z(@6�{�Oq����qn(�%2c�ܘY}j�((����C�m	ܹP*C�d>�ȴ&��sp�<A
	�O���::�W"tuK���]�W'��� �9�;��Kk�Gu8�{�%:�4p��:���c��,��X�f�ߨN�oM���,�C/��(i�ʱjg�o�D��&��?�-4Jg~	�����9���}n���k��Hm�ڗ�$2�D��]���r�V����w�*C�ci��I ��ڷJbݞ����M?�4Fk�[�{cЄ�U���n>YE�"���I�Ԕ|�L��g�3�L��x���k��_?�L�$�u'���r\tS5�,{O��sۈ߅yV$�^Q����ny/��@!�{�eKxb�}�x�2r�6+�����L��~���du�	��F3q��1�c%'rR�/0N�;o���A�h��5��+'r�/��r��r�I|8�4�T�ʪ���KM��#�t��Nm&�I����Z,��ر1�:E�}Cn�_Z*�����b_��@7H}�;�b�R
�����E�U�$���S@8��[�x48ṜU���F1�H3��n���E$%})AT[��#FeatRefData
� Fe��8l;vR���-��	�Bkp����.��3���w�R�5����3>����ZI���@�R	���?��P��8|u.�λ(-��K��6܋hլ������7�]c�fw{�ے��Hp����	TpطL�8�Wj�w���ߖ�6�쒼=ϫ`�g슇�ӑ�}q@Xh��V'T�A�gԣ���V�Z��o{���%r��۠�1D��GSc}�3Q$e#END_OF_UGC
